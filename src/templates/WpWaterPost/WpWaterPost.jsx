import React, { useState, useEffect, useContext } from 'react';
import { graphql } from 'gatsby';
import {
  Container, Row,
} from 'react-bootstrap';
import Layout from '../../components/layout';
import SEO from '../../components/seo';
import './wp-water-post.scss';

import { GlobalDispatchContext } from '../../context/GlobalContextProvider';
import { ThemeDispatchContext } from '../../context/ThemeContextProvider';

import SummaryContent from '../../components/Chapters/SummaryContent/SummaryContent';
import ChapterCarouselContent from '../../components/Chapters/ChapterCarouselContent/ChapterCarouselContent';
import Eboti from './components/Eboti/Eboti';

import LastSlideImage from './components/ImgComponents/LastSlideImage';

import WaterAnimationSlide1 from './components/WaterAnimationSlide1/WaterAnimationSlide1';
import WaterAnimationSlide2 from './components/WaterAnimationSlide2/WaterAnimationSlide2';
import WaterAnimationSlide3 from './components/WaterAnimationSlide3/WaterAnimationSlide3';
import WaterAnimationSlide4 from './components/WaterAnimationSlide4/WaterAnimationSlide4';
import WaterAnimationSlide5 from './components/WaterAnimationSlide5/WaterAnimationSlide5';
import WaterAnimationSlide6 from './components/WaterAnimationSlide6/WaterAnimationSlide6';
import WaterAnimationSlide7 from './components/WaterAnimationSlide7/WaterAnimationSlide7';

import WaterNoAnimationSlide1 from './components/WaterAnimationSlide1/WaterNoAnimationSlide1';
import WaterNoAnimationSlide2 from './components/WaterAnimationSlide2/WaterNoAnimationSlide2';
import WaterNoAnimationSlide3 from './components/WaterAnimationSlide3/WaterNoAnimationSlide3';
import WaterNoAnimationSlide4 from './components/WaterAnimationSlide4/WaterNoAnimationSlide4';
import WaterNoAnimationSlide5 from './components/WaterAnimationSlide5/WaterNoAnimationSlide5';
import WaterNoAnimationSlide6 from './components/WaterAnimationSlide6/WaterNoAnimationSlide6';
import WaterNoAnimationSlide7 from './components/WaterAnimationSlide7/WaterNoAnimationSlide7';

import BgSlide1 from './components/ImgComponents/WaterBgSlide1';
import BgSlide2 from './components/ImgComponents/WaterBgSlide2';
import BgSlide3 from './components/ImgComponents/WaterBgSlide3';
import BgSlide4 from './components/ImgComponents/WaterBgSlide4';
import BgSlide5 from './components/ImgComponents/WaterBgSlide5';
import BgSlide6 from './components/ImgComponents/WaterBgSlide6';
import BgSlide7 from './components/ImgComponents/WaterBgSlide7';

const slideAnimations = [<WaterAnimationSlide1 />,
  <WaterAnimationSlide2 />, <WaterAnimationSlide3 />, <WaterAnimationSlide4 />,
  <WaterAnimationSlide5 />, <WaterAnimationSlide6 />, <WaterAnimationSlide7 />];

const slideNoAnimations = [<WaterNoAnimationSlide1 />,
  <WaterNoAnimationSlide2 />, <WaterNoAnimationSlide3 />, <WaterNoAnimationSlide4 />,
  <WaterNoAnimationSlide5 />, <WaterNoAnimationSlide6 />, <WaterNoAnimationSlide7 />];

const slideBg = [<BgSlide1 />, <BgSlide2 />, <BgSlide3 />,
  <BgSlide4 />, <BgSlide5 />, <BgSlide6 />, <BgSlide7 />];

const wpWaterPost = ({
  data: {
    wpWaterPost: {
      title, acfChaptersFields, seo, language, translations,
    },
  },
}) => {
  useEffect(() => {
    dispatch({ type: 'SET_TRANSLATION_LINKS', payload: { translationLinks: translations } });
    dispatch({ type: 'SET_LANG', payload: { lang: language } });
    dispatchTheme({ type: 'SET_THEME', payload: { themeName: 'water' } });
  }, []);

  const dispatch = useContext(GlobalDispatchContext);
  const dispatchTheme = useContext(ThemeDispatchContext);

  const [activeIndex, setActiveIndex] = useState(0);
  const [carouselOrSummary, setCarouselOrSummary] = useState('carousel');

  const handleCarouselOrSummary = () => {
    if (carouselOrSummary === 'carousel') {
      setCarouselOrSummary('summary');
    } else {
      setCarouselOrSummary('carousel');
    }
  };

  return (
    <Layout>
      <SEO
        title={title}
        meta={
        [
          { opengraphSiteName: seo.opengraphSiteName },
          { opengraphType: seo.opengraphType },
          { opengraphTitle: seo.opengraphTitle },
          { opengraphImage: seo.opengraphImage?.link },
          { imageHeight: seo.opengraphImage?.mediaDetails.height },
          { imageWidth: seo.opengraphImage?.mediaDetails.width },
        ]
    }
      />
      <Container fluid className="container-global-styling water-post-container custom-max-height">
        <Row className="h-100 justify-content-center align-items-center">

          {
            carouselOrSummary === 'carousel'
              ? (
                <ChapterCarouselContent
                  chapterTitle={title}
                  acfChaptersFields={acfChaptersFields}
                  handleCarouselOrSummary={handleCarouselOrSummary}
                  eboti={<Eboti index={activeIndex} />}
                  activeIndex={activeIndex}
                  setActiveIndex={setActiveIndex}
                  slideAnimations={slideAnimations}
                  slideNoAnimations={slideNoAnimations}
                  slideBg={slideBg}
                />
              )

              : (
                <SummaryContent
                  summaryTitle={acfChaptersFields.summaryTitle}
                  summaryText={acfChaptersFields.summaryText}
                  exercisesLinkLabelText={acfChaptersFields.exercisesLinkLabelText ? acfChaptersFields.exercisesLinkLabelText : ''}
                  exercisesPageLink={acfChaptersFields.exercisesPageLink ? acfChaptersFields.exercisesPageLink.uri : ''}
                  handleCarouselOrSummary={handleCarouselOrSummary}
                  lastSlideImage={<LastSlideImage />}
                />
              )
              }
        </Row>
      </Container>
    </Layout>
  );
};
export default wpWaterPost;

export const query = graphql`
  query($id: String) {
    wpWaterPost(id: { eq: $id }) {
      id  
      title
      translations {
        language {
          locale
          name
        }
        uri
      }
      language {
          locale
          name
      }
      acfChaptersFields {
        fieldGroupName
        readMoreButtonLabel
        slidesContent {
          fieldGroupName
          text
          title
          introductionText
          additionalFieldsContent{
            title
            text
          }
        }
        summaryTitle
        summaryText
        exercisesLinkLabelText
        exercisesPageLink {
          ... on WpWaterExercisesPost {
            uri
          }
        }
      }
      seo {
          opengraphUrl
          opengraphSiteName
          opengraphType
          opengraphTitle
          opengraphImage {
            link
            mediaDetails {
              height
              width
            }
          }
         }
    }
  }
`;
