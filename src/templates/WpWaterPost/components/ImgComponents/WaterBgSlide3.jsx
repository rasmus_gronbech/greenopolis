import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const BgSlide1 = () => (
  <StaticImage
    style={{
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    }}
    layout="fullWidth"
    alt=""
    placeholder="blurred"
    src="../../images/water_bg_slide3.jpg"
    formats={['auto']}
  />
);

export default BgSlide1;
