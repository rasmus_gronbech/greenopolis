import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const LastSlideImage = () => (
  <StaticImage
    style={{
      width: '100%',
      height: '100%',
      objectFit: 'cover',

    }}
    layout="fullWidth"
    alt=""
    placeholder="blurred"
    src="../../images/noise_last_slide_image.png"
    formats={['auto']}
  />
);

export default LastSlideImage;
