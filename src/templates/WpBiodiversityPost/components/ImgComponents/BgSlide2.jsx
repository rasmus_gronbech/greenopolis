import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const BgSlide2 = () => (
  <StaticImage
    style={{
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    }}
    layout="fullWidth"
    alt=""
    placeholder="blurred"
    src="../../images/bg_slide2.jpg"
    formats={['auto']}
  />
);

export default BgSlide2;
