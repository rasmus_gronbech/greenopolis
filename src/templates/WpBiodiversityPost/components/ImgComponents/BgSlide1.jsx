import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const BgSlide1 = () => (
  <StaticImage
    style={{
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    }}
    layout="fullWidth"
    alt="bg_slide1"
    placeholder="blurred"
    src="../../images/bg_slide1.jpg"
    formats={['auto']}
  />
);

export default BgSlide1;
