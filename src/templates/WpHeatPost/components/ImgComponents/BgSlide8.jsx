import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const BgSlide8 = () => (
  <StaticImage
    style={{
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    }}
    layout="fullWidth"
    alt=""
    placeholder="blurred"
    src="../../images/heat_bg_slide8.jpg"
    formats={['auto']}
  />
);

export default BgSlide8;
