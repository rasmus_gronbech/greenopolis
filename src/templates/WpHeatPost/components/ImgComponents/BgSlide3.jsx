import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const BgSlide3 = () => (
  <StaticImage
    style={{
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    }}
    layout="fullWidth"
    alt=""
    placeholder="blurred"
    src="../../images/heat_bg_slide3a.jpg"
    formats={['auto']}
  />
);

export default BgSlide3;
