import React, { useState, useEffect, useContext } from 'react';
import { graphql } from 'gatsby';
import {
  Container, Row,
} from 'react-bootstrap';
import Layout from '../../components/layout';
import SEO from '../../components/seo';
import './wp-heat-post.scss';

import { GlobalDispatchContext } from '../../context/GlobalContextProvider';
import { ThemeDispatchContext } from '../../context/ThemeContextProvider';

import SummaryContent from '../../components/Chapters/SummaryContent/SummaryContent';
import ChapterCarouselContent from '../../components/Chapters/ChapterCarouselContent/ChapterCarouselContent';
import Eboti from './components/Eboti/Eboti';

import LastSlideImage from './components/ImgComponents/LastSlideImage';

import AnimationSlide1 from './components/HeatAnimationSlide1/HeatAnimationSlide1';
import AnimationSlide2 from './components/HeatAnimationSlide2/HeatAnimationSlide2';
import AnimationSlide3 from './components/HeatAnimationSlide3/HeatAnimationSlide3';
import AnimationSlide4 from './components/HeatAnimationSlide4/HeatAnimationSlide4';
import AnimationSlide5 from './components/HeatAnimationSlide5/HeatAnimationSlide5';
import AnimationSlide6 from './components/HeatAnimationSlide6/HeatAnimationSlide6';
import AnimationSlide7 from './components/HeatAnimationSlide7/HeatAnimationSlide7';
import AnimationSlide8 from './components/HeatAnimationSlide8/HeatAnimationSlide8';

import NoAnimationSlide1 from './components/HeatAnimationSlide1/HeatNoAnimationSlide1';
import NoAnimationSlide2 from './components/HeatAnimationSlide2/HeatNoAnimationSlide2';
import NoAnimationSlide3 from './components/HeatAnimationSlide3/HeatNoAnimationSlide3';
import NoAnimationSlide4 from './components/HeatAnimationSlide4/HeatNoAnimationSlide4';
import NoAnimationSlide5 from './components/HeatAnimationSlide5/HeatNoAnimationSlide5';
import NoAnimationSlide6 from './components/HeatAnimationSlide6/HeatNoAnimationSlide6';
import NoAnimationSlide7 from './components/HeatAnimationSlide7/HeatNoAnimationSlide7';
import NoAnimationSlide8 from './components/HeatAnimationSlide8/HeatNoAnimationSlide8';

import BgSlide1 from './components/ImgComponents/BgSlide1';
import BgSlide2 from './components/ImgComponents/BgSlide2';
import BgSlide3 from './components/ImgComponents/BgSlide3';
import BgSlide4 from './components/ImgComponents/BgSlide4';
import BgSlide5 from './components/ImgComponents/BgSlide5';
import BgSlide6 from './components/ImgComponents/BgSlide6';
import BgSlide7 from './components/ImgComponents/BgSlide7';
import BgSlide8 from './components/ImgComponents/BgSlide8';

const wpHeatPost = ({
  data: {
    wpHeatPost: {
      title, acfChaptersFields, seo, language, translations,
    },
  },
}) => {
  useEffect(() => {
    dispatch({ type: 'SET_TRANSLATION_LINKS', payload: { translationLinks: translations } });
    dispatch({ type: 'SET_LANG', payload: { lang: language } });
    dispatchTheme({ type: 'SET_THEME', payload: { themeName: 'heat' } });
  }, []);

  const dispatch = useContext(GlobalDispatchContext);
  const dispatchTheme = useContext(ThemeDispatchContext);

  const [activeIndex, setActiveIndex] = useState(0);
  const [carouselOrSummary, setCarouselOrSummary] = useState('carousel');

  const slideAnimations = [<AnimationSlide1 />,
    <AnimationSlide2 />, <AnimationSlide3 />, <AnimationSlide4 />,
    <AnimationSlide5 />, <AnimationSlide6 />, <AnimationSlide7 />, <AnimationSlide8 />];

  const slideNoAnimations = [<NoAnimationSlide1 />,
    <NoAnimationSlide2 />, <NoAnimationSlide3 />, <NoAnimationSlide4 />,
    <NoAnimationSlide5 />, <NoAnimationSlide6 />, <NoAnimationSlide7 />, <NoAnimationSlide8 />];

  const slideBg = [<BgSlide1 />, <BgSlide2 />, <BgSlide3 />,
    <BgSlide4 />, <BgSlide5 />, <BgSlide6 />, <BgSlide7 />, <BgSlide8 />];

  const handleCarouselOrSummary = () => {
    if (carouselOrSummary === 'carousel') {
      setCarouselOrSummary('summary');
    } else {
      setCarouselOrSummary('carousel');
    }
  };

  return (
    <Layout>
      <SEO
        title={title}
        meta={
        [
          { opengraphSiteName: seo.opengraphSiteName },
          { opengraphType: seo.opengraphType },
          { opengraphTitle: seo.opengraphTitle },
          { opengraphImage: seo.opengraphImage?.link },
          { imageHeight: seo.opengraphImage?.mediaDetails.height },
          { imageWidth: seo.opengraphImage?.mediaDetails.width },
        ]
    }
      />
      <Container fluid className="container-global-styling heat-post-container custom-max-height">
        <Row className="h-100 justify-content-center align-items-center">

          {
            carouselOrSummary === 'carousel'
              ? (
                <ChapterCarouselContent
                  chapterTitle={title}
                  acfChaptersFields={acfChaptersFields}
                  handleCarouselOrSummary={handleCarouselOrSummary}
                  eboti={<Eboti index={activeIndex} />}
                  activeIndex={activeIndex}
                  setActiveIndex={setActiveIndex}
                  slideAnimations={slideAnimations}
                  slideNoAnimations={slideNoAnimations}
                  slideBg={slideBg}
                />
              )

              : (
                <SummaryContent
                  summaryTitle={acfChaptersFields.summaryTitle}
                  summaryText={acfChaptersFields.summaryText}
                  exercisesLinkLabelText={acfChaptersFields.exercisesLinkLabelText ? acfChaptersFields.exercisesLinkLabelText : ''}
                  exercisesPageLink={acfChaptersFields.exercisesPageLink ? acfChaptersFields.exercisesPageLink.uri : ''}
                  handleCarouselOrSummary={handleCarouselOrSummary}
                  lastSlideImage={<LastSlideImage />}
                />
              )
              }
        </Row>
      </Container>
    </Layout>
  );
};
export default wpHeatPost;

export const query = graphql`
  query($id: String) {
    wpHeatPost(id: { eq: $id }) {
      id  
      title
      content
      translations {
        language {
          locale
          name
        }
        uri
      }
      language {
          locale
          name
      }
      acfChaptersFields {
        fieldGroupName
        readMoreButtonLabel
        slidesContent {
        fieldGroupName
        text
        title
        introductionText
        }
        summaryTitle
        summaryText
        exercisesLinkLabelText
        exercisesPageLink {
          ... on WpHeatExercisesPost {
            uri
          }
        }
      }
      seo {
          opengraphUrl
          opengraphSiteName
          opengraphType
          opengraphTitle
          opengraphImage {
            link
            mediaDetails {
              height
              width
            }
          }
         }
    }
  }
`;
