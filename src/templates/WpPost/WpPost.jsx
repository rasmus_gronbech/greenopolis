// import React, { useContext } from 'react';
// import { Link, graphql } from 'gatsby';
// import Image from 'gatsby-image';
// import parse from 'html-react-parser';

// // We're using Gutenberg so we need the block styles
// // import '@wordpress/block-library/build-style/style.css';
// // import '@wordpress/block-library/build-style/theme.css';

// import Bio from '../../components/bio';
// import Layout from '../../components/layout';
// import SEO from '../../components/seo';
// import { GlobalDispatchContext } from '../../context/GlobalContextProvider';

// const BlogPostTemplate = ({ data: { previous, next, post } }) => {
//   const dispatch = useContext(GlobalDispatchContext);
//   dispatch({ type: 'SET_TRANSLATION_LINKS', payload: { translationLinks: post.translations } });
//   // dispatch({ type: 'SET_LANG', payload: { lang: post.translations.language.locale } });
//   const featuredImage = {
//     fluid: post.featuredImage?.node?.localFile?.childImageSharp?.fluid,
//     alt: post.featuredImage?.node?.alt || '',
//   };

//   return (
//     <Layout>
//       <SEO
//         title={post.title}
//         description={post.excerpt}
//         meta={
//           [
//             { opengraphSiteName: post.seo.opengraphSiteName },
//             { opengraphType: post.seo.opengraphType },
//             { opengraphTitle: post.seo.opengraphTitle },
//             { opengraphImage: post.seo.opengraphImage?.link },
//             { imageHeight: post.seo.opengraphImage?.mediaDetails.height },
//             { imageWidth: post.seo.opengraphImage?.mediaDetails.width },
//           ]
//       }
//       />

//       <article
//         className="blog-post"
//         itemScope
//         itemType="http://schema.org/Article"
//       >
//         <header>
//           <h1 itemProp="headline">{parse(post.title)}</h1>
//           <p>{post.date}</p>

//           {/* if we have a featured image for this post let's display it */}
//           {featuredImage?.fluid && (
//             <Image
//               fluid={featuredImage.fluid}
//               alt={featuredImage.alt}
//               style={{ marginBottom: 50 }}
//             />
//           )}
//         </header>

//         {!!post.content && (
//           <section itemProp="articleBody">{parse(post.content)}</section>
//         )}
//         <hr />

//         <footer>
//           <Bio />
//         </footer>
//       </article>

//       <nav className="blog-post-nav">
//         <ul
//           style={{
//             display: 'flex',
//             flexWrap: 'wrap',
//             justifyContent: 'space-between',
//             listStyle: 'none',
//             padding: 0,
//           }}
//         >
//           <li>
//             {previous && (
//               <Link to={previous.uri} rel="prev">
//                 ←
//                 {parse(previous.title)}
//               </Link>
//             )}
//           </li>

//           <li>
//             {next && (
//               <Link to={next.uri} rel="next">
//                 {parse(next.title)}
//                 →
//               </Link>
//             )}
//           </li>
//         </ul>
//       </nav>
//     </Layout>
//   );
// };

// export default BlogPostTemplate;

// export const pageQuery = graphql`
//   query BlogPostById(
//     # these variables are passed in via createPage.pageContext in gatsby-node.js
//     $id: String!
//     $previousPostId: String
//     $nextPostId: String
//   ) {
//     # selecting the current post by id
//     post: wpPost(id: { eq: $id }) {
//       id
//       excerpt
//       content
//       title
//       date(formatString: "MMMM DD, YYYY")
//       translations {
//       language {
//         locale
//       }
//       link
//       }
//       featuredImage {
//         node {
//           altText
//           localFile {
//             childImageSharp {
//               fluid(maxWidth: 1000, quality: 100) {
//                 ...GatsbyImageSharpFluid_tracedSVG
//               }
//             }
//           }
//         }
//       }
//       seo {
//           opengraphUrl
//           opengraphSiteName
//           opengraphType
//           opengraphTitle
//           opengraphImage {
//             link
//             mediaDetails {
//               height
//               width
//             }
//           }
//          }
//     }
//     # this gets us the previous post by id (if it exists)
//     previous: wpPost(id: { eq: $previousPostId }) {
//       uri
//       title
//     }
//     # this gets us the next post by id (if it exists)
//     next: wpPost(id: { eq: $nextPostId }) {
//       uri
//       title
//     }
//   }
// `;
