import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 200 300"
      {...props}
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path
            className="prefix__cls-1"
            d="M117.91 243.47v14.08c0 11.13-7.91 20.17-17.72 20.18-9.8 0-17.76-9-17.78-20.12v-14.09c0-9.72 6.92-17.61 15.48-17.62h4.49c8.62-.02 15.52 7.85 15.53 17.57z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <path
            d="M158.86 207.19l-.89-1a80.45 80.45 0 00-26.52-20.63 71.29 71.29 0 00-60.79-.56 80.54 80.54 0 00-26.91 20.16l-.91 1a2.25 2.25 0 000 2.85l.89 1a80.35 80.35 0 0026.52 20.64 71.38 71.38 0 0060.79.53 80.57 80.57 0 0026.91-20.17l.91-1a2.2 2.2 0 00.53-1.42 2.24 2.24 0 00-.53-1.4z"
            fill="none"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-1{fill:#2bb673}.prefix__cls-8{fill:none}.prefix__cls-4{fill:#199b69}.prefix__cls-8{stroke:#006838;stroke-linecap:round;stroke-linejoin:round;stroke-width:4.97px}.prefix__cls-9{fill:#fff}'
          }
        </style>
      </defs>
      <path
        className="prefix__cls-1"
        d="M74.45 247.28a24.12 24.12 0 01-24.12 24.12 24.12 24.12 0 0124.12-24.12z"
        transform="rotate(169.02 62.388 259.34)"
      />
      <path
        className="prefix__cls-1"
        d="M81.22 248.68a16.93 16.93 0 01-16.93 16.93 16.93 16.93 0 0116.93-16.93z"
        transform="rotate(131.06 72.757 257.145)"
      />
      <path
        className="prefix__cls-1"
        d="M117.91 243.47v14.08c0 11.13-7.91 20.17-17.72 20.18-9.8 0-17.76-9-17.78-20.12v-14.09c0-9.72 6.92-17.61 15.48-17.62h4.49c8.62-.02 15.52 7.85 15.53 17.57z"
      />
      <g clipPath="url(#prefix__clip-path)">
        <path
          className="prefix__cls-4"
          d="M117.61 238.45l1.3 10.94a75.49 75.49 0 01-38 4.34L79.58 243c-1.24-10.42 5.19-19.74 14.37-20.84l4.8-.57c9.18-1.11 17.62 6.41 18.86 16.86z"
        />
      </g>
      <path
        className="prefix__cls-4"
        d="M120.41 156.35A21.41 21.41 0 0199 177.76a21.41 21.41 0 0121.41-21.41z"
        transform="rotate(9.39 109.7 167.02)"
      />
      <path
        className="prefix__cls-1"
        d="M71.34 140.53a33.57 33.57 0 0133.57 33.57 33.57 33.57 0 01-33.57-33.57z"
        transform="rotate(-170.61 88.13 157.32)"
      />
      <path
        id="prefix__hrad"
        className="prefix__cls-1"
        d="M170 204.93l-7-7.21a88.64 88.64 0 00-125.67-1.16l-7.18 7.07a5.81 5.81 0 00-.15 8.17l7 7.2a88.12 88.12 0 0062.6 26.71 89.06 89.06 0 0017.4-1.52 88.09 88.09 0 0045.73-24l7.18-7.07a5.79 5.79 0 00.09-8.19z"
      />
      <g clipPath="url(#prefix__clip-path-2)">
        <path
          className="prefix__cls-4"
          d="M158.86 207.19l-.89-1a80.45 80.45 0 00-26.52-20.63 71.29 71.29 0 00-60.79-.56 80.54 80.54 0 00-26.91 20.16l-.91 1a2.25 2.25 0 000 2.85l.89 1a80.35 80.35 0 0026.52 20.64 71.38 71.38 0 0060.79.53 80.57 80.57 0 0026.91-20.17l.91-1a2.2 2.2 0 00.53-1.42 2.24 2.24 0 00-.53-1.4z"
        />
      </g>
      <path
        d="M157.62 208.66l-.87-1a78.59 78.59 0 00-26-19.89 70.81 70.81 0 00-59.51-.55 78.67 78.67 0 00-26.34 19.41l-.89 1a2.13 2.13 0 000 2.74l.87 1a78.59 78.59 0 0026 19.89 70.86 70.86 0 0059.51.56 78.7 78.7 0 0026.34-19.42l.89-1a2.11 2.11 0 00.51-1.37 2 2 0 00-.51-1.37z"
        fill="#beeda2"
      />
      <path
        d="M108.22 224.31a7.66 7.66 0 11-15.3 0c0-4.42 3.32-2.38 7.55-2.38s7.75-2.04 7.75 2.38z"
        fill="#006838"
      />
      <path className="prefix__cls-8" d="M75.89 201.39l.19 12.55" />
      <circle className="prefix__cls-9" cx={75.18} cy={201.43} r={1.31} />
      <path className="prefix__cls-8" d="M119.13 201l.19 12.55" />
      <circle className="prefix__cls-9" cx={118.43} cy={201.04} r={1.31} />
      <g>
        <path
          className="prefix__cls-1"
          d="M125.94 247.28a24.12 24.12 0 0124.12 24.12 24.12 24.12 0 01-24.12-24.12z"
          transform="rotate(10.98 137.959 259.282)"
        />
        <path
          className="prefix__cls-1"
          d="M119.17 248.68a16.93 16.93 0 0116.93 16.93 16.93 16.93 0 01-16.93-16.93z"
          transform="rotate(48.94 127.634 257.144)"
        />
      </g>
    </svg>
  );
}

export default SvgComponent;
