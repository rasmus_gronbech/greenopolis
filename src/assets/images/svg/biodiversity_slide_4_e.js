import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 1440 797"
      {...props}
    >
      <defs>
        <clipPath id="prefix__clip-path" />
        <clipPath id="prefix__clip-path-2">
          <path
            className="prefix__cls-1"
            d="M399.57 671.53l-30.18-5.2.98 15.93 27.04 3.97 2.16-14.7z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-3">
          <path
            className="prefix__cls-1"
            d="M351.56 664.49l-30.18-5.2.99 15.93 27.04 3.97 2.15-14.7z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-4">
          <path
            className="prefix__cls-1"
            d="M519.78 680.76l-30.61-.88 3.22 15.63 27.34.11.05-14.86z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-5">
          <path
            className="prefix__cls-1"
            d="M471.27 680.57l-30.61-.88 3.22 15.63 27.33.11.06-14.86z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-6">
          <path
            className="prefix__cls-1"
            d="M798.13 623.51l-39.96 7.3 8.49 19.39 35.49-7.4-4.02-19.29z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-7">
          <path
            className="prefix__cls-1"
            d="M684.52 647.2l-39.96 7.3 8.49 19.39 35.5-7.4-4.03-19.29z"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-1,.prefix__cls-14{fill:#88b8b6}.prefix__cls-4{opacity:.5;mix-blend-mode:multiply}.prefix__cls-5{fill:#dda862}.prefix__cls-6{fill:#56b071}.prefix__cls-11{fill:#74aaaa}.prefix__cls-14{stroke:#88b8b6;stroke-linecap:round;stroke-linejoin:round;stroke-width:2.81px}.prefix__cls-15{fill:#e9fcff}.prefix__cls-16{fill:#e1f4ec}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_1" data-name="Layer 1">
          <g clipPath="url(#prefix__clip-path)">
            <g id="prefix___1_FG" data-name="1 FG">
              <g id="prefix__tree_B" data-name="tree B">
                <path
                  className="prefix__cls-5"
                  d="M227.22 617.37h5.88v61.92h-5.88z"
                />
                <path
                  className="prefix__cls-5"
                  d="M216.08 622.21v-5.48h-3.87v5.48c0 11.65 9.88 17.54 19.86 17.66v-3.38c-8.07-.13-15.99-4.89-15.99-14.28z"
                />
                <path
                  className="prefix__cls-5"
                  d="M200.4 612.41v-5.48h-3.86v5.48c0 11.65 9.87 17.54 19.85 17.66v-3.38c-8.05-.12-15.99-4.88-15.99-14.28zM246.32 614.39v-5.85h3.45v5.85c0 12.42-8.8 18.69-17.7 18.83v-3.61c7.17-.13 14.25-5.21 14.25-15.22z"
                />
                <path
                  className="prefix__cls-5"
                  d="M252.06 622.37h-5.84v3.44h5.84c12.42 0 18.7-8.8 18.83-17.7h-3.6c-.14 7.18-5.21 14.26-15.23 14.26z"
                />
                <rect
                  className="prefix__cls-6"
                  x={174.65}
                  y={588.39}
                  width={111.01}
                  height={28.97}
                  rx={14.49}
                />
                <rect
                  className="prefix__cls-6"
                  x={193.57}
                  y={577.69}
                  width={73.16}
                  height={28.97}
                  rx={14.49}
                />
              </g>
              <g id="prefix__small_car-3" data-name="small car">
                <path
                  className="prefix__cls-1"
                  d="M402.77 683.75a4.59 4.59 0 002.77-3.57l1-6.8a12.17 12.17 0 00-10.28-13.81l-15.59-2.29-1.26-2.16-.71-1.22-10.35-17.84-.44-.78a3.76 3.76 0 00-2.68-1.79l-33.86-5a3.63 3.63 0 00-3.92 2.2l-7.07 17.71-2.76 18.85a4.6 4.6 0 001.53 4.14 4.59 4.59 0 01-1.53-4.14l-.37 2.53a4.6 4.6 0 003.88 5.22l78.82 11.56a4.61 4.61 0 005.23-3.89l.37-2.53a4.59 4.59 0 01-2.78 3.61zM354.4 638a2.32 2.32 0 012.64-2l7.49 1.1 10.56 18.83-20.53-3a2.33 2.33 0 01-2-2.64zm-28.74 8.27l4.82-12.53a1.76 1.76 0 011.86-1.14l15.49 2.27a1.86 1.86 0 011.57 2.13l-1.88 12.85a1.86 1.86 0 01-2.12 1.57l-18.4-2.73a1.78 1.78 0 01-1.34-2.44z"
                />
                <path
                  className="prefix__cls-1"
                  d="M399.57 671.53l-30.18-5.2.98 15.93 27.04 3.97 2.16-14.7z"
                />
                <g clipPath="url(#prefix__clip-path-2)">
                  <ellipse
                    className="prefix__cls-1"
                    cx={383.48}
                    cy={682.75}
                    rx={12.44}
                    ry={12.85}
                    transform="rotate(-81.66 383.468 682.748)"
                  />
                </g>
                <g id="prefix__hjul">
                  <ellipse
                    className="prefix__cls-1"
                    cx={383.49}
                    cy={682.67}
                    rx={10.92}
                    ry={11.28}
                    transform="rotate(-81.66 383.478 682.665)"
                  />
                  <ellipse
                    className="prefix__cls-1"
                    cx={383.48}
                    cy={682.75}
                    rx={6.72}
                    ry={6.94}
                    transform="rotate(-81.66 383.468 682.748)"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M351.56 664.49l-30.18-5.2.99 15.93 27.04 3.97 2.15-14.7z"
                />
                <g clipPath="url(#prefix__clip-path-3)">
                  <ellipse
                    className="prefix__cls-1"
                    cx={335.48}
                    cy={675.72}
                    rx={12.44}
                    ry={12.85}
                    transform="rotate(-81.66 335.465 675.713)"
                  />
                </g>
                <g id="prefix__hjul-2" data-name="hjul">
                  <ellipse
                    className="prefix__cls-1"
                    cx={335.49}
                    cy={675.63}
                    rx={10.92}
                    ry={11.28}
                    transform="rotate(-81.66 335.475 675.63)"
                  />
                  <ellipse
                    className="prefix__cls-1"
                    cx={335.48}
                    cy={675.72}
                    rx={6.72}
                    ry={6.94}
                    transform="rotate(-81.66 335.465 675.713)"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M392.93 662.51l6.26.92a6 6 0 015.06 6.8l-10.57-1.55a1.63 1.63 0 01-1.38-1.85zM380.5 658.45l-1.75-.26-.73-1.3-11.86-21.15-.64-1.14.82-.29.93 1.59 12.46 21.24.77 1.31z"
                />
              </g>
              <g id="prefix__small_car-4" data-name="small car">
                <path
                  className="prefix__cls-1"
                  d="M524.68 692.42a4.63 4.63 0 002.24-3.93v-6.88a12.16 12.16 0 00-12.12-12.22l-15.76-.06-1.55-2-.87-1.11-12.77-16.2-.56-.71a3.72 3.72 0 00-2.9-1.39l-34.22-.13a3.62 3.62 0 00-3.57 2.73l-4.5 18.53-.07 19a4.6 4.6 0 002.1 3.88 4.6 4.6 0 01-2.1-3.88v2.56a4.6 4.6 0 004.58 4.62l79.67.31a4.6 4.6 0 004.62-4.59v-2.55a4.63 4.63 0 01-2.22 4.02zm-54.35-38.49a2.32 2.32 0 012.34-2.32h7.57l13.11 17.16-20.75-.08a2.33 2.33 0 01-2.32-2.34zM443 666.17l3-13.08a1.74 1.74 0 011.68-1.39l15.65.06a1.87 1.87 0 011.86 1.88l-.05 13a1.87 1.87 0 01-1.87 1.86l-18.62-.08a1.79 1.79 0 01-1.65-2.25z"
                />
                <path
                  className="prefix__cls-1"
                  d="M519.78 680.76l-30.61-.88 3.22 15.63 27.34.11.05-14.86z"
                />
                <g clipPath="url(#prefix__clip-path-4)">
                  <ellipse
                    className="prefix__cls-1"
                    cx={505.44}
                    cy={694.15}
                    rx={12.44}
                    ry={12.85}
                    transform="rotate(-89.78 505.397 694.142)"
                  />
                </g>
                <g id="prefix__hjul-3" data-name="hjul">
                  <ellipse
                    className="prefix__cls-1"
                    cx={505.44}
                    cy={694.07}
                    rx={10.92}
                    ry={11.28}
                    transform="rotate(-89.78 505.397 694.062)"
                  />
                  <ellipse
                    className="prefix__cls-1"
                    cx={505.44}
                    cy={694.15}
                    rx={6.72}
                    ry={6.94}
                    transform="rotate(-89.78 505.397 694.142)"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M471.27 680.57l-30.61-.88 3.22 15.63 27.33.11.06-14.86z"
                />
                <g clipPath="url(#prefix__clip-path-5)">
                  <ellipse
                    className="prefix__cls-1"
                    cx={456.93}
                    cy={693.96}
                    rx={12.44}
                    ry={12.85}
                    transform="rotate(-89.78 456.879 693.95)"
                  />
                </g>
                <g id="prefix__hjul-4" data-name="hjul">
                  <ellipse
                    className="prefix__cls-1"
                    cx={456.93}
                    cy={693.88}
                    rx={10.92}
                    ry={11.28}
                    transform="rotate(-89.78 456.878 693.87)"
                  />
                  <ellipse
                    className="prefix__cls-1"
                    cx={456.93}
                    cy={693.96}
                    rx={6.72}
                    ry={6.94}
                    transform="rotate(-89.78 456.879 693.95)"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M511.94 672.78h6.33a6 6 0 016 6h-10.69a1.64 1.64 0 01-1.63-1.64zM499.06 670.51l-1.77-.01-.91-1.18-14.72-19.27-.79-1.04.76-.4 1.15 1.44 15.34 19.28.94 1.18z"
                />
              </g>
              <path
                className="prefix__cls-1"
                d="M1141.56 398.81h5.4V389a3.44 3.44 0 013.32-3.55h55.58a3.44 3.44 0 013.32 3.55v9.78h27.72a3.69 3.69 0 013.55 3.81v5.07a3.69 3.69 0 01-3.55 3.81h-.54v294.11h-94.27V411.5h-.53a3.69 3.69 0 01-3.56-3.81v-5.07a3.69 3.69 0 013.56-3.81zm69.35 99.36a3 3 0 002.93 3h4.82a3 3 0 002.93-3v-17.34a3 3 0 00-2.93-3h-4.82a3 3 0 00-2.93 3zm0-47.11a3 3 0 002.93 3h4.82a3 3 0 002.93-3v-17.35a3 3 0 00-2.93-3h-4.82a3 3 0 00-2.93 3zm-50.07 23.41a3 3 0 002.93 3h4.82a3 3 0 002.93-3v-17.34a3 3 0 00-2.93-3h-4.82a3 3 0 00-2.93 3z"
              />
              <rect
                className="prefix__cls-11"
                x={1134.59}
                y={397.32}
                width={109.06}
                height={15.46}
                rx={4.45}
              />
              <g id="prefix__Bus-2" data-name="Bus">
                <path
                  className="prefix__cls-1"
                  d="M798.13 623.51l-39.96 7.3 8.49 19.39 35.49-7.4-4.02-19.29z"
                />
                <g clipPath="url(#prefix__clip-path-6)">
                  <ellipse
                    className="prefix__cls-1"
                    cx={783.22}
                    cy={644.84}
                    rx={17.05}
                    ry={16.5}
                    transform="rotate(-11.78 783.09 644.722)"
                  />
                </g>
                <g id="prefix__hjul-5" data-name="hjul">
                  <ellipse
                    className="prefix__cls-1"
                    cx={783.19}
                    cy={644.73}
                    rx={14.96}
                    ry={14.48}
                    transform="rotate(-11.78 783.052 644.62)"
                  />
                  <ellipse
                    className="prefix__cls-1"
                    cx={783.22}
                    cy={644.84}
                    rx={9.21}
                    ry={8.91}
                    transform="rotate(-11.78 783.09 644.722)"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M684.52 647.2l-39.96 7.3 8.49 19.39 35.5-7.4-4.03-19.29z"
                />
                <g clipPath="url(#prefix__clip-path-7)">
                  <ellipse
                    className="prefix__cls-1"
                    cx={669.61}
                    cy={668.52}
                    rx={17.05}
                    ry={16.5}
                    transform="rotate(-11.78 669.504 668.419)"
                  />
                </g>
                <g id="prefix__hjul-6" data-name="hjul">
                  <ellipse
                    className="prefix__cls-1"
                    cx={669.59}
                    cy={668.42}
                    rx={14.96}
                    ry={14.48}
                    transform="rotate(-11.78 669.466 668.317)"
                  />
                  <ellipse
                    className="prefix__cls-1"
                    cx={669.61}
                    cy={668.52}
                    rx={9.21}
                    ry={8.91}
                    transform="rotate(-11.78 669.504 668.419)"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M800.41 607.62l-7.06-1.85a4.1 4.1 0 01-2.71-2.31L781 581.77a4.11 4.11 0 00-4.59-2.36l-143.53 29.93a4.11 4.11 0 00-3.18 4.86l11.49 55.12a6.89 6.89 0 008.16 5.34l156.36-32.6a4.1 4.1 0 003.19-4.86l-5.51-26.44a4.13 4.13 0 00-2.98-3.14zm-141.07 23.3a2.07 2.07 0 01-1.61 2.46l-13.81 2.88a3.09 3.09 0 01-3.66-2.4l-3.68-17.65a3.1 3.1 0 012.4-3.67l13.8-2.88a2.08 2.08 0 012.46 1.62zm23.73-5a2.09 2.09 0 01-1.62 2.49l-15 3.12a2.1 2.1 0 01-2.48-1.64l-4.09-19.59a2.1 2.1 0 011.63-2.49l15-3.12a2.1 2.1 0 012.49 1.63zm23.74-5a2.09 2.09 0 01-1.62 2.49l-15 3.12a2.09 2.09 0 01-2.48-1.64l-4.09-19.59a2.1 2.1 0 011.63-2.49l15-3.12a2.1 2.1 0 012.49 1.63zm23.74-5a2.11 2.11 0 01-1.63 2.49l-15 3.12a2.1 2.1 0 01-2.49-1.63l-4.09-19.6a2.11 2.11 0 011.63-2.49l15-3.12a2.1 2.1 0 012.48 1.63zm27.73-6.8a3.1 3.1 0 01-2.4 3.67l-18.18 3.79a2.12 2.12 0 01-2.5-1.64l-4.08-19.58a2.12 2.12 0 011.64-2.5l18.17-3.79a3.09 3.09 0 013.66 2.4zm27.41-2.55L764.93 611a1.9 1.9 0 01-2.26-1.48l-4.17-20a1.92 1.92 0 011.48-2.26l17-3.53a1.89 1.89 0 012.15 1.13l8 19.2a1.9 1.9 0 01-1.44 2.64z"
                />
                <rect
                  className="prefix__cls-1"
                  x={642.83}
                  y={599.18}
                  width={38.21}
                  height={7.14}
                  rx={3.15}
                  transform="rotate(-11.5 677.673 613.267)"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 725.602 656.425)"
                  d="M646.68 654.33h158.17v4.36H646.68z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 648.201 673.128)"
                  d="M644.65 671.67h7.38v3.17h-7.38z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 659.507 620.511)"
                  d="M657.24 608.5H662v24.23h-4.76z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 683.235 615.563)"
                  d="M680.98 603.55h4.76v24.23h-4.76z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.5 723.827 621.233)"
                  d="M704.72 598.6h4.75v24.23h-4.75z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 730.692 605.666)"
                  d="M728.45 593.65h4.76v24.23h-4.76z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 660.2 623.767)"
                  d="M657.92 606.23h4.76v35.31h-4.76z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 683.928 618.818)"
                  d="M681.66 601.28h4.76v35.31h-4.76z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 707.656 613.87)"
                  d="M705.4 596.33h4.75v35.31h-4.75z"
                />
                <path
                  className="prefix__cls-1"
                  transform="rotate(-11.78 731.385 608.921)"
                  d="M729.14 591.38h4.76v35.31h-4.76z"
                />
              </g>
              <path
                id="prefix__lygtep\xE6l"
                className="prefix__cls-14"
                d="M1118 584.12h-.4v-65.58a5.84 5.84 0 00-5.85-5.84h-45.26a1.32 1.32 0 00-1.32 1.32v.76a1.32 1.32 0 001.32 1.32h13.84a1.32 1.32 0 001.32-1.32v-.7h27.08a4.34 4.34 0 014.34 4.34v65.7h-.4a1.25 1.25 0 00-1.25 1.25v13.26a1.25 1.25 0 001.25 1.25h5.33a1.25 1.25 0 001.25-1.25v-13.26a1.25 1.25 0 00-1.25-1.25z"
              />
              <path
                id="prefix__lygtep\xE6l-2"
                data-name="lygtep\xE6l"
                className="prefix__cls-14"
                d="M1409.79 576.3h-.4v-65.58a5.85 5.85 0 00-5.85-5.85H1358.28a1.34 1.34 0 00-1.33 1.33v.8a1.33 1.33 0 001.33 1.32h13.84a1.32 1.32 0 001.32-1.32v-.71h27.07a4.35 4.35 0 014.35 4.35v65.7h-.4a1.25 1.25 0 00-1.25 1.25v13.26a1.25 1.25 0 001.25 1.25h5.33a1.25 1.25 0 001.25-1.25v-13.3a1.25 1.25 0 00-1.25-1.25z"
              />
              <path
                className="prefix__cls-1"
                d="M60.09 495.51h5.41v-9.78a3.44 3.44 0 013.31-3.55h55.59a3.44 3.44 0 013.32 3.55v9.78h27.71a3.68 3.68 0 013.56 3.8v5.08a3.68 3.68 0 01-3.56 3.8h-.53v294.09H60.62V508.19h-.53a3.68 3.68 0 01-3.55-3.8v-5.08a3.68 3.68 0 013.55-3.8zm69.36 99.36a3 3 0 002.93 3h4.82a3 3 0 002.93-3v-17.34a3 3 0 00-2.93-3h-4.82a3 3 0 00-2.93 3zm0-47.11a3 3 0 002.93 3h4.82a3 3 0 002.93-3v-17.35a3 3 0 00-2.93-3h-4.82a3 3 0 00-2.93 3zm-50.07 23.41a3 3 0 002.93 3h4.82a3 3 0 002.93-3v-17.35a3 3 0 00-2.93-3h-4.82a3 3 0 00-2.93 3z"
              />
              <rect
                className="prefix__cls-1"
                x={53.12}
                y={494.01}
                width={109.06}
                height={15.46}
                rx={4.45}
              />
              <g id="prefix__chinese_house" data-name="chinese house">
                <path
                  className="prefix__cls-1"
                  d="M1060.25 555.89h-76.67a2 2 0 00-2 2v52.88a2 2 0 002 2h76.67a2 2 0 002-2V557.9a2 2 0 00-2-2.01zm-56.79 39.82a2.82 2.82 0 01-2.81 2.83H990a2.83 2.83 0 01-2.82-2.83v-17.94A2.82 2.82 0 01990 575h10.63a2.81 2.81 0 012.81 2.82zm50.9 0a2.83 2.83 0 01-2.83 2.83h-10.61a2.83 2.83 0 01-2.83-2.83v-17.94a2.83 2.83 0 012.83-2.82h10.61a2.83 2.83 0 012.83 2.82z"
                />
                <rect
                  className="prefix__cls-11"
                  x={989.31}
                  y={518.53}
                  width={65.58}
                  height={7.63}
                  rx={2.02}
                />
                <rect
                  className="prefix__cls-1"
                  x={977.3}
                  y={557.48}
                  width={91.3}
                  height={9.07}
                  rx={2.02}
                />
                <path
                  className="prefix__cls-11"
                  d="M1076.11 551.32L1059 538.77l-9.08-14.69a2 2 0 00-1.71-1H996.8a2 2 0 00-1.72 1l-8.89 14.55-17.09 12.69a2 2 0 001.2 3.64h104.61a2 2 0 001.2-3.64zM1009.16 612.8h23.07v-37.31a5.08 5.08 0 00-5.09-5.08h-12.9a5.07 5.07 0 00-5.08 5.08z"
                />
                <rect
                  className="prefix__cls-11"
                  x={964.03}
                  y={548.47}
                  width={120.33}
                  height={12.99}
                  rx={2.02}
                />
              </g>
              <rect
                className="prefix__cls-1"
                x={956.64}
                y={607.02}
                width={141.37}
                height={37.71}
                rx={6.32}
              />
              <g id="prefix__human">
                <path
                  className="prefix__cls-11"
                  d="M857.33 625.6l8.72-1.46-2-11.65a3.58 3.58 0 00-4.12-2.94l-1.65.28a3.58 3.58 0 00-2.94 4.13z"
                />
                <path
                  className="prefix__cls-11"
                  transform="rotate(-39.56 869.518 630.829)"
                  d="M867.96 620.09h3.33v21.54h-3.33z"
                />
                <rect
                  className="prefix__cls-11"
                  x={874.07}
                  y={637.09}
                  width={5.13}
                  height={1.7}
                  rx={0.85}
                  transform="rotate(-39.56 876.521 637.9)"
                />
                <path
                  className="prefix__cls-11"
                  transform="rotate(-75 856.908 633.49)"
                  d="M846.13 631.82h21.54v3.33h-21.54z"
                />
                <rect
                  className="prefix__cls-11"
                  x={854.35}
                  y={640.75}
                  width={1.7}
                  height={5.12}
                  rx={0.85}
                  transform="rotate(-75 855.213 643.308)"
                />
                <rect
                  className="prefix__cls-11"
                  x={857.8}
                  y={601.67}
                  width={3.77}
                  height={7.2}
                  rx={1.27}
                  transform="rotate(-9.52 859.74 605.292)"
                />
                <path
                  className="prefix__cls-11"
                  d="M861.55 606.47l.71-.12a1 1 0 00.83-1.17 1 1 0 00-1.17-.83l-.71.12z"
                />
                <path
                  className="prefix__cls-11"
                  transform="rotate(-9.52 859.554 608.965)"
                  d="M858.4 607.67h2.22v2.47h-2.22z"
                />
                <rect
                  className="prefix__cls-11"
                  x={847.37}
                  y={616.45}
                  width={16.61}
                  height={3.25}
                  rx={1.47}
                  transform="rotate(-69.52 855.683 618.072)"
                />
                <rect
                  className="prefix__cls-11"
                  x={865.54}
                  y={608.86}
                  width={3.25}
                  height={16.61}
                  rx={1.47}
                  transform="rotate(-60.62 867.124 617.16)"
                />
              </g>
              <g id="prefix__human-2" data-name="human">
                <path
                  className="prefix__cls-11"
                  d="M282.42 661.26l8.79.84 1.13-11.75a3.58 3.58 0 00-3.22-3.91l-1.66-.16a3.59 3.59 0 00-3.91 3.23z"
                />
                <path
                  className="prefix__cls-11"
                  transform="rotate(-24.56 292.856 669.43)"
                  d="M291.27 658.75h3.33v21.54h-3.33z"
                />
                <rect
                  className="prefix__cls-11"
                  x={295.31}
                  y={677.33}
                  width={5.13}
                  height={1.7}
                  rx={0.85}
                  transform="rotate(-24.56 297.805 678.085)"
                />
                <path
                  className="prefix__cls-11"
                  transform="rotate(-60 279.972 668.775)"
                  d="M269.19 667.11h21.54v3.33h-21.54z"
                />
                <rect
                  className="prefix__cls-11"
                  x={274.93}
                  y={675.25}
                  width={1.7}
                  height={5.12}
                  rx={0.85}
                  transform="rotate(-60 275.786 677.824)"
                />
                <rect
                  className="prefix__cls-11"
                  x={286.35}
                  y={640.34}
                  width={7.2}
                  height={3.77}
                  rx={1.27}
                  transform="rotate(-84.52 289.956 642.231)"
                />
                <path
                  className="prefix__cls-11"
                  d="M291.44 643.87l.72.07a1 1 0 001.11-.91 1 1 0 00-.92-1.11l-.71-.07z"
                />
                <path
                  className="prefix__cls-11"
                  transform="rotate(-84.52 288.849 645.705)"
                  d="M287.6 644.59h2.47v2.22h-2.47z"
                />
                <rect
                  className="prefix__cls-11"
                  x={274.46}
                  y={651.94}
                  width={16.61}
                  height={3.25}
                  rx={1.47}
                  transform="rotate(-54.52 282.779 653.576)"
                />
                <rect
                  className="prefix__cls-11"
                  x={292.48}
                  y={647.35}
                  width={3.25}
                  height={16.61}
                  rx={1.47}
                  transform="rotate(-45.62 294.08 655.64)"
                />
              </g>
              <path
                id="prefix__Ground"
                className="prefix__cls-1"
                d="M1526.78 859.34L14.42 832V620s328.28 85 504.26 81.75c105.33-1.92 347.85-68 564.78-101.4C1229 578 1466 587.69 1466 587.69z"
              />
            </g>
          </g>
          <g id="prefix__Clouds">
            <g id="prefix__Cloud">
              <image
                className="prefix__cls-4"
                width={124}
                height={85}
                transform="translate(187.36 38.36)"
                xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAABVCAYAAACLvYBAAAAACXBIWXMAAAsSAAALEgHS3X78AAAMVUlEQVR4Xu2dbXfiOBSDFWinM7v//7fuvLSwH4oGRdG1HaAt0OocHzshMbEf69oO3dlpv9/jS59Hm94FX7ovfQH/ZHroXXALmqZp6l0zov0nmN+mW2vjCXD9+lUNvrdBcBPAO5BHBgCvGWlsec09wL9q4AVoP9c7bik13s8trrll8FcJPICujvV8VW5JG5/K1eevJ66x8zq6KuAd0A65l3tZ1YLby2flW4N+NcAbsKdOufpMVc3hCtPL6Zzf81e3Av4qtmUGuwe3l/zeSi3IreTaA69tuAXoH+rwBmjmnjYDZQeflGDvBsrVAJh14jWD/zDgHVdXoDehrMfp3iSHtgv5Lhz7PVoXJL9a6B8CfBB2AqtpG84l8Pod7mqHuQPwIudaqev2a4T+7nP4AOwEeit5VU7g9Tuo5G4FrbmX9Tl1kGjd0yG/ynn9XYF3YFegU3oozjv0FNYVtLqacJ/l+OVwzDodvIZ1LV8t9HcDHmDzWOEkNxOwp62VW9BV7mwF/iw501bKDp7aHc6r2wGBDlxHiH8X4AOwK1cr4EfJH+28u34U+Islgv0T8u0h9zqT2x3s1YB/c+AnwlaYhPtNjr9hCV7d7i5X+bytsBXwHwC/MYfNOp9xbIs6Hcgun6Q865P3hv+mwE+A7Y5WuN9C4nl3OuubMP/etFBzZ/+W/OGQp6jhScHrnD7h+L0z8MD7u/7NgBewNVUh/BFz2E+SP9k5BV+5vHJ4cjdBM/2yOqvo4W1N4X2SMvXu4C8OfGDbNQKbIJ8kfbdcwXt4Z538Lio5XGET+C/MYTNqVA7Xud1X8XvMwVd6l3n+osDPgK3ztMJWyJ7U8Qpd6/WFW1qwJXd75GBdrdCu0Fn/ZN9JiBrio+vfCvxFgBtoIMNmJ2nnpflaHc30w3KHrnO5h19/Nt+OOfBfWE4TXt/GEtvIxdzL4Tz39grXU6U3AX828EFXV85W2HQrQf4okoJXl7cAqQggObwFO83f3jYm1r3BfIB5iNdjdfvC/dN0mRc4ZwEfhO2dMersHwD+CXkLuIfgBLwV0r/hFbrX5bBTm5hza6eLQodeud5X9tRft58L/WTgjS2XgmaewrjP2R7CCViTO73l8Ap4crhuyRLsFvBtSKyT9XOAJfA6AIBlqJ+5/VzoJwEf2HJ5CG85uwrjDttd7nN4Aq7PA8ydpC6nw1Mda2A/4HXQ6EucB/ked7wPggnzME/HU2dDXw1cYK8N4cxTGB+B7Q7Xe6twnhyegD/i6HB39QhwDhK+rOHg0XfyL5L7HP8iuUIHsuNPhr4KeLEaXxPC3dk+Z7dgK/DeCl1hV8A9rOugTGHco1ZqG+d/Rgx1uk4fL5ZvcISt0IGly4EzoA8D74Tx3sin+9I+W2GPuFvDeVqsjQB3l1eOrgZzah9h89WsJl0nPFvaSpnfQ+jUxaAPAV8Bu+oMd7WuyAlR5+g0byfY1RZKIemza5gk8Cp0twYzc20f26jv4h16Ggh/pG7u4wkdOD7/RaAPAReNwPYw565OL1XU3Qr9hyWHXb1scdjMHTjDuna23p/K2k6F/YRXl1fAfxdl1kfw2r8uQmc7JikPqQs8LNJYVuhrXa3OVtBeTmE8hfIHHJ+jcrdqf7hGV8Z+XzW4E3A6+wnH17MJMtOjlHWgtmATLJ9Vzw27vAl8YJHmDtdOIBhC9hDuUFugW7B1GknAgKUTOI8nsAjnRoE7aM39VzjO+WxDikwtaYjvgqa6Dj+o1wHu7OToBLlK37GErXO2z9s68BJAWHmPDF2vdfgt4JWjK9i87yeWsIH5swDz59VnhhzvR1xeAg/uBurGb5HDeAU5ubkFOm2/WvN21XGqCccO9PMOuwVcF2qeHLq+q++52yEn6JoPacThE5YNZ57m7gQ7LcZ8q8X8yZJ2UBXGW6F8RJtDrvfymJ9XwP/g9Rl95Z3mbo9Syd3AEeAIdFXX5SPAqTTaN1jOZb4w6+2r1dkOOoVwXaBVsHug1R3UBrXbq0H+jNdn0311Bd1/iWvN2wS6k3wnx5qA5XOXisBtZZ4c3gpvOnerw/8B8C+WwHtztc/Xac7uOVTFzqkGxeZYLNuu7fa3Zg6fwL+h/pMpPouC1uTv3DeS+0uapstHHZ4arqH8AfO9sUJ02P/iOAh6sL1zzoWt51vgvc4KOF/NPkjZwf9Bno5YF5Vg+w8uCp4RacK5Dje52yt3u8PTQi2Fcw3jvYWZOuIU2CrtqAq6fj5h7i4C31nu0H06cmfzOxyyDho9xxdFfA5texf8AngI51pW2OryBDyt0nXBpu5Oi5leCE/PyeNRVdB5ng7SjmVHq8vcfQqHb9N83qb4PQ7VpwcOGF7n0HVNUob1UYd7cofrHO4LN12Be1lD+cgqXN0NK/N4rSroKg+f7GhdWG0xB76VPIVxdXY1FTBxN6B1ce7WtN7hhRy2urvl8u9YwvfPOEg87G0seeMg+SXlbtFzmhS+gtdwT+DaBlg9CtoXedzjV/3i/dFVBTw5SGE7dF1R6+LN99TV/pp1VM5e3bAVarnCv0/BA3NwCp7hvgLDe3bIsPVtXG+XwmijLi/dPgMefgb1MislbObJ5a30iGWDRmG3oHdD2qC0w7QT/XMdAOp8h6D36ipcQT9h7mz2UwW76pO/z57m8V5I9w5nY9ThhK1zucPXXK9x0D3YLgdxrkYGTGsAaHKpqzlvs1/4C1pydeqnqo90oEb1gKsq8PogDr1KPciQHJh3sHf2CKRTNVJ3GgDAa3tUhF311zmghzUCPI2ijeQJPBuTyunB2TkJcpqTLu3sSj3g6XMfmD2TpH5z2NrP2v+rwbeApwrSw7fSyAOr3MnpvJ77aPkz7C1Xtcyi/dXrU4ftcnPM1HP4JLmXR+CnEZgeUuc+X3FSQyP4HVV1qkLnVs0T5f3nfemfVf2p5aYResBd3uk9kCpvtK5Wea+HQVfvO95DI5GlamsFv6pT25v6ZHV/rAV+irzB+kKCb4v2mI9k4ITGXJHc5fo2TV/BVgPhzXQucH24yr2+73w45Bu5j3PXmohxzXLgOtD9BxEfCHqPwz97MFwCeAU4vRt+OOSEqduUzwRc/zomDYCd5B4JztJa4O5ih51+5eHrQm4zFHZ611zN37emFPEcuP6lazUAPOz7FLBKPeA6UlNy2P5DAN8gETbr2mG+19QVKXB/wNlm7R++M/e/alXwac6vWEDyUi3geyw7vgJd/QBAqDpf7w7X6o8lyd23DD0ZRU2h/fTzkH5JSq6vwLua0HsOB9rO1nlnLWx9k0R3X/N2rKeq89nuPZbzOPvqJ4D/UEP3+b1yelcjwIE2bHe2wlTYeq2H82r+vgXQLu34qt88rCt0uj3N7y3oQ+oB18pGgdPVFWz+SFC9VwduE7Sr128eEX/hCFyhjwCHlUuNzOE+SvXhfYHmL094La/hT6T8kcBfIyLkt6h9yHX+JTxdqfegs68VdnJ5E/oM+H6/3x/+CEJhJ+g+d28wdyvkWoc9Es5vGTblznOXu1k8vKew7nN5BXsPIP7bbr2QDswfdsLyoZNLeY8vUBR2azt2b8CBuSt1OnToCj45XEN6Cu1NjQAHssOnkHitDgiHzVB+j9sxV+VyQnvGHLqDTws3he4O72oB3MI6xWO6nE5X8JBrFHjvj/AcOKx8q9L+c+DaR+50Ba/HLXf7d5T/VOcahzMn9JdwjTYobcFSKL9Xd1MOJIV2ha7wNU8h/XyHm7wifeh0rQNPoNXZaXV+j3LDVE4neB8Aej6F82HwEbiE9cVHyLD5mY7cLV4fMv3ZTgv2PYFXAD3oDr5KKZzPdO5/H55uduju7hcst2opjFfuvgfoKToy1/5KbvdyNXevcjfQAB5c7hUSun7h5nCegLmoa83ZCe49Auc5B78LeZX0ugi692+8dP9XlOGf7ZpC2ljZj9M9WqeX703aycnpDl/Bp88c9h7owwbGQjq1xysUf/hJ8g2OD5cgp8HjuifwCcA+5K3koBHKw+o6HED135y547WcAK919S2Db3WqG4Z5awCkazQfcjcwCBxYQAdqt7acPAr73lVB97z12V+NwgZWAKcG/gtTLbcAfwHPxwlqLK8BTa0GDkS3A6fB/YzQqw4fGQTHE6eAw4nAqQI88AV4jUYHwOvJc4DhTOCqBnzVyDWfTV0A50JWXQy4ahD+lxq6JGTVmwDv6WtAvB3Qnj4E+Jc+Tht86VPpC/gn0/+GA8quD1M0wQAAAABJRU5ErkJggg=="
              />
              <path
                className="prefix__cls-15"
                d="M204.72 89.36A19.39 19.39 0 01222.39 70a19.4 19.4 0 0118.75-14.48h2.55A19.4 19.4 0 01262.43 70H268a19.42 19.42 0 0119.4 19.4v2a18.93 18.93 0 01-1 6 3 3 0 01-2.89 2.07h-75a3 3 0 01-2.88-2.07 19.21 19.21 0 01-1-6v-1c0-.33.09-.7.09-1.04z"
              />
            </g>
            <g id="prefix__Cloud-2" data-name="Cloud">
              <path
                className="prefix__cls-15"
                d="M502.14 244.07a12.67 12.67 0 0111.53-12.61 12.66 12.66 0 0112.24-9.46h1.67a12.65 12.65 0 0112.22 9.4h3.63a12.68 12.68 0 0112.66 12.66v1.32a12.58 12.58 0 01-.62 3.93 2 2 0 01-1.89 1.36h-48.93a2 2 0 01-1.88-1.36 12.3 12.3 0 01-.63-3.93v-.65-.66z"
              />
            </g>
            <g id="prefix__Cloud-3" data-name="Cloud">

              <path
                className="prefix__cls-15"
                d="M763.45 99.26a25.08 25.08 0 0122.84-25 25.07 25.07 0 0124.25-18.72h3.29a25.07 25.07 0 0124.22 18.62h7.18a25.11 25.11 0 0125.08 25.08V101.85a24.66 24.66 0 01-1.24 7.78 3.94 3.94 0 01-3.73 2.68h-96.92a3.94 3.94 0 01-3.73-2.68 24.66 24.66 0 01-1.24-7.78v-1.3-1.29z"
              />
            </g>
            <g id="prefix__Cloud-4" data-name="Cloud">

              <path
                className="prefix__cls-15"
                d="M1296.47 76a11.31 11.31 0 0110.31-11.28 11.34 11.34 0 0111-8.46h1.49a11.33 11.33 0 0110.94 8.42h3.24A11.34 11.34 0 011344.74 76v1.18a11.18 11.18 0 01-.56 3.52 1.78 1.78 0 01-1.69 1.21h-43.78a1.78 1.78 0 01-1.68-1.21 11.18 11.18 0 01-.56-3.52v-.59V76z"
              />
            </g>
          </g>
          <ellipse
            className="prefix__cls-16"
            cx={972.06}
            cy={172.94}
            rx={4.55}
            ry={43.96}
            transform="rotate(-80.55 972.107 172.916)"
          />
          <path
            className="prefix__cls-16"
            d="M996.17 196a16.13 16.13 0 00-10.5-6.55l-28.07-4.67-1.83 11a8.73 8.73 0 007.23 10.08l31.74 5.28a3.74 3.74 0 004.28-3.06 16.1 16.1 0 00-2.85-12.08zm-18.08 1.61a3.45 3.45 0 01-4 2.83l-1.72-.29a3.43 3.43 0 01-2.82-4l.72-4.35a3.43 3.43 0 014-2.82l1.72.29a3.43 3.43 0 012.83 4zm18.2 6.51l-9-1.49a7.4 7.4 0 01-6.08-8.52l.33-2a1.84 1.84 0 012.13-1.52l3.61.6a5.82 5.82 0 012.24.85 20.28 20.28 0 014.09 3.52 16 16 0 013.79 7.44 1 1 0 01-1.11 1.15z"
          />
          <path
            className="prefix__cls-16"
            d="M916.93 178.04l42.17 7.02-1.89 11.38-41.35-11.96 1.07-6.44z"
          />
          <path
            className="prefix__cls-16"
            d="M912.52 173.93a6.64 6.64 0 105.46 7.64 6.64 6.64 0 00-5.46-7.64zm3.5 7.31a4.49 4.49 0 01-.7 1.78l-2.59-1.72a1.7 1.7 0 00.1-.32 2.18 2.18 0 000-.25l3.23-.65a4.54 4.54 0 01-.06 1.16zm0-1.77l-3.25.65a1.61 1.61 0 00-.32-.48l1.88-2.84a4.62 4.62 0 011.67 2.67zm-3.77-3.58a4.62 4.62 0 011.56.56l-1.88 2.84a1.39 1.39 0 00-.33-.1h-.25l-.66-3.29a4.8 4.8 0 011.51-.01zM910 176l.66 3.26a1.55 1.55 0 00-.48.32l-2.63-1.74A4.55 4.55 0 01910 176zm-.28 4.67l-2.9.59a4.71 4.71 0 010-1.59 4.65 4.65 0 01.42-1.29l2.6 1.72a1.59 1.59 0 00-.1.33 1.93 1.93 0 00.01.28zM907 181.9l2.86-.57a1.58 1.58 0 00.33.48l-1.58 2.37a4.59 4.59 0 01-1.61-2.28zm3.66 3.16a4.43 4.43 0 01-1.53-.54l1.58-2.36a1.68 1.68 0 00.32.09 1.06 1.06 0 00.25 0l.57 2.84a4.34 4.34 0 01-1.18-.03zm1.8-.05l-.57-2.86a1.58 1.58 0 00.48-.33l2.56 1.7a4.62 4.62 0 01-2.46 1.48z"
          />
          <path
            className="prefix__cls-16"
            d="M916.26 176.56l-4.36-1.22-4.8.93 1.28-13.58a1.92 1.92 0 012.15-1.79l2.69.45a1.84 1.84 0 011.5 1.65z"
          />
          <path
            className="prefix__cls-16"
            d="M965.59 181.57h16.23a6.26 6.26 0 016.26 6.26v.51h-27.4v-1.87a4.9 4.9 0 014.91-4.9z"
            transform="rotate(9.45 973.939 184.807)"
          />
          <circle className="prefix__cls-16" cx={911.43} cy={180.48} r={4.65} />
          <path
            className="prefix__cls-16"
            d="M912.73 181.3l2.59 1.72a3.14 3.14 0 01-.38.5l-2.56-1.7a1.58 1.58 0 01-.48.33l.57 2.86a3.88 3.88 0 01-.61.1l-.57-2.84a1.06 1.06 0 01-.25 0 1.68 1.68 0 01-.32-.09l-1.58 2.36a3.58 3.58 0 01-.52-.34l1.58-2.37a1.58 1.58 0 01-.33-.48l-2.86.57a4.08 4.08 0 01-.16-.6l2.9-.59a1.93 1.93 0 010-.24 1.59 1.59 0 01.1-.33l-2.6-1.72a3.65 3.65 0 01.32-.54l2.63 1.74a1.55 1.55 0 01.48-.32L910 176a6 6 0 01.61-.15l.66 3.29h.25a1.39 1.39 0 01.33.1l1.88-2.84a3.93 3.93 0 01.52.35l-1.88 2.84a1.61 1.61 0 01.32.48l3.25-.65a4.09 4.09 0 01.1.61l-3.23.65a2.18 2.18 0 010 .25 1.7 1.7 0 01-.08.37z"
          />
          <path
            className="prefix__cls-16"
            d="M915.32 183l.37.25-.35.52-.4-.27-2.56-1.7a1.58 1.58 0 01-.48.33l.57 2.86.13.62-.61.12-.13-.64-.57-2.84a1.06 1.06 0 01-.25 0 1.68 1.68 0 01-.32-.09l-1.58 2.36-.39.59-.52-.34.39-.59 1.58-2.37a1.58 1.58 0 01-.33-.48l-2.86.57-.62.13-.12-.62.58-.11 2.9-.59a1.93 1.93 0 010-.24 1.59 1.59 0 01.1-.33l-2.6-1.72-.36-.24.35-.52.33.22 2.63 1.74a1.55 1.55 0 01.48-.32L910 176v-.22l.61-.12v.19l.66 3.29h.25a1.39 1.39 0 01.33.1l1.88-2.84.08-.12.52.35-.08.12-1.88 2.84a1.61 1.61 0 01.32.48l3.25-.65.23-.05.12.61-.25.05-3.23.65a2.18 2.18 0 010 .25 1.7 1.7 0 01-.1.32z"
          />
          <rect
            className="prefix__cls-16"
            x={903.46}
            y={170.32}
            width={6.94}
            height={2.16}
            rx={1.08}
            transform="rotate(9.45 906.497 171.318)"
          />
          <rect
            className="prefix__cls-16"
            x={954.42}
            y={212}
            width={31.53}
            height={2.39}
            rx={1.2}
            transform="rotate(9.45 969.756 213.048)"
          />
          <path
            className="prefix__cls-16"
            transform="rotate(-12.16 980.604 211.409)"
            d="M979.8 207.47h1.37v7.81h-1.37z"
          />
          <path
            className="prefix__cls-16"
            transform="rotate(-51.08 961.818 208.285)"
            d="M957.94 207.59h7.81v1.37h-7.81z"
          />
          <rect
            className="prefix__cls-16"
            x={970.3}
            y={170.16}
            width={2.31}
            height={12.81}
            rx={1.16}
            transform="rotate(9.45 970.996 176.463)"
          />
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
