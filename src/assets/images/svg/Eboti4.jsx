import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 200 200"
      {...props}
      className="eboti"
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path
            d="M146.15 49.64c-3-14.09-11.42-22.64-24.22-24.73-12.06-2-22.18-.05-30.09 5.68-12.34 9-14.7 23.85-14.79 24.48a1.68 1.68 0 000 .67c.27 1.22 2.63 7.48 22.12 10.6 5.56.89 11.91 1.73 18.06 2.46 14.2 1.7 27.39 2.86 27.58 2.87a1.94 1.94 0 002.08-1.62 62.47 62.47 0 00-.74-20.41z"
            fill="none"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-17{fill:none}.prefix__cls-4{fill:#2bb673}.prefix__cls-5{fill:#199b69}.prefix__cls-6{fill:#fafcff}.prefix__cls-7{fill:#cfdae8}.prefix__cls-10{fill:#006838}.prefix__cls-17,.prefix__cls-18{stroke-linecap:round;stroke-linejoin:round}.prefix__cls-18{fill:#5eeaea}.prefix__cls-17,.prefix__cls-18{stroke:#c6edf5;stroke-width:1.06px}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_2" data-name="Layer 2">
          <image
            width={215}
            height={185}
            transform="translate(-4.47 9.35)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANcAAAC5CAYAAAC7g6qfAAAACXBIWXMAAAsSAAALEgHS3X78AAAgAElEQVR4Xu2dbXfisJKEC5LMzH3Z//9H9+6dmSSwH6DicrlaEoQQElzn6MgYW5alftQt2SSb/X6PVatWXV7b3gGrVq06Tytcq1Z9kB57B6y6TW02m03vmHO0X+cJF9NmbcuvoQZMl4IsGsIK2/la4bphBaASSCPHuFKnd/etoJ2mNSy8MRlQ79lOnx0O/Vxtv0mrtoLW1+q5bkAND9XKq+98OymBtLftkfzwYTWiqBWuT1THSzlA1ecEHD8nOVgOVbUvHc99h43VmGZa4foEFVBV4KS0bXynZXHbO7kF1c4+txI8XwGbtM65riwBqwXVNmxr7vtGIHNvU8G06+S+rdrz/lbIVriupuCtWlBtT0wJNEgOzD1MBdMufH4N+zeYnxshu3fAVriuoMJbOVQKycOJuZ6fAEteK0HlQL3atsOWIHuD+N692ArXB6rwVhVYhEXBeZR96XMF2FauqXKPlaCq0ot9VtA2mMOqulsvtsL1QeqARQgcEMKjeUoOGsvQstV7UaNgvUieEr/byjkEDFgBA7DC9SFqhIE+V3KvxPQU8ifMj1HQWuGhqoLLoXrGBNKz5Lr9IMc4ZHot6u4AW+G6sAqwNFxTqAiHQ/Sj2NbkcClgp8CVvJWC9Fc+/7WckD1jut7r8VosX3VXgK1wXVABLIdK51LqpRQkTT8xwfUDc9BGvRdVzbcULvVWDtOfY/4Xh+v/lcRrvmA+qAB3DNgK14VkYGlK3sq9lIL005J+p4B5eKhzN14Xx+23FTzMFx58nqVwKTx/QkpQK8yv0+Z9ArbCdQENzLF6UDH9kvwXMmQpNLwkXM+S6K3+APgt+W9M16/CUQ9JF4Dhm2uF63KqQkEHy6H6Jekflitw6r3SvMvhSmEhMIcrhYUaEipYv465ek6f71VgUbw2gMOg9J291wrXO1UsuTtY7q0cqJT0GIWL5fica2tJvah6Lp939cJC9VZaB/dcFVh6TUgOfPPwcIXrHerMsxJY6qkI0T8l/UNyBcznXCx3dDGDGl3UcLh+Afgv5mGpX7sFlu/TEPFbggWscJ2tAqxq4YLe6ieWUP3rmBwyDQ2ZfL51yjI80A8NFTBd0GA4qF6L99gKQ91j6ee3EPG7eq8VrvfJwUrhoM6vElSaFLAqJBwFK4WFQN976bMuwpVWKnlt3juvtx9MWsdvGR6ucJ0hm2cBORx8xHyORbAUpn8fk+7TeZcvZPg8qwVWy3ONAPaMwzUJV7WIMoOkUTaTA0Z9K7CAFa73yA3ZQ0KfZylc/w6JnstDwspjJLC2OGhjOZXgIgR7HKAiZE84AFZBleZZXibL87S1YwHg24WHK1wnKqwOaig4ApbC9T+y7SFhAiutDHpYVnktqgJMPc0r5l6yglrLVKA07UKuc75v671WuE7Q4FsYaXUwea3/kVznWzrXqryGz3VaYGnIRu0t13Bth0P5r8hAtTxWmrtp7p5MPRiA7+W9VrhOVxUO6ij/hPkiRuW5dL7FkFC9Vs9jaYLkVBUW6uc95ka+k31+zeSx1Nu9FOnZPj/Idb6t91rhGtTAw2J6LV0drELCtJ3mWdU8J3ksyoFSuRGrV9vLPuY0/OSteJ7CpSuN/vKvwsVjWfbMe30XrXANqPFMS8FSr0XA0oPitPSuq4MtsNxjQXKqBVclAgXMIaug4nEtqHQpn9tcJKH3YpjIa+wBfJvQcIWro8Y8ywF7xASGv4mhYP3Tkj8s9mdZ1VyngkuVPE31nYrf8fhUTgoJFSp/i/4HptVHejDelwLG8r+8VrgaCs+zgMkIksfSuZaDVUHlcyxfcj93fpVUeaD0HcM1Sm2l5bUI1j+wBOwvliuPurDxrbTCVaiYY6m38rlWWiHU9M+wr7Xk3vNY1AhULfW8lC5gPByPe0TttRSmX1i+8Ovhrt/btwkNV7iCLBRkrmDpyOtgeUiYUvUy7mgo6NuXUAsyBcxDwhcsw2G+Se9vmJzimb80WMAK10wNb8WkUGkoWK0SjoDlo7mvDKrBwbY/QmrcqT3ce3EO9cOSDx56r36fvNdvpRUuRKiY01sxT2DpiK0rhC2wTn2ORV3TADeYA0bvtceyLVJ7OGDqtVJoqAsbX95rAStcp/w13C3mhlF5q+S1FKrktSpv5XX7bLE9EmBPltxjaeoNJhsA2Gw2X/qv9d41XGGZnds6mlZzLJ9b6eqgg+Xh4IjHugW59+I+bxv3XgmydM9pzvVtdLdwDT6/SuGPT94JUVpqZ/JQsJp7JG91awZXea/KgyWv1fNc30J3CdfAC7gtoyFcvedYrWdZycC8LpD8M5W8F/e3BiGHSe+5GlT0vr9sOEjdJVxHVWA5VA6Wz7HSe4Lp7Yvktb7qqD3Sbtp+FWDuuQBrg6/8vOvu4ApvXSQDSVAlsBQuBazntRysZFy3BJp7r1MGplO8lpcPfGEPdndwHdUyjhZU1Twr/WTfPVdrrgXcFkyj8vZrebGUWoB9ed0VXMVD4uS1UgioK4P/lPxfIaWQ8AeWYKURW/NbEr0Xt1NysCrAPBzUtvg2uiu4jtJO1M51r+VL7dWqIAHTPHktDY30usDXNSoHK3kxH7zSdz7AfNX2mOlu4CrmWu651GvRcyWg/lVs+3OuX1JWayEDIb9VpfpWgFXeyr22JmqDLzzfAu4ILpMbhBrCI5bPsVL4l5bdFapWOJiMC5J/FY0A1ktfvQ1K3Rtc1WjpniuFhQrYv+Uzv9N5WfXQOHmsr64KMAen5bW8rG+he4NLlTrdAePDYgdMQVNvxTBQVwef0F8hA76GYW2Qf5KSAPO23Yb93xqyu4Br4KckBMtDw/Sak8/BHKzWu3QJqq8sHxx6KQGVyvgWugu4GmKHq+ciXA5Yes7loaDOsRysB7meGpPmX1VefwfKvVYLMC/jy74df09wVR2qBqBgMaVXnxy45LE8FPyuYFEOSdXGm5D3QPuS2vYO+Orq/JEZ7WT3YAkwBc33eRh4T2CpKlhSm1eAwbarfrxpfWu4Bn5WomA9hPQouYLm3k2PuzewWmBUg5cm/34GlX7+aoB9W7hO/FnJCGAj+5ORfEewHKa0vwWWt/kIaF8OsG8H1+YofrRUdbR7nxTeOVBuGG4UChVk+8sYxwlyCByUanDqDVBfGrBvtaBhUDHXzlYoUmjXS6OG4Ne/F6WBLAHVSvwT1/wTbpR+/hL/ifJbwDX4HEs7mh2ZFip01U8XKkahcsAg298RtDSQJbDYjqnN+ffkCZeDBRyAU+3Z77cK2ZeHa8BbVWClZ1i+1O6vLyXAElDfHSY35mpAc2+V2vzvMX/B5LG0XOYbTH+IVHWzXuzLwnWCt3Kw/LmVvt7Uez/wCUu4WiFhyr+TWlB5m1eDGf/riXosB+wF83b0sPEmvdiXhOsMb5XASu8N+hvu/vaFezEH657lA5qG3g7WL0yhIP+Zg8KifUmwXiXH8dib9mJfDq7GSqBDxQ5mvJ/A+on5+4L6k5IWYDScXnh4L6o8l8LlUPHfCHmo55D+PeaEjJ5LIbtJL/Zl4OqEgSkE9El0Gj2T1/JfFfvvs5Lnulfv1RrcdFBju/M/oeh/l/Q/HaCDI9OzbPNcejH996+qT/diXwKuThjoYFXeSuN8f8u9AszffK88lxuZe7DvCN0GGQztBy5i+L9rpbfaF+dWKXkxlgcUXuyzALt5uAbCwBQCslOfsIRKFy90fuUwtX5d3AsN70mEzPuEEcMrDm3mUPEch+qxSH8w92IbyenJytVE4Pph4s3C1QgDtWF9lHSodG6VvJVD5IsZChXTyKLGdwdMvRY/K1g7TAOdA6XnpajDk0YjTDznGXMPxnb3MHEP4Op/YPQm4eqApakKAXtQjQDlUFXPvLQ+CbDvDFrqJ4JCkBJYPFaBcai0rZ/kmFa7c5FjZzl11TDxJuESJbA0DNSO0AULfV7lEPlyu4LVg0rBesRUn42le5P20R5zuN7mP5i3kYaCPkB6e3sYThtgzvLoyQgZMAd8gysCdnNwFXMsBSt1RrUK6DBVz7L+IecSKEL1A8swRTuX9btHr7W3HDi0BXBom3RO6k8FqAdXChFZpl6DkPliB3AlwG4KrkGwvAMqqBymBFaaUylYrQ7VTr03sJL0vunBIDm/q/pUo5AKMu8Hh4v5M5a6eoh4U3AdNQqWLlhUUI3+fUGHqurMCqp7BUu9lq7+cUEDOLQh20Pbjv36aKkVHlYRhPaLJ0h+VcBuBq5iEcM7gWGazq0cqvSnpfXvC6YFi2pedU4H3gtYVA8wbReFyz1QBZrva/VN6iNgPge7GmA3A9dR2ihsJPdaBIuQqFdKfxVX37RIYOm86gnzjkthh3ccZPvewKJagG3lGO1bTe+By/up1T/UVQC7CbgaS+8OFkEgIAmo9E/oksca9VYOFkLu2/eoCjBfJezB5ZClz6d4LgeN+nDAbgKuo6oO0Ib3cJBw/Rvz/4/lf266BZZ3Gq85ClX6fK9KgFGV0WsfP2DZ5ykf8VwpPHR9KGCfDlfngTEbTr1WgisB5i/eOlhpNOyNfCtUfSWwuE+9WPI4PqBWsI16rxF9GGCfDtdRbsDe0AwLE1yV9+J3lcfSzmqFFMCyo0Y77l5FmHRb22wLLMLFreTa9wm2bfhcDYraj5X0WRgAXORVqVuBi0qAjXov9WI+z9Kl9spbaecg5L69qi22VcuLKYA71JB5St7KIa36qgKG128dc5JuDS5gOZrRw6j3qp5v6ZI8t0fASqPcCtVlRJh029t4ZNHDwdJj/PiqHxWafUizY9/rvT4VrsZ8yxs4eS995Unf0Pgln9MbFwrWBu3OSJ9XnS6HSLXF0ot5v4zA1PJY+5C3PBj1LsA+Fa5CPcDUizlsmnyZveexgHnHrFBdXi3I9Dv97LaQIPN+rJS8lSfgQoDdIlxUBVkCrZd0YpzAWqG6rjZYQgRMXoz7q5T6T/tRyxyBKXmyxe/BTtWtweWNo3mCTEHz5Mf0OoTXWXUdsa2TF1PIeGwFkfchVcHkvy+rQNP6nbV6eGtw7Ys8HdPSSEd4h6xgfY42mPo0DXSn9GGSg7WT7ZSrfc1WEE8F7NbgqpRGH029UcjlnTPSSas+TuoltC+SB1NVXguY20plN24/eh6Tln8SYLcIVwWS/+WgV0n+l4XSH0PxBtyEfNXnquqTrXyvx7bkcFVgtWCjveys3CHdElw+cqSbJkiani0naC84rBSmBluBul1VfVN5sdSHLZhewz4fgNNg/JaPeq9bgguoAVOonnH4+3WtxKX4F+QVQmAaDYEVtlsT+4PbquTFkt3ske2o57UcOJa7w4nzr1uDC8gN5XARsD8hjb6NoTDtw75VnyvvG+5zg9bP7nVa3qtKCpdDmoAu9WlwydsZlVtXsJgqsH5jemPD33r3ZXhgfk1+twJ2m0p9o1HHwzF3sBJgLah8Pu8ejInX6oaHHwqXvd4UD5Hcj3XAUlj4G/NXnPx3WtVrTnrdnXy36jaVAHPIqB5cI5Cl8FE9mXrVUheDqwFSy2gdLofMG0jhYghIr5XASiGhQ5Ymyqv3uj05YFtMA6N6lCfZbnkuXxjzxbAUJhJmfm56r3fBFYBKBtk7RoFK3gVYejAPDyvA0qtPCrRqXeC4fTlgajfAPERUbzMKl684Mz1gCXLXe50FV3ibvbU9+r16EuYKg4eJyYPpy7rqtarXnyA5sAL2FZT6xcPDFBYmsPQxTpUe5Tx6S9ahqZPgKqBq5SPH6LEKmELhkLkXI2DutVqrhQkyb7AVsNtU6hfazR6H/k5wcWqRVp45UP/CHC4e+3AsQ+2mGRoOwxVW9yowRvdXxzApYL44AUwNx5vXBqrmXak+Lh8F3xoR9TmrPle0Fw3dGMo9YT4g/8Jy7p4e53CgfsbyRXB6L7XFhbpwBW9VgeHGW3kJN+7qHL0ZAuaQcXSiF1PA/PjkuSC5KgEGrF7slsT+15x2Q7t4xAQWf/f3gsMK818cQNPHOfzhLacYrcinqy5cogoQvagbsVdoBLhUlgOm0ABLwKofSDpYlfaYrqvSjlz1+UqAAXPAaDev6P/AtlpxTgMzUzn3asJloWAFgF7cvYQadQ+09F0CzCHjscBysaNqIGAMMmACrOrIVbcj7RsClmznqUiPltx21EZnSvOuEq4OWF7hVjoFtFZeXZeNwPrqQsdfLL0WbFsbZC+5to3WZQXstpT6ROfIaj/JPh8l96ioGpQ1L9ULCyuwvHKJeCU/jQBa6ffkWj5vmAsd/AfVDhdVuXTufzjmW0yTZco7cdXnKPVh6mvdr7bs9rEp9lVllopwFQ+HHSyC9CR55WJ5vI8GDlgFUXWTVYP4HEyPV6mn8uTfEywtg9urF/t8eQRSRSS+TzUKkNsfEMprea5k0IRDwaomhf4CbRXDJshaYAHZ+Cket8cBLm+wBNKu2P90PIceDJjqpuL1uL3qOkp2UPXrrpH8GC9Hr6HbfsxMvbAQWIJFuAjQzyKNvu/XAqwCq9VgerMMD/W8VgNXkDGx3sASMq3fCtjHqmXo2pdc4Oq99pReeXLoeI1hLeAqnmup8StcBEn/2YH/bfbW76t8wlgBpqoa7kVyBeZVPvdGrpFEL7axnFq92MfJjZt9AtRQ+dsYXOjSNzP8jYwEmQ/cXdAqz6VG7YARLkKj/yur9S9RR8LDTcghuTYevZK/xrKV/WwgII9iWlYLwh5kLS+mn1edLzVmNXAmH0zVNvTlAn1orJ8dNLWfZANd9cJCD8t8MYNhIf9mu/93Ef1b7QwPfXHDPaMbqxqmwuWN9wfzsI3A8JwXzMFTT6eN2PN0j5gDlgYBaoXs/aqgAuZG74OnDrqE6TeA/xZJYSNkPcDe6tZ9t7Dzm6wUFjpc+p9G3IO1nnynUFATsGxEBYvQKlh6nDbOFvMXMiu4ep5My2t5Me4HVshOkRtry1s5WOxf/VEtwfq/kBww9WIVYF2du1o4Aph6MP/5fQVXy2sBS2g4KtETAocb5/cKG+diwOTBUoytcPk+TU+YvNeIF9N9K2S1RqBSsLS/qlDQwfrPMf0vasA8PPTBdeG9XL2wEJgbvHsvX9jQf+mj/9kxhYYeFlYeSw3S4dI3MCDfawNvMAduh+maT5h7ME8OWILtUbZZF3o0vQ/VCtlSLaiYq6dSW9DBVkNBBYtwKVhM/8EEmIaHGuG453qr77m/RHYDd9B8/qVeTEHT0JDHV17L4aJ4Q9qY6i3Y6IROrwP5jo20wdL994BK3k3viVDRm/nA4bp3yCqgdNs9lUPlYOnCBcNBgvMfSy24PCx0z9VVDy6VglUBlp59+dJ8CgvdU6mRERxg3rDPcq56Jn2+pl6NHfN8PJZl+/yr5cW4/ev4+QcmwNR7tbzYClkfqlO8VQLrtyQPB5kTqv8gw/XuVcMRuCpjUMAcMgVNYUtwVR5Lr5vgco/FxtafnCjAPJ9g7o77tpiHEy3A0vc6gqoXU8B0ELlXT5YMcR9yTztLChX7xJfaCYkvYChcuq2rhrpiqH2sYL3pvX9arTrZPU0LNMLWCgu1rCRtbAWLXuspJAUsgcjw8D1wMf045r03Uva4H8haQOn2KFSVt3KPpbA4WLrN79XTpWddyWOVUFEjcKlS4X6RBJqmBBfPUyOqPJcex05QoHRVUo2c5xFGNtwek/dKID0PJB73U8rVgeUSkH0VwM4Bip9HQkAHSx8E02MpOFXS1cFqCV7hYnqr80l/znq/3+/tWVdqiF5SJa+m0KlhqfdKooHxe85tkmd0L0nDVmNV7wUc6pK8UwsyHeV+Su4e7BKQ3TJgpwDF3FPyVDtMBq794GFgy2M5aAkqXcCowJrdYw8sYMxzVRB5Y/ioMwqbw8XjVDSsHSZPt8fSG6q3SoBpCErA2IAb5HDPR0uFSnP9q0GcZ3qI6pDpIFNBpmDdImAJIt1uAVXZ0Yi34sPhNMdKMDH88+V2XbwgWGoXasvJpku14BppjNeQkgvVCrlxjMBFbTGBkDyhe7HKsBVQjalH4dKO9sSV0b9YPt9Lddlh6ck0XAZuE7AWVD2gElSvkrcGtbQi6GD5tnqqlrdKdrwAa8RrAWOeCxiDSkcbr+SigkdtitylRuXeL83n0uJGy4uxnsC8Y3k/PbD+4PA8j51WAeY/wUnezNtnK9u3AlgCqwXVLuSVLbWg6oWCKVVQpblVslnaxUlgATVc2ompoSrX7YbZGgWoBJYbjhuVpxZg+tzLX8FSwFg/IHd2axT9B+Ydr4DpYwhNCv0r5pDtMfewtwTYPuS+3YLJvdRIO/+R3EPBBJiHf1wFHPVWaqtgfgpYQIBLFjUSYK3RRiucIKvcrBvJiMG0PFgCi3MgN2rWmWW9IgOWOl/hYs63UggYH6I7ZArbKw518Q51KWCfpQqsBFWCKQGlbdvzVlU4qDm3e1CpfaptRidwKlhAPyxMjaeNVHktv4kKsFH5SM1zfd6lYZbC9dOShwWs3wZzD6bGwONGDEB/NPobE2DMnyV/OW6nwYfi/ev2tb1XCyytO/tZ8xZQbjN/JXePlbyWJ/Vu2j8Jqt6gfxZUVA8uSolOo8+zJb+JF7RvQg1nRB4WtjyXvoblMXc1gjlges+8vwouwqSJXozgsZ1+Srl6TWB5j8B8VfEzpVCpTaTBqIKJbTgKVvJaDp0mLStBpW3uA/67wQL6q4We9zxXBZk2st5UqnxlPDp687OGhbpi6F7rF5bhQRq9tF7cDzlO79k9GOFhXkGm4ckz2u3hgPkx14JMbcDBSlAlm0gw8bOCUA1YCTIHKrUv21gH0FcsoZq17XvBAgq4iofJ2qipMSuwFLBkxEyjhqKhoRpeWtBQuFK90iim9wjZz3veWhluFOzkBBkXPrQuel33xlvZVg+mOqXtztE+5N5OOoC6HfwtttMxqR01VUAlT8X2fUXu5w/xVqqRsLAFVgsuB63lJSD5qKFsJPdFDYKVRtFR2Hm/sO0N5vf+jGlx5C8O102Q0Wi0Dt4GCpPe0w5TSHhqO11S2lduBw6L3jM/K0AVfD3AHChN2rZuc1fxVqoRuKjWiMWbqsBKN+s36J6yFxpy243xEYdrVIAlyJPX0v2a43jNFxwMX8FNkLkxaR20M33eyERvyaRtU7XTR0nbSAHj/ScgdHHBBxltF4dNj9c8gVXZmkPFegMC1qWhokq4jqHh20csDS95rhZglVHriDxqKAqWhlEc5R8xLXN78us76Ak0NWatKw2foBEwXeZPIyrLBaa6+0NlTXz25Wm0vc7VwhBlW8FyuHxpXOdIDoTbyQhko2BVfQzmHwUWMO65vFMdMB29vLGS5/AbVp1qNBVcOxwASzBVUHnyToKdx2u/4nBtbQcFzO+d8kUYXYhxD1e11zVV2QDvU8FKrx/5/CjZhw7UClgCMtlW8lhuvwA+FixgHC4qGWFqmKoR3Hv4TY+ApV5L9ylgLWBGofL0gkksg1IvRsh4jt4zz3FvpSubCcZbBov3SQD+YAJKfzfFB7uce/E+q7ZO0PhgV/Wt51cHCxiDax9yVtwbxF37KYC5RkAD5mDtMfdgT1g2bgVVq3NfMIcGVtbmWAbzZAwOFj3WD+QFj9RGCyO5gvxa3n46qGpYqD9MVO/lXmsEFm27llIbLdrsGmABHbgaS/LeAC3PVY1CleH0GhCYey89ZyvHpMZN9Xeg0sDweMy5msdyFRpggmuLbBy66PKE/LaIGt0r8nWA8ba6hKr284FV4dLwkN6LcKkdzAzfPvuK6Svm75G+YhpcPUVdCyxgzHNRvQbuGSgb1EdlN0DmpxjORnJ6MIaHT8fvKrC8zj5hZtIXfRWwZBx+T8DcW/n8wQefCqyrGUZDlQ0kwPS9P26rLWhfa2Jb7zBvb8r7kcnP2Ut+dY3CpZXUhq3AcsB0v4dMDq17ygqy5L3YuOrBgLmRVmDRKPi61G/MfxrCDk9lK0heZzUYbYfKo2u7JLA+xVCO8v7fY2kDOkipF+OCxjOmewSmNt3YNuQ62v4Om9tPSlre1dSFq3hLPhnqiOfy0dlHHW+MntLoxE7CcZ8vY2tdGZb5T0K4cudg6eiqYtnc3uJwDWCqz2sj7STX9qWuahSDSoD5YJUigWdMYGlbqmfXQYxtU/1UiKrAYhl7APF/F3+UunCZtNJqCNqwKXloqEal3ks1CpkC5vsJLDtOH9A+FUmBUm+lo6oahXckMB8sdABRkHyA8XJSm9yKtI4VYGmQ1YFVB8Et5n3C9tdrvGDeP+7hWmnWX9cC7FS4KAUsea+W11LAkmFpg5yjjeQKlwKm6THs20ruQLXqxXqn+6nuE5J/BWmd/d6SPaTEPmEf6copE9sfmOD6iwkw9W5Uq821367S3kNwFW9r6A2kUasXHo54L2DZMEnqvfZhn0KSPFG1PQqVSuubIEqfk0655i3IbaJKer9sb3qtHzi84MzQXL1XgsuBqa7n7X6V8HAILpMaRuW5LuG9zjUsNWwvY6TMlsGfIi8jlengjgDV+/4zpPfmg0c1ePJe6bX4aIK/Yvhx3M9BkjZF6LStRoDeYd52l+jjps6BC1iOUjqXaHktJh5XzTsguXqgU8TzKvk9OODd0c90Smele9F9yWueev8fJa+n76O8rbx96LUYinO+pb/YpocCDrbyjOWihtqgJ+1DThFO6ad36VS4aOTJAPWmepNa9VyV9/IOawGWOtFB9fp66nVOgqwHnGsEoARSMuLPhE3rqLnvr+rL73wuzNBQ/94IQ8MdJrgSWC+Sq309YOpLXncP4MNDw2G4iiV5N9r3hIYsJ40w7JheQ+wt53Y1EHh9Ut1aA8ApSkbXAqsCjd/dglr3ocfocb6fkOkCEz2YhoY7zENCYA6W2hy3H4/5A+Z9N2pP79IwXCYHy0eQkdAwGfGDle2N4EbljaNw9TxU6pA0CFSe7FzIqFNgqwz02qrq48kXhYDl8ZSf45A9YRpwXzCFiexj7UN9nsZfJdBzbY7AOxAAAAr+SURBVBG810fqXLgoBewVbePtgaWGywbQ7ST3UMwTXHot7xDtGN3HuvbA0jzVtWWIvl+PvyV5ndK9eHLAkrQPHTQCRqA411Kw2G8MJf+gfrPG2/hDn3mdBNeZS/IKlb9Tl0DTRtBRaoPaU+k267LHvD7MvS7+9sDIe39+7wj5KCh+nBtoOtZVQT1qNK068ju9L6+rP8aoHmVoWVWitOxH2QYmW3vG/K9r6Rs2CtcDDv2n9ahs6mI6CS6TG3MKuxJYashMT8fjdRWIZScjU7lR0/hTXbwOvZ+P87wqLDxFLXj8u3ReT+8xFjdq35fk9+FAJW8BzPvJ7Ya5gubl81wufvgra/6Wh3qu7bH8q+g9cAHLUWfEc7mnYPIHg3vMO8jlUGk9tMM8Hv9j2wqV/1JWPZcaQGvEPVUOGLfPgczrMVqvZPxJ1QDhQFWeS9tKB0CPDjwUp3gNzsmY0utrHhKmOo22z1k6Fy4dJdWYk1EnqNS4uSKUnl34w0JVgqqCy2HyPyqpuYOvIat6xXOg6gFTATQCFqV1GqmfG9kp16ogIwSp77TdXiylcJxtD0xlKci6APIgSb9P9XvTR827ToZrYEnevRfhUi/xG1OMzOXVBFYF1whYrAs7S39jlP4LRgoNHSwNCTX11DNYfl+Vla6j7e+A7IvcpefrZ24Dy2v7vbSgcmPWPtofj9XBj3aiCxO69O72ptdU0LaY25TXieq1+7t0MlymllF7OMaG4x/J/I251+KN8nwNExNc2ulq8FoH9VwtuPQPqNBreaiic4FRqEaVwElguGFpns5LZajYtl6ellnJjbQFGLDsH2qLQ19rJKO/pdNBd3s8R6MIVXX9BNXIPb5L74WL0oZLcy4Pyf6LKUbmCMNyeH7yXG44nhLgrINe2/8/bvWnv5LnUqMFgLefjtufRJgdMrA/gZBASZ/TsfuwT6XtmkDQe6nKbCm1Bc9/lc8+h9K5k863OeACh37QeZnXjaqguorOgqtYkq88l8JFb6XLpSxIwziFjuBR3tHp+hVcfzDBpf/dPXkuhctDQiB3Zk/p+ASB35ffo466WmY6B7ZNJbh0xN/L91WbpzqoEqDaluxbXlvnTroIsccU5WyP56Y+8np9qs6Cy+SN/SrJPZe6ew0Fed4zlvOwFly7kBMspgS4/mUih+sPlhNq91pvnacT4fAHfZIqI9Vr+EDhSaFI5TqQyeBaQCVP5vVK5es1Wvf5Kjmv6XMmBYu2RLi4z6OMqr8+RZeAC8iNr2DR3fsSqTfeXyzDxdTJ3rFufGzgKjTlfMtDw2oxI823Tu00PSfdh9a9Shom7WVby0yQep0VHjVk3e7BxTo5cOmafj7PhRwLLMHmOexDfVuDA6cuQvUGxHP67WydDZeFhmwINkbyXL8xH5kSWD8x92zuubyjvMNZHuvAhtYVQwfMFzc0LGRZer23zhlcvvUOTYaWwHqx5J5eQQNye3jbqBwoeg73ZpS2rxpxShVwbhu8D4eL4jm0I414dpjbli9GeV20H0b67d06Gy6TGswGS7j8+YOOSjoC8Tc87t38OhVcVScq5L64klYL3XC8c0ZVQZUMTkF6tsRwiOXssPRiqUzm2m7A0mv58nUrYtB+TcnbzWHb2fcEZR8Sj6N96NL8HlM92JctwLwNTu3Lk3UJuLSy2skvmI+KbgwK1l9MCx2EK50DzBtKO0w/a8cmoyVgOg/rLWZ4x58iPzdBpXXTQUDB4jk+8KRyfeTeYRLBUajS8yEf2LTOPlCx7dIcKKUXTHVKALDObBudi6eohwOlz8OqPoRtX1zvgssm8NooHI0UMB8JHS5/J8xDQp6n19FOqODSzvQR968lB8s7/U2DISFQQ1WBRWPV5WjIeRoapRDK79uNayPpISQND1P5rLd6DF0M8vbsQQZM9uJ9SfsgXGobPEajEgWsB9do/52td8El8pGADbPB4Sb9WO0kGlR62VI7l+e2APOOSYZWhV4vkmun+DVHO8WPT4CpYdBQfbUUmIzoBbVX1/vWe+U+rQtDQgfrEefDpfNWhawH2O54HYVA74PX6q0yaztWoSH7c6YTBsqT9G64gvfaHbfZYNx+OwVLuKq3mEfhUqN12By0F8k16XfNDjmjM7yOlcfio4oElo7eqX2SUVb3oSEh273V/tWgoHNXfWbYe+NF21vrlODSyEbvHZjgcsCYWgPlh+vdcIm0wgqZ7nOwOBKnjmUDagcz94bSfSlXwLTj1BB1v3ayX+dUOezJGNxzV2D5Me65WHcfLPRe6JHUc1V9sMH83rW9fFDQh/P+3DB5Em1rYA7Xq+S8ltbPBxW1J49ILt2fw7oIXOHhaYKL+72TPN73UdMNSLdTY7Vg8/RquXZA7IhBr+X11Dq5gXLRwoEhJDTgEbi8fDVibT+fb7n3YtkjcOlcpwKsms8qXMAU7TxgDk2aLmjdHDD3kLyOtgGA4f48SxeBS+QV1YbThnjAHC6HKoWEWoZu95JD4gD1gHq75js6Qq/lxulGA8zB+ovlSqofz3OYkofQtnO4FCw3YJa9l7LUQ6j30rkXQ0SdfyWDZ9nA5L3YXgTN7SQN5DqweKr69UN1Mbjspygqv7Et5g3nQHk46GV646Tt9JkpNXTa52UPqVhB5TVecbg/GstfLOcQCqCGjPrmSoJLz20ZF5DDQo8gWgbscLGeGiL680MN01LdgAnoHeZ2srXEY4F5+2odNXeIDyeeP1gO6WJwAagA2yM3xBZTw21CguQ8t5dX+9Ln3r63/J2doGWrcbp30GMIlv+EvbWaynMdMDUuyuFysHpl+/xGFxH0uWGac9FzVUbPNlE78QFY6wbM287L1W3v5w/VReEC0PNg3L+RzxVYlbTcCEPnuxKikL8HLK8L75cDyjOWcBEsXSFTsDQkTACwjDRy633zXAWqCglZtoPLelaQearmW27wCpfaSQUWpfeveUrv6ddhXRwuAD0Ppg2UPlNpdKo+X3z7PY1fLPCwwzc4GBmwBEuN1qGq5kRahkKQRnHvAwXVofVy95hD+yK5Alal5LUUAkrrCNne2T5XBRLLftt+T9+eog+BC5huoBMmjkCVlBrH9/U+x/0Xbni/bxoTkMGisT5iml+1lsnTAMTyElgUz/W5TPKIwLJMnze9DKTksVLdgKWNtOyFx+v2Aigsr/Hh2lzWlmrZSN6DqQKsVdnejZTfXxioNx3vmfei8wb1Fr6o4IsLHgomuDiAeVK4ILnD5ds9uCrI1KOlxHMSWN4HyUa8XrxvlZf39v1H9XOlD/NcroYni4d3vncNH3/tBgbeRmFel55LO5/G+oBpJVHnQhVY1QiuhquJ0vO3ljuwkPM9nNMQ7zXk+n3ypBVYI/vcjirIPqPPAeB6nitp4Fe7J+mzGrElucdNSMmbtVKCgGU7CLqd2iXVx6FN5Shcvl0lPc/T4QJF3zVspBpYph2fbA+fCte9KACm29uwnUByj6XlUBVgmgPLOlTlUQmKXWe7+h62PQzB6GA8Wt5Ha4XrSjLAmFdGXsGUzq00ChfzCi4H4pyEsH0zEHyUVriuqGJRpwdb9ZmqYKg+q04trwdMEybJvz1YwArXp2hg5dQ9UwLxFKVO9jJ6ZVag9PLZ9j1ARa1wfaLCHKIHnW9fUq1yW94wwbT4fE9QUStcN6IOaCP7LyUtvzKOtH+x7x6BUq1w3aAGVsV633+0otHcO0yuFa4vpAHorqYVpL5WuFat+iBtewesWrXqPK1wrVr1QVrhWrXqg/T/RNT/jKZcSPkAAAAASUVORK5CYII="
            style={{
              mixBlendMode: 'multiply',
            }}
            opacity={0.5}
          />
          <path
            className="prefix__cls-4"
            d="M134.66 129.4l.89 16.69c.52 9.75-7.23 18.07-17.31 18.57l-5.29.27c-10.08.5-18.67-7-19.19-16.76l-.89-16.68c-.53-9.76 7.22-18.08 17.3-18.58l5.29-.26c10.08-.51 18.68 6.99 19.2 16.75z"
          />
          <path
            className="prefix__cls-5"
            d="M134.77 131.52l.55 10.25a96.25 96.25 0 01-41.8 1.91L93 133.61c-.52-9.75 7.23-18.07 17.31-18.57l5.29-.27c10.06-.5 18.65 7 19.17 16.75z"
          />
          <path
            id="prefix__HAND_R"
            data-name="HAND R"
            className="prefix__cls-4"
            d="M155.82 143.61a19 19 0 002.26 14.74 18.79 18.79 0 0012.17 8.5 1 1 0 001.17-.74A18.93 18.93 0 00157 142.87a1 1 0 00-1.18.74z"
          />
          <g id="prefix__jacket">
            <path
              className="prefix__cls-6"
              d="M124.62 131.61l15.73.48 3.86 37.8-20.65 3.71a4.13 4.13 0 01-4.13-4.13v-6.88z"
            />
            <path
              className="prefix__cls-7"
              d="M127 132.04l4.72 10.59-5.22 1.9 3.68 2.6-10.74 15.46 4.81-32.65 2.75 2.1z"
            />
            <path
              className="prefix__cls-6"
              d="M109 131.32l-16.73 1.26L91 170.37l19 2.93a4.13 4.13 0 004.13-4.13v-6.88z"
            />
            <path
              className="prefix__cls-7"
              d="M106.56 131.74l-4.71 10.59 5.22 1.91-3.68 2.59 10.75 15.46-4.82-32.65-2.76 2.1z"
            />
          </g>
          <path
            id="prefix__hrad"
            className="prefix__cls-4"
            d="M188.38 106.83l-6.48-8.31A97 97 0 0049.15 80l-8.54 6.22a5.81 5.81 0 00-1.17 8.31l6.48 8.32a95.61 95.61 0 0062.62 35.85 98.06 98.06 0 0018.55.84 95.23 95.23 0 0051.58-18.18l8.54-6.21a5.81 5.81 0 001.17-8.32z"
          />
          <path
            d="M175 107.66l-.79-1.15a83.34 83.34 0 00-24.81-23.86 78.05 78.05 0 00-62.86-8.77A83.9 83.9 0 0056.08 90l-1.08.92a2.13 2.13 0 00-.4 2.8l.79 1.14a83.22 83.22 0 0024.81 23.86 78.05 78.05 0 0062.86 8.77 83.83 83.83 0 0030.45-16.15l1.07-.89a2.12 2.12 0 00.4-2.79z"
            fill="#217b38"
          />
          <path
            d="M174.81 108.93l-.79-1.14a83.37 83.37 0 00-24.81-23.87 78.16 78.16 0 00-62.86-8.77A83.77 83.77 0 0055.9 91.31l-1.07.88a2.13 2.13 0 00-.4 2.8l.79 1.14A83.37 83.37 0 0080 120a78.16 78.16 0 0062.86 8.77 84 84 0 0030.45-16.16l1.07-.88a2.14 2.14 0 00.4-2.8z"
            fill="#beeda2"
          />
          <path
            className="prefix__cls-10"
            d="M138.9 90.09a2.77 2.77 0 00-3.29 2.16l-1.6 8.36a2.83 2.83 0 005.56 1.06l1.59-8.36a2.78 2.78 0 00-2.26-3.22zM88.76 82.86A2.79 2.79 0 0085.47 85l-1.59 8.37a2.83 2.83 0 005.56 1.06L91 86.08a2.79 2.79 0 00-2.24-3.22z"
          />
          <path
            d="M126.5 106l-28.29 4.6s3.43 12.94 15.14 10.57S126.5 106 126.5 106z"
            strokeLinecap="round"
            strokeLinejoin="round"
            stroke="#006838"
            strokeWidth={2.72}
            fill="#006838"
          />
          <g id="prefix__hat-2" data-name="hat" fill="#fcf568">
            <path
              d="M146 70.55s-21.21 2.93-41 2.14c-13.45-.53-25.45-3.64-32.84-7.18-18.29-8.78 6.93-11 6.93-11s5.34 4.57 20.71 7.63 46.2 8.41 46.2 8.41z"
              strokeWidth={3.84}
              stroke="#fcf568"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <g clipPath="url(#prefix__clip-path)">
              <path d="M146.15 49.64c-3-14.09-11.42-22.64-24.22-24.73-12.06-2-22.18-.05-30.09 5.68-12.34 9-14.7 23.85-14.79 24.48a1.68 1.68 0 000 .67c.27 1.22 2.63 7.48 22.12 10.6 5.56.89 11.91 1.73 18.06 2.46 14.2 1.7 27.39 2.86 27.58 2.87a1.94 1.94 0 002.08-1.62 62.47 62.47 0 00-.74-20.41z" />
              <path
                d="M125.71 24.11a79.36 79.36 0 00-19.27 19.71A53.31 53.31 0 0098 68.92l-17.18-4.68s1.7-12.77 7.72-22.56c6.39-10.38 22.07-19.37 22.07-19.37z"
                stroke="#f9cf5f"
                strokeWidth={3.84}
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </g>
          </g>
          <g id="prefix__hand">
            <path
              id="prefix__HAND_R-2"
              data-name="HAND R"
              className="prefix__cls-5"
              d="M63.24 147.59a21.22 21.22 0 00-12.84-10.53 20.93 20.93 0 00-16.4 2 1.11 1.11 0 00-.42 1.48 21.08 21.08 0 0029.24 8.52 1.12 1.12 0 00.42-1.47z"
            />
            <path
              d="M29.32 165.31l24.78-2.62a4.48 4.48 0 003.9-5.95l-11.7-40.28a6.87 6.87 0 00-7-4.79l-24.78 2.61a4.48 4.48 0 00-3.92 6l11.7 40.28a6.88 6.88 0 007.02 4.75z"
              fill="#5eeaea"
            />
            <path
              id="prefix__HAND_R-3"
              data-name="HAND R"
              className="prefix__cls-4"
              d="M63.52 147.93a18.23 18.23 0 00-6.69-10.31 11.71 11.71 0 00-10.3-2.08.78.78 0 00-.43 1c2.28 8.9 9.9 14.46 17 12.39a.77.77 0 00.42-1z"
            />
            <path
              className="prefix__cls-17"
              d="M18 121.35l5.87 4-2.25 3.43-5.87-4 1 4.39 5.11 3.48 4.28-1.35 16.59 12.23a2.68 2.68 0 003.89-.35c.74-1.18.08-2.91-1.33-3.8L27.9 128.57l-.43-4.57-5.11-3.48-4.34.79s-.02.03-.02.04z"
            />
            <ellipse
              className="prefix__cls-17"
              cx={43.81}
              cy={141.39}
              rx={1.16}
              ry={1.39}
              transform="rotate(-53.62 43.817 141.383)"
            />
            <g>
              <path
                className="prefix__cls-18"
                d="M46.91 129l-1.27 1.94-13.07-8.89 3.15-4.8 9.71 6.6-.09.13c-.99 1.51-.28 3.76 1.57 5.02zM26 145.4l.43.3a1.57 1.57 0 002.21-.24l8.75-13.33-3.28-2.23-8.75 13.33a1.63 1.63 0 00.64 2.17z"
              />
              <path
                className="prefix__cls-18"
                d="M37.76 125.58l2.09 1.42-3.1 4.72-2.09-1.42 3.1-4.72z"
              />
            </g>
            <g>
              <path
                className="prefix__cls-17"
                d="M34.06 153.43a2.32 2.32 0 01-2.35-1.59 2.3 2.3 0 00-2.34-1.59h-.25a1.47 1.47 0 00-1.28 2 1.48 1.48 0 01-1.29 2 1.48 1.48 0 00-1.29 2l.07.24a2.28 2.28 0 002.34 1.51 2.31 2.31 0 012.33 1.61 2.3 2.3 0 002.34 1.59h.25a1.47 1.47 0 001.28-2 1.48 1.48 0 011.29-2 1.48 1.48 0 001.29-2l-.05-.2a2.29 2.29 0 00-2.34-1.57z"
              />
              <ellipse
                className="prefix__cls-17"
                cx={45.04}
                cy={157.7}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 45.048 157.705)"
              />
              <ellipse
                className="prefix__cls-17"
                cx={43.1}
                cy={151.16}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 43.117 151.166)"
              />
              <ellipse
                className="prefix__cls-17"
                cx={51.6}
                cy={157.01}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 51.601 157.022)"
              />
              <ellipse
                className="prefix__cls-17"
                cx={49.66}
                cy={150.47}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 49.66 150.478)"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
