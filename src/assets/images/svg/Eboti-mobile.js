import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 200 200"
      {...props}
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path
            d="M155.52 116.44l-.82-1A75.73 75.73 0 00130 95.66a67.12 67.12 0 00-57.18-1.35 75.82 75.82 0 00-25.57 18.59l-.87.95a2.13 2.13 0 00-.07 2.69l.83 1a75.52 75.52 0 0024.66 19.78 67.12 67.12 0 0057.2 1.35 75.65 75.65 0 0025.6-18.59l.88-1a2.12 2.12 0 00.06-2.69z"
            fill="none"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-10{fill:none}.prefix__cls-4{fill:#d9d9d9}.prefix__cls-5{fill:#199b69}.prefix__cls-6{fill:#2bb673}.prefix__cls-10{stroke:#116043;stroke-linecap:round;stroke-linejoin:round;stroke-width:5px}'
          }
        </style>
      </defs>
      <g id="prefix___1st" data-name="1st">
        <g id="prefix__website">
          <path
            id="prefix__main_shape"
            data-name="main shape"
            d="M7.6 31h184.8v150.85a11 11 0 01-11 11H18.56a11 11 0 01-11-11V31h.04z"
            stroke="#fff"
            strokeMiterlimit={10}
            strokeWidth={2}
            fill="#bababa"
          />
          <path
            id="prefix__topbar"
            d="M17.06 7.19h165.88a9.46 9.46 0 019.46 9.46V31h0H7.6h0V16.65a9.46 9.46 0 019.46-9.46z"
            fill="#fff"
            stroke="#fff"
            strokeMiterlimit={10}
            strokeWidth={2}
          />
          <circle
            id="prefix__dot_1"
            data-name="dot 1"
            className="prefix__cls-4"
            cx={180.06}
            cy={19.1}
            r={6.29}
          />
          <circle
            id="prefix__dot_1-2"
            data-name="dot 1"
            className="prefix__cls-4"
            cx={162}
            cy={19.1}
            r={6.29}
          />
          <circle
            id="prefix__dot_1-3"
            data-name="dot 1"
            className="prefix__cls-4"
            cx={142.4}
            cy={19.1}
            r={6.29}
          />
          <rect
            id="prefix__web_adress_bar"
            data-name="web adress bar"
            className="prefix__cls-4"
            x={13.01}
            y={12.81}
            width={94.37}
            height={12.58}
            rx={6.29}
          />
        </g>
        <g id="prefix__Boti_1" data-name="Boti 1">
          <path
            className="prefix__cls-5"
            d="M125.73 62a24.42 24.42 0 01-24.42 24.42A24.42 24.42 0 01125.73 62z"
          />
          <path
            className="prefix__cls-6"
            d="M74.06 54.88a31.59 31.59 0 0131.59 31.59 31.59 31.59 0 01-31.59-31.59z"
            transform="rotate(-180 89.86 70.67)"
          />
          <rect
            className="prefix__cls-6"
            x={83.35}
            y={120.6}
            width={33.56}
            height={55.93}
            rx={16.78}
          />
          <path
            className="prefix__cls-5"
            d="M83.35 153.88v-22.37h33.56v22.37s-6.22 3.23-16.78 3.23-16.78-3.23-16.78-3.23z"
          />
          <path
            id="prefix__hrad"
            className="prefix__cls-6"
            d="M166 114.47l-6.53-6.88a83.39 83.39 0 00-118.22-2.86l-6.85 6.55a5.45 5.45 0 00-.18 7.68l6.52 6.88a82.93 82.93 0 0058.52 26 84.46 84.46 0 0016.34-1.19 82.86 82.86 0 0043.36-22l6.85-6.55a5.45 5.45 0 00.19-7.63z"
          />
          <g clipPath="url(#prefix__clip-path)">
            <path
              className="prefix__cls-5"
              d="M155.52 116.44l-.82-1A75.73 75.73 0 00130 95.66a67.12 67.12 0 00-57.18-1.35 75.82 75.82 0 00-25.57 18.59l-.87.95a2.13 2.13 0 00-.07 2.69l.83 1a75.52 75.52 0 0024.66 19.78 67.12 67.12 0 0057.2 1.35 75.65 75.65 0 0025.6-18.59l.88-1a2.12 2.12 0 00.06-2.69z"
            />
          </g>
          <path
            d="M154.34 117.8l-.81-1a74.08 74.08 0 00-24.16-19.09 66.69 66.69 0 00-56-1.36 74 74 0 00-25.06 17.9l-.85.91a2 2 0 00-.06 2.59l.81 1a74 74 0 0024.16 19.08 66.69 66.69 0 0056 1.36 73.93 73.93 0 0025-17.89l.86-.92a2 2 0 00.5-1.28 2 2 0 00-.39-1.3z"
            fill="#beeda2"
          />
          <path
            d="M99.25 136.78a9.38 9.38 0 01-5.32-1.43 4.57 4.57 0 01-1.94-3 7.26 7.26 0 011.52-5.62 8.2 8.2 0 0114.3 3.36c.27 1.57.19 5.34-6.28 6.45a13.6 13.6 0 01-2.28.24z"
            fill="#116043"
          />
          <g id="prefix__hand">
            <path
              className="prefix__cls-6"
              d="M159.87 134.91c-6.35 0-11.53 7.39-11.56 16.57 0 9.18 5.09 16.64 11.44 16.66z"
            />
            <path
              className="prefix__cls-6"
              d="M168.21 161.31c2.36-4.88.11-10.86.11-10.86s-6.08 1.94-8.45 6.83-.12 10.86-.12 10.86 6.09-1.94 8.46-6.83z"
            />
          </g>
          <g id="prefix__hand-2" data-name="hand">
            <path
              className="prefix__cls-6"
              d="M39.1 133.39c6.35 0 11.53 7.39 11.56 16.57 0 9.18-5.09 16.64-11.44 16.66z"
            />
            <path
              className="prefix__cls-6"
              d="M30.77 159.79c-2.37-4.88-.12-10.86-.12-10.86s6.09 1.94 8.45 6.83.12 10.86.12 10.86-6.09-1.94-8.45-6.83z"
            />
          </g>
          <path
            className="prefix__cls-10"
            d="M124.11 110.79v7.75M77.51 110.79v7.75"
          />
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
