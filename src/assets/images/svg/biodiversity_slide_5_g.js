import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 1608.97 1243.74"
      {...props}
    >
      <defs>
        <style>
          {
            '.prefix__cls-1{fill:#f99c43}.prefix__cls-2{fill:#f4cb5d}.prefix__cls-3{fill:#fced6f}.prefix__cls-4{fill:#e07070}.prefix__cls-5{fill:#ec987e}.prefix__cls-6{fill:#efc4b9}.prefix__cls-7{fill:#e5a25e}.prefix__cls-8{fill:#f1e1b0}.prefix__cls-9{fill:#f7cb6b}.prefix__cls-10{fill:#efc1b3}.prefix__cls-11{fill:#f68b61}.prefix__cls-12{fill:#db5b4d}.prefix__cls-13{fill:#f7e1dc}.prefix__cls-14{fill:#c6baff}.prefix__cls-15{fill:#eed7ff}.prefix__cls-16{fill:#fff}'
          }
        </style>
      </defs>
      <g id="prefix__little_flies_orange">
        <rect
          className="prefix__cls-1"
          x={648.9}
          y={504.26}
          width={18.49}
          height={6.58}
          rx={3.29}
          transform="rotate(-132.93 668.47 635.93)"
        />
        <rect
          className="prefix__cls-2"
          x={653.45}
          y={502.77}
          width={18.49}
          height={6.58}
          rx={3.29}
          transform="rotate(-86.48 743.159 658.503)"
        />
        <rect
          className="prefix__cls-3"
          x={655.94}
          y={511.83}
          width={12.77}
          height={4.53}
          rx={2.26}
          transform="rotate(-21.23 1219.751 830.209)"
        />
        <g>
          <rect
            className="prefix__cls-1"
            x={669.24}
            y={445.29}
            width={18.49}
            height={6.58}
            rx={3.29}
            transform="rotate(-132.93 688.806 576.963)"
          />
          <rect
            className="prefix__cls-2"
            x={673.78}
            y={443.8}
            width={18.49}
            height={6.58}
            rx={3.29}
            transform="rotate(-86.48 763.498 599.532)"
          />
          <rect
            className="prefix__cls-3"
            x={676.27}
            y={452.85}
            width={12.77}
            height={4.53}
            rx={2.26}
            transform="rotate(-21.23 1240.08 771.248)"
          />
        </g>
      </g>
      <g id="prefix__little_flies_red">
        <rect
          className="prefix__cls-4"
          x={455.1}
          y={189.58}
          width={18.49}
          height={6.58}
          rx={3.29}
          transform="rotate(-101.19 517.78 336.05)"
        />
        <rect
          className="prefix__cls-5"
          x={459.75}
          y={190.7}
          width={18.49}
          height={6.58}
          rx={3.29}
          transform="rotate(-54.74 646.433 379.718)"
        />
        <rect
          className="prefix__cls-6"
          x={458.07}
          y={198.36}
          width={12.77}
          height={4.53}
          rx={2.26}
          transform="rotate(10.52 -787.222 -104.107)"
        />
        <g>
          <rect
            className="prefix__cls-4"
            x={461.19}
            y={263.51}
            width={18.49}
            height={6.58}
            rx={3.29}
            transform="rotate(-101.19 523.866 409.973)"
          />
          <rect
            className="prefix__cls-5"
            x={465.84}
            y={264.62}
            width={18.49}
            height={6.58}
            rx={3.29}
            transform="rotate(-54.74 652.52 453.643)"
          />
          <rect
            className="prefix__cls-6"
            x={464.16}
            y={272.29}
            width={12.77}
            height={4.53}
            rx={2.26}
            transform="rotate(10.52 -781.133 -30.234)"
          />
        </g>
        <g>
          <rect
            className="prefix__cls-4"
            x={435.53}
            y={231.33}
            width={18.49}
            height={6.58}
            rx={3.29}
            transform="rotate(-101.19 498.21 377.796)"
          />
          <rect
            className="prefix__cls-5"
            x={440.18}
            y={232.45}
            width={18.49}
            height={6.58}
            rx={3.29}
            transform="rotate(-54.74 626.866 421.466)"
          />
          <rect
            className="prefix__cls-6"
            x={438.51}
            y={240.11}
            width={12.77}
            height={4.53}
            rx={2.26}
            transform="rotate(10.52 -806.767 -62.38)"
          />
        </g>
      </g>
      <g id="prefix__big_flies_orange">
        <path
          className="prefix__cls-7"
          d="M186 648l5.92 34.41 24.52-9.48-6-34.78a11.1 11.1 0 00-5.2-7.95 10.66 10.66 0 00-9.51-.46c-6.73 2.71-11 10.96-9.73 18.26z"
          transform="translate(-76.66 223.37)"
        />
        <path
          className="prefix__cls-8"
          d="M169.53 654.35l21.56 28.37 24.52-9.49-21.79-28.67a18.61 18.61 0 00-9.44-6.31 17.29 17.29 0 00-11.27.23c-6.7 2.7-8.11 9.86-3.58 15.87z"
          transform="translate(-76.66 223.37)"
        />
        <path
          className="prefix__cls-9"
          d="M187.38 691.41c1.73 4.45 7 6.57 11.75 4.73l19.08-7.37a9.55 9.55 0 005.18-4.73 8.34 8.34 0 00.32-6.68c-1.72-4.46-7-6.58-11.75-4.74L192.89 680a9.53 9.53 0 00-5.18 4.72 8.36 8.36 0 00-.33 6.69z"
          transform="translate(-76.66 223.37)"
        />
        <g>
          <path
            className="prefix__cls-7"
            d="M302.56 623.73l3.07 17.86 12.73-4.92-3.11-18a5.76 5.76 0 00-2.7-4.13 5.52 5.52 0 00-4.93-.23 8.94 8.94 0 00-5.06 9.42z"
            transform="translate(-76.66 223.37)"
          />
          <path
            className="prefix__cls-8"
            d="M294 627l11.18 14.72 12.73-4.92-11.28-14.8a9.69 9.69 0 00-4.9-3.27 8.93 8.93 0 00-5.85.12c-3.47 1.35-4.22 5.06-1.88 8.15z"
            transform="translate(-76.66 223.37)"
          />
          <path
            className="prefix__cls-9"
            d="M303.29 646.26a4.65 4.65 0 006.1 2.45l9.89-3.82a4.93 4.93 0 002.69-2.45 4.32 4.32 0 00.17-3.47 4.67 4.67 0 00-6.1-2.46l-9.89 3.83a4.93 4.93 0 00-2.69 2.45 4.32 4.32 0 00-.17 3.47z"
            transform="translate(-76.66 223.37)"
          />
        </g>
        <g>
          <path
            className="prefix__cls-7"
            d="M408.63 615.13l2.44 14.15 10.08-3.9-2.46-14.3a4.53 4.53 0 00-2.14-3.27 4.39 4.39 0 00-3.91-.19 7.1 7.1 0 00-4.01 7.51z"
            transform="translate(-76.66 223.37)"
          />
          <path
            className="prefix__cls-8"
            d="M401.87 617.75l8.86 11.66 10.09-3.9-9-11.79a7.64 7.64 0 00-3.89-2.59 7 7 0 00-4.63.09c-2.71 1.11-3.3 4.06-1.43 6.53z"
            transform="translate(-76.66 223.37)"
          />
          <path
            className="prefix__cls-9"
            d="M409.21 633a3.68 3.68 0 004.83 1.95l7.85-3A4 4 0 00424 630a3.46 3.46 0 00.13-2.75 3.69 3.69 0 00-4.83-2l-7.84 3a4 4 0 00-2.14 2 3.43 3.43 0 00-.11 2.75z"
            transform="translate(-76.66 223.37)"
          />
        </g>
      </g>
      <g id="prefix__dragonflies">
        <rect
          className="prefix__cls-10"
          x={123.43}
          y={106.88}
          width={21.35}
          height={69.14}
          rx={10.68}
          transform="rotate(13.53 -845.723 -69.924)"
        />
        <rect
          className="prefix__cls-11"
          x={39.65}
          y={391.26}
          width={23.39}
          height={13.22}
          rx={6.61}
        />
        <rect
          className="prefix__cls-12"
          x={61}
          y={391.26}
          width={13.22}
          height={13.22}
          rx={6.61}
        />
        <rect
          className="prefix__cls-11"
          y={391.26}
          width={40.67}
          height={13.22}
          rx={6.61}
        />
        <rect
          className="prefix__cls-13"
          x={99.02}
          y={109.93}
          width={21.35}
          height={69.14}
          rx={10.68}
          transform="rotate(-33.34 444.342 384.18)"
        />
        <g>
          <rect
            className="prefix__cls-10"
            x={229.86}
            y={204.83}
            width={10.53}
            height={34.1}
            rx={5.26}
            transform="rotate(13.53 -744.669 10.5)"
          />
          <rect
            className="prefix__cls-11"
            x={149.7}
            y={458.28}
            width={11.53}
            height={6.52}
            rx={3.26}
          />
          <rect
            className="prefix__cls-12"
            x={160.23}
            y={458.28}
            width={6.52}
            height={6.52}
            rx={3.26}
          />
          <rect
            className="prefix__cls-11"
            x={130.14}
            y={458.28}
            width={20.06}
            height={6.52}
            rx={3.26}
          />
          <rect
            className="prefix__cls-13"
            x={217.83}
            y={206.33}
            width={10.53}
            height={34.1}
            rx={5.26}
            transform="matrix(.84 -.55 .55 .84 -162.71 382.74)"
          />
        </g>
      </g>
      <g id="prefix__butterflies">
        <g id="prefix__butterfly">
          <rect
            className="prefix__cls-14"
            x={930.59}
            y={499.59}
            width={6.61}
            height={14.23}
            rx={3.3}
            transform="translate(-96 260.78)"
          />
          <g id="prefix__left-wing">
            <path
              className="prefix__cls-15"
              d="M896.37 471.39h7A28.55 28.55 0 01932 499.94v9.57h-6a29.57 29.57 0 01-29.57-29.57v-8.56l-.06.01z"
              transform="rotate(177.73 873.633 601.368)"
            />
            <path
              className="prefix__cls-15"
              d="M911 508h4.32a17 17 0 0117 17v5.85H928a17 17 0 01-17-17V508z"
              transform="rotate(-92.27 990.704 667.928)"
            />
          </g>
          <g id="prefix__right-wing">
            <path
              className="prefix__cls-15"
              d="M963.28 470.11h6v9.57a28.55 28.55 0 01-28.55 28.55h-7v-8.56a29.57 29.57 0 0129.55-29.56z"
              transform="rotate(-2.27 6550.855 2535.576)"
            />
            <path
              className="prefix__cls-15"
              d="M952.77 507h4.32v5.85a17 17 0 01-17 17h-4.32V524a17 17 0 0117-17z"
              transform="rotate(-92.3 1015.003 666.363)"
            />
          </g>
        </g>
        <g id="prefix__butterfly-2" data-name="butterfly">
          <rect
            className="prefix__cls-14"
            x={1090.52}
            y={421.67}
            width={5.26}
            height={11.33}
            rx={2.63}
            transform="translate(-85.86 247.71)"
          />
          <g id="prefix__left_wing">
            <path
              className="prefix__cls-15"
              d="M1063.51 398.94h5.6a22.73 22.73 0 0122.73 22.73v7.62h-4.79a23.54 23.54 0 01-23.54-23.54v-6.81z"
              transform="rotate(178.73 1038.115 525.374)"
            />
            <path
              className="prefix__cls-15"
              d="M1074.74 428.58h3.44a13.56 13.56 0 0113.56 13.56v4.65h-3.44a13.56 13.56 0 01-13.56-13.56v-4.65z"
              transform="rotate(-91.27 1154.151 586.856)"
            />
          </g>
          <g id="prefix__right-wing-2" data-name="right-wing">
            <path
              className="prefix__cls-15"
              d="M1116.09 398.29h4.79v7.62a22.73 22.73 0 01-22.73 22.73h-5.6v-6.81a23.54 23.54 0 0123.54-23.54z"
              transform="rotate(-1.27 11145.685 3983.417)"
            />
            <path
              className="prefix__cls-15"
              d="M1107.25 428.1h3.44v4.65a13.56 13.56 0 01-13.56 13.56h-3.44v-4.65a13.56 13.56 0 0113.56-13.56z"
              transform="rotate(-91.27 1173.104 586.378)"
            />
          </g>
        </g>
      </g>
      <path
        className="prefix__cls-16"
        d="M979.53 113.12a13.92 13.92 0 0112.69-13.88 13.94 13.94 0 0113.47-10.41h1.84A13.93 13.93 0 011021 99.18h4a14 14 0 0113.93 13.94v1.45a13.53 13.53 0 01-.69 4.32 2.18 2.18 0 01-2.07 1.49h-53.88a2.18 2.18 0 01-2.07-1.49 13.53 13.53 0 01-.69-4.32v-.73-.72z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud"
      />
      <path
        className="prefix__cls-16"
        d="M1074.89 86.16a9.89 9.89 0 019-9.84 9.88 9.88 0 019.55-7.37h1.3a9.86 9.86 0 019.53 7.34h2.83a9.88 9.88 0 019.87 9.87v1.03a9.75 9.75 0 01-.48 3.06 1.55 1.55 0 01-1.47 1.06h-38.16a1.55 1.55 0 01-1.47-1.06 9.76 9.76 0 01-.49-3.06v-.51c0-.17-.01-.35-.01-.52z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud-2"
        data-name="cloud"
      />
      <path
        className="prefix__cls-16"
        d="M1000.47 470.14a13.94 13.94 0 0112.68-13.88 14 14 0 0113.48-10.4h1.83a13.94 13.94 0 0113.46 10.35h4a14 14 0 0113.93 13.93v1.45a13.63 13.63 0 01-.69 4.33 2.17 2.17 0 01-2.07 1.48h-53.86a2.18 2.18 0 01-2.07-1.48 13.91 13.91 0 01-.68-4.33v-.72c0-.24-.01-.49-.01-.73z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud-3"
        data-name="cloud"
      />
      <path
        className="prefix__cls-16"
        d="M1071.41 505.64a8.28 8.28 0 017.55-8.27 8.3 8.3 0 018-6.19h1.09a8.3 8.3 0 018 6.16h2.38a8.31 8.31 0 018.3 8.3v.86a7.93 7.93 0 01-.42 2.58 1.29 1.29 0 01-1.23.89h-32.07a1.29 1.29 0 01-1.23-.89 7.92 7.92 0 01-.41-2.58v-.43c0-.15.04-.28.04-.43z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud-4"
        data-name="cloud"
      />
      <path
        className="prefix__cls-16"
        d="M669 60.72a20.73 20.73 0 0118.88-20.65A20.74 20.74 0 01708 24.59h2.73a20.74 20.74 0 0120 15.4h5.94a20.75 20.75 0 0120.73 20.73v2.17a20.32 20.32 0 01-1 6.43 3.24 3.24 0 01-3.09 2.22h-80.18a3.25 3.25 0 01-3.08-2.22 20.32 20.32 0 01-1-6.43v-1.08c0-.36-.05-.72-.05-1.09z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud-5"
        data-name="cloud"
      />
      <path
        className="prefix__cls-16"
        d="M312 271.22a20.75 20.75 0 0118.89-20.66 20.74 20.74 0 0120.05-15.48h2.73a20.73 20.73 0 0120 15.4h5.95a20.76 20.76 0 0120.73 20.74v2.16a20.11 20.11 0 01-1 6.44 3.24 3.24 0 01-3.08 2.21h-80.16a3.24 3.24 0 01-3.08-2.21 20.39 20.39 0 01-1-6.44v-1.08q0-.54-.03-1.08z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud-6"
        data-name="cloud"
      />
      <path
        className="prefix__cls-16"
        d="M596.92 93.35a13.92 13.92 0 0112.69-13.88 13.94 13.94 0 0113.48-10.41h1.83a14 14 0 0113.46 10.35h4a14 14 0 0113.92 13.94v1.45a13.53 13.53 0 01-.69 4.32 2.18 2.18 0 01-2.07 1.49h-53.86a2.18 2.18 0 01-2.07-1.49 13.81 13.81 0 01-.69-4.32v-.73-.72z"
        transform="translate(-76.66 223.37)"
        id="prefix__cloud-7"
        data-name="cloud"
      />
      <path
        d="M1000.42 22.2c20.78-35.48 68.71-115.14 164.5-173.31 86.65-52.62 236.41-102.19 348.73-48.82 185.57 88.18 196.35 422.83 144.87 630.81-81.38 328.83-392.39 639.84-577.1 582-225.18-70.48-258.42-687.82-81-990.68z"
        transform="translate(-76.66 223.37)"
        fill="none"
        stroke="#eed7ff"
        strokeMiterlimit={10}
      />
    </svg>
  );
}

export default SvgComponent;
