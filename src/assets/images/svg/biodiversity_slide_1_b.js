import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 1440.93 515.68"
      {...props}
    >
      <defs>
        <clipPath id="prefix__clip-path" transform="translate(.44 -122.9)">
          <circle
            className="prefix__cls-1"
            cx={1027.53}
            cy={491.81}
            r={10.91}
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2" transform="translate(.44 -122.9)">
          <circle
            className="prefix__cls-1"
            cx={1075.75}
            cy={488.34}
            r={10.91}
          />
        </clipPath>
        <clipPath id="prefix__clip-path-3" transform="translate(.44 -122.9)">
          <rect
            className="prefix__cls-1"
            x={315.32}
            y={460.99}
            width={54.35}
            height={24.8}
            rx={10.08}
            transform="rotate(-14 353.103 478.812)"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-4" transform="translate(.44 -122.9)">
          <path
            className="prefix__cls-1"
            d="M351.92 465.61a6.54 6.54 0 114.9 7.92 6.59 6.59 0 01-4.9-7.92"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-5" transform="translate(.44 -122.9)">
          <circle className="prefix__cls-1" cx={605.94} cy={169.7} r={4.54} />
        </clipPath>
        <clipPath id="prefix__clip-path-6" transform="translate(.44 -122.9)">
          <ellipse
            className="prefix__cls-1"
            cx={356.82}
            cy={169.7}
            rx={4.67}
            ry={4.41}
            transform="rotate(-44.19 356.852 169.7)"
          />
        </clipPath>
        <style>
          {
            ".prefix__cls-1{fill:none}.prefix__cls-4{fill:#e9fcff}.prefix__cls-5{fill:#eb826a}.prefix__cls-7{fill:#fff}.prefix__cls-12{fill:#414042}.prefix__cls-15{fill:#f99c43}.prefix__cls-17{fill:#edbe69}.prefix__cls-19{fill:#e1f4ec}.prefix__cls-22{fill:#fced6f}"
          }
        </style>
      </defs>
      <g
        style={{
          isolation: "isolate",
        }}
      >
        <g id="prefix__Layer_1" data-name="Layer 1">
          <g id="prefix__FLOAT">
            <image
              width={902}
              height={375}
              transform="translate(537.34)"
              xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA4YAAAF3CAYAAAD5IlWYAAAACXBIWXMAAAsSAAALEgHS3X78AAAgAElEQVR4Xu3d7XLiSLNF4QT7Pfd/v9M25wedTbLJzCoJgcFaT4SiSkJgPjoMa4ruOZxOJwMAAAAA7NdxdAIAAAAA4HcjDAEAAABg5whDAAAAANi5z9EJAIDf53A4HEbn/LQTfwkeAICnOfC+CwC/xzsE35aIRwAAtkEYAsCb2lsEziIWAQBYjjAEgDfxpBB8xs9QD30jIhQBABgjDAHghd0Rg2uv9y5WvXkRiQAA5AhDAHgxC2Nwybkztr69GVu+EU3fFpEIAMAFYQgAL2BBDG593ruaffOaOo9IBADsHWEIAD9oMghH53SXj677jro3rrWXnU/gTREAsFOEIQD8kIkorC7Pju8lDpeGX3V+++ZHIAIA9oYwBIAnWxmEemy0PzpeWXr+Fpa8ES0JPT02c87lAt4gAQA7QhgCwBMNonAUe3F/dG51bOayV7FlBMb9metfLuCNEgCwA4QhADzBHUE4Mx/tL/nZP23JV0Vn46+LwtH++SBvlgCAX44wBIAHa6JwNuYOMt47j94lDJdG4Ojy2fkVAhEA8FsRhgDwIA8Iwi4Ql8Rjd2zJ5fdYsjKYHesCsBqXHMv2zwd54wQA/EKEIQA8wGQUjubdqOfPRKMeHx17tlEQLonAtWM3vxzkzRMA8MsQhgCwsYVROArCe47FsZt3x57l3ii851g3OgIRAPCrEYYAsKEiCqsVu27s5jPndaPOs/3Zy5bq3nS6GOsCrpvPnFcd6+aXg7yRAgB+AcIQADYyEYXZfEkAjo5Xt2NhjPPR/X2WtUHoYxWBs8f0cjcKxctB3kwBAG/uc3QCAGBsQRTqsSUxONq627Fk1Hm2/wxVGGqQjYIu27pzOic7PxdxNJn/czgcDsQhAOCdsWIIAHdaEYWjiBttx+J4dRuWzOPossfRHV+jetOpVuXWxODM9t3cliXzOOr8cpA3VQDAm2LFEADusFEUzmwag1UcVpsl8zg63R8dX6KKpiq4shDUfY292WPHcHudk50fexzNLs/H1W2wcggAeFeEIQCs9MAorCLwmBzLzu82S+YuzlV32awlYagRGOdVFB7D3I8f5JyZ2+ru5yHMzS63/w9xCAB4R4QhAKywMgp1Hrcs9PSYHh8FY7VZM+p8JDt3NoqyIIzzURR+J3MPRN8/hOMHGeN1OzEI4z5xCAD4NQhDAFjojijM9rsg1BjM9qvrHQabydz3s3knO282iKoY9LELwyoKde6ReLBL0Gkc+u13gRhjUEeT+fkAcQgAeCOEIQAscGcUVtsoBKtjs6GYbZbMXfYYu+MzqkDKgtDHGIbd3x/8DpvuewTGQDzYJQKrn7s0Ek3m5wN//7wQiACAV0cYAsCkB0RhtUJ475bddrZZMnfZY91SDKVRFMYIjOFWhWC1Zc9BFohxpbEyHYdmxuohAODlEYYAMOHBUTjaPgb7M4HYbZaMOt+SBpIGYZxrDOrYbV/JfvUcaCB6HOp9iLo49Mv/IQ4BAK+MMASAgY2jMFvR60LwYzCOQjH+rGqzZhzJzpuJn3jOKAy/k/koBL/s/Ljj8UPYj4890vvVrRpGHoc+N7uOxfMBvloKAHhRhCEANB4YhTqvYnBm/qg4jGZDsZKF0KkZszAcReFHmMco9CA8hH0fza6fhxiC8f5UgRiDMO6XcWhmrB4CAF4OYQgAhQdHYbZl8dcdy0JRI/Eg826zMOq8kp0zEzyzUTgKQl0t9Cj8CPOjXa8SdpEY+X36/nsbVSDGGNTR7HLbV88LcQgAeCWEIQAknhyFXfgt2fS2qjDUSLRk7mbicKklUTgThl92HYa+Vc93jMQvO8teR1099EDUOIwRqHHox/S88wG+WgoAeBGEIQCIDaNQY2xNEH4O9tfGYbVZGHW+hRhAM2G4Ngq/zOyPXT/f3ePWQFQxCmMcHkxiz26jMJ6Tnc/qIQDgxxGGABA8IAo1zGKwVVH4OTnq+fH29OccZJ5t1oxbqcIwi0ONwpPl/9JotnkUxjiMr0d8zG4UiH4f41dLu5jT1UPiEADwsghDAPjryVGoARdj79OuIzDb1yjUTcOwi6NXCcMYXt2K4ZflYfgnzI9h9DjsHnOMQj8WLztY/tXSEeIQAPAWCEMAMPupKNStCsJs+0jGLA7958X7Eu9fFkgWRp3fo4pCH7Ot+wppDMM/dn6ccbUw/j3DP3b7GnXb6Kuler+7SOzi0C//hzgEAPwEwhDA7r1AFM7G4P9kfxSH2aqhhtHRzuLjcPq477U0DONXSLMg9DGLwg+7/hpp3GIk6qay5+BglxD0+3YMx2L8KY9Dn5sl5xOHAIBnIwwB4NaaKIyhNRuFGoQfdht/GoRVHK5ZNazCKIuhbH+WBk4VhT7Xr5KOVgtnonD0upmMvunK4cFuv1JqdonD+FiiURDGOQAAT0cYAti1YrXw38XJWG26Crc0CkchqKNuS1cN9T5nj9VVz1F1vAscXTVcsmLYrRb62EXhkkDUx5Y9L2a3j2fmK6W6nz5frBoCAJ6JMASwW4OvkGZjtWWrhGuisIrA/yXH4nx21TCGUhVJlowue75mdauG1Yqhfo20Wy2McTh6vFkQu+y5OFi+cniwfPWwC8QYg9X4D3EIAHgWwhAALqoQisc1pLaIQo09DcIsDLOVw27VMIulanPVfC1dXfMxi8QsCmMQflu/Whgft75O2eOtngc9FgNRnxN/HPFrpVkgdiuF1XEAAB6KMASwS8lq4SiIsljYMgqrGMzCcMmqoUZSF0jx8cXR6f4SoxXDOK9WDJeuFupr1D3u6nmI++5gt/9ri4Pdrh56IHZxGOf+PMQ5q4YAgKcgDAHszsK/V+jzastW49ZEYbVS+H/FZTOrhhqpGrJdDLnsueqeP5UFzSkZsy3+HcPs7xfGv2NYrRbOBnG1mYwHu43CTIzCNXF4hTgEADwaYQgAeQD4WG0aWVUQLonCauvi0PfXrhge7awKoaiKoBmPWDGMq4VdBMYYXBqG+rzE5+Bgt3/3MJqNQ7Pz9U/JCADAUxCGAHZlsFpodhsBumWREaOwCsSlUfh/g8uzVcMYoRqHWTj5fbewb8noRs9dJ0aOzrOVw9HfMYxbfL6zx7okDE3mLjsnrhzG86KZODTLY/Bqn1VDAMAjEYYA9i6LoC4cPC6qrQvCmSj8v2K+Ngw1WGPMzkSRhk51LJNFTBaIGofdiuGH5WHo4+hxdmFok/txjKrnJUZh99zp6iEAAE9DGALYjQWrhdnxKjKqIMyiMMZhF4UzcaiRGX9eFoU+6uM42tlM/Iyev4pGztoVww/LVw2Pdvk6qb5WeqwLw9nNknHEo9AD0elzc5BjV/usGgIAHoUwBLBn2Yf7URToqpRuXSB2q38ahaM41NVCjcJqxfAQRp9b2LdkjGZDyHWrhtmYhaGP/ncMq+f+j12/RtljHb2+ulmy78ei7HnJHntcNTzZ7fVYNQQA/AjCEADOqgjKttkQHG2jKMziMF4vu80uDDWaqgiKo87voSuFOvr8O+zHFcNjGP0rpXGlML4u1etWvabd8xH34zGVPb64H+/HzNdKDzICAPAwhCGAXWj+v4XZB/4sDDQ6dLUqC8TZ1cLZOOxWDD+sDsPsvvtolj/mqAuYJbJY8lHn1d8z9DD0r5DG18JXDONrFse1myXjLA1gs0sU+m1l0XcojvN1UgDAQxCGAJCHULdlUahxOLNSWK0KzqwYjsJQ71e83/FxWNi3ZIyWRlHURaGOMQg1CuNXSb+sflyj1zDbKqPLI1011MelP9OPKX2+/FwAAB6CMASwR1n8aCBk4aBB2EVhFoej1cJRHMbVQr+9+HOq1cIYhnHMHnMc3WwUzdBw0tG3o12i8Gi3caivh75O1ePMXuOoO78TI9D3P4rLNRD9WObQXAYAwGYIQwC/3uBfI63CIM41OrI4rL5CGiNOxyoOu01XCrso1K+SdrFkyehmwmhWt3Ko8aR/r1DDMH4ls3ts3ea6y0ayKOw2j17/OT736zq/7AZfJwUAbI0wBLA32Qf+LJB0O8q8ikMNtpkorCJxtFoYVyZnVgtnVtBc9TytUQXMKRl1Vc1DSiPwUBzTx+RmXuPqeh29z93mgRujMP7MGIi+b3Z5Hg7hGAAAmyIMAexV9YE/C4e42hbn2QpdtWo4E4XdqqHeTlwp1DCMUagrhRqH1ow634KGTRWHMYRiAHok+t8vtHCZPqZss2Kuj3P0uGcj8EP2/XHdvWoIAMCWCEMAv9qKr5H6qFu2SqirhRqFWRx2m56TXVdXI+PPizG4drVwaSAtNRuGPvco9HCK/5sHv/9fYZ49nu4xzz7eeL9HQfhtlyA82eX513Oz+xQD0fdTfJ0UALAlwhDA3lWx0G0ahRqHWRRmWxWFWURm19fVQg3DGIRVGFozZrrLMhouGj0xiHyM84Nd/oVSC/s+9+3LrlWvq14+S++fR6AG4Xc4Hv8F1WM4Vv25OoWxMrocAIBVCEMAezIKHz2efXhfsmI4CsLqWBWCMQiXfIV0FIXV87IknCrZbcTI830NI42kGIl+3XjfXXY821fZbTld6cui8NNu4/DDruPwO4z+2vjj8q2Kw+wYAACbIQwB7FUVQVU4+Qf5bssCMfs6aReJXRTOBmG2Uhjj0JJ5HF0VSveqIicej/O42uaPwwMxXjeOenyGh16cV1HoWwzCD7lMVwxjHMb4jfcxxmFEFAIAHoowBLBnXQTqpqtuWRRqHMagy0JvSRBqHHZBmIWhbtaM0ZKwmpWFT+RxFPdjUPk8Xp7No6WPQwNRg09XBv018mNfVr8evmKY/dmrVgxT/D1DAMBWCEMAv9bgH55Rem4WU9mH/CoKNRB1rOYakdkKYRaHet+6KMxCcCauotE5XazE68ZIjFGkt69RGONw9md1shCMYxeHX3Z+LbJxFIXZazIKvZlzAABYhDAEsBczgZB9SK82jbEq3GbiMLu8isAqBmeDMD62OOq8Ozajup4GTYwcn8fRZO5jjEOPLQ9F9RHm+vNPYTO7DkENxC+7jUF/zWaicOY10eciyo4BALAJwhDA3szGT/chvgqz2VCs4q86Hm+zCsIsPCzZf0VZEFWRFB/HKAqziNKVQd0+7RKC8auhGoBxrq939zXS6vWI+6PnAACAzRGGAPZIP4T7mH1o1+O6dVE4M9cAzIKw2g4yj/e9iw893okxsuR60Shouss1jiKPpyoQswj8SI75Fr8e6lGYxbuH34eMo616jXQ/e85HzyEAAHchDAHsVRZO1eVdFOq+BmKca0hkUTgKiyowfOweV1yFOsgxn7vsNraMk+q2TsWY0cegz8Px72U++rl+7MMuQfhht393cCbeR69d9Wcn2yJ/nQAAeArCEABuxQ/pow/zWbhV+xqF2fElUagx1IkBqPvZitwzo0R/1tKfrc+Bvj6nZPwI+x6FMQ7j3yGsor56rfXnV392VLxs+jngXyYFAGyBMATwK93xL5JqXGTnVlsWBbNbFRZZTOjYidH3qvGQ3a9q5XD2MXSvka8Sdq/Hh93+XcFRvOvrOHot4+s381oCAPAwhCEAnOkHdT0+u42iobteNteIqALCVwA1nLJjley6jzC6P/HybH4KW0Yfc/V8f8t+fP2y11Iv19e8e331dXTZMQAAno4wBIBb3Yf47JylWxYR1swjDaWDjNn52Tk6z4wCbmv686oo9DGbx3NU9npWr0v2GunloyCMPyfKzslkr+no9QYAYBXCEACWqT7Qjz7km11fV+OgCwmXrZhpMOn5WUhoVMzc92e5Nw6rWIzXi7LXIXuNqmPV1t1W5ZVeBwDAzhCGAPZuyw/jo1CI53VmA+hk1/+T96NdIjAGSRx13h17pmr1KwtDn1fPj/5P6vXyqPq5VdzFMfPTzyMAAKsQhgDQWxt3nSx2spDJIvAg+/HvyrksZnSe7b+SLuA08pY8h3qeJfNneuXXAACwI4QhAPQ0IuLxTBckfnk8r7qORp9v3+G2dFXrJMey0b1DkHTPuYacPpffMv9OjlevUzV3ug8AwNsjDAHsXYype1QBUUVfNo8R41u1Ynmy26+OZudWYVgdewVVeMXj2bx7vr9l/i3HZ14b37Kf5XR/ZMm5AAA8DGEIAMtUcaD7VaDEeRcr33b+/+hl8Xay6xXCLArfeaVQaTxpiOlYPe/xedXnOXs9stds9BrrfdP71RldDgDAwxCGAHAr+5Dvx3WeBYmuRHUR+GXX/zP1KvDs7/U//l4vfs30GM6tVgyr/XeQBdPsa+HPeXzuv8L4lRzT16h6/fTn6Oa6+7rUvdcHACBFGALAmX6Y1+Pdh/9shSkLiy+Zxyj0wMvi7vT3so+/cz1Xw9Ds+vpVDL5qJI6iRy/XWMpep+r1GG2jWMzicWazMFZGlwMAsBnCEMCvdDqdTofDYTZ8PLZ87mP2wTwLDp134eDBcfw7/rHrwItxF39O/PuE1f9Y3WTu+2r2eXkVVSDF4xpb2Ws0CsM/MlahmMWg/jmoQtGKefUYAQB4CsIQAG5pcFRb9sF/FB8ahjHyXHab8Suk2epiFYTvtlqYqaJJXycds9cqvi76+vyx2zisIlFvo4rBKg5N5vFY9XgBAHgYwhDAXvmH7yqQ4gd0/WCvH/yrLQuKP5bHXaS3HaOwC0NLRvdOITii4aSxlb1uGoYadjEE45ZFYxaH1db92YmbhVHnrdPpNH0uAAAVwhDAHp3s+uuaB8s/qPvlui0JQQ3Co12+Pup/NzD7OXq7HzZeLcxWDKv9d5aFkMZV9brF5zbGXRaF/8mYBeIoCrtAjIg7AMCPIgwB4DoU47Fuy6LQ/yGZ0UphFnJ625/J7Y7C0JJRvXMgVvEUj3dhWL1u31avGP6XHNPz9LXWODzJfGZz2TEAADZHGALYi5NdVgarONIP4V1UxH2Nt2yVUIMu0p/hUeirhNlqocahJaN75xjsaCydklGfW30d/fXz10uj7z+7XTnUINQVxGwl8STzKgQjYhAA8DSEIQCcaTBmUXGUfY0Lj8AYhbMrhX47MQo9COM2u2LofmsUui4Osy2Leo1DD70YgtU8xmS2YqhxeJJ5dn8tjFF3GQAAdyEMAfxaE//LivgB21cTuw/sWRBqFMYY/GN1wOntx6+Oftr1SuGHXYJwJgr3FIfxNdR5fI41zPT1+7bbVUANwbh6WEWhbl0g6p81R/gBAJ6OMASwVye7XbmLcWh2/cHdg+JoeRRmcThaJYyR4kHoX0mtvkY6E4Y6V+8YiqNYygIxvpZdGPrzH7c/yfaf1XFYRaHGYRaFWSRW0XiFf5EUALAVwhDAnsT4y+IoHq8+tH//PaeLwyrc9HbjbXgQxtXC+FXSQzLGINzjaqHZbTidklGfc33uY8DFFcAqDqvVQ/06aRWFXSBaGB3xBwB4OMIQAK6NgtDDLAuLURD6qGFSrRbO/v1CS0ad/3ZZEProc42z7DXUVcMYiNnfM4zH4nWyKNR5dh+rCBxdDgDAXQhDAL/a4O8ZZh+yD1ZHoe6PgjD7uVmYxCCs/tGZaiWy+nl7ikKz69eyisP43GdxGIMuBl62cpitFGZfK9U4zDaX3V8AAJ6CMASwVyerwy2LwxiEvn3JWAVbvG2PBP3HZuLfKaz+NdKDzC2Zu72FoVkehD7OxGH8CqiuHHZfL9WvkcYo1DjsItHC6Mo45O8XAgC2RBgC2JssCOMHbI9CP65RmMWhX09lMfIZ5h6BcbUw/oMzo6+QahBmgThrzXUeaU30xOt0UVjFoYahz3VFsAvFtVEY41CteS4AAFiEMATw6018nTRe5h/CPRBn4rAKRI2TGAcfybY0CrcKw6XnP8vSIKrC0EcNwnvisAvFeN3ZKLTiWLT0+QAAYBphCGCPPAZjFMYP3X6ZH9eo8HNGsutWYTiKQo1DS+Zu5r65Jef+hKUxlAWhj9mmYahxGCMx+7poFoq6xSjsIlGVj52vkQIAtkYYAsB1IPp+nMcgdKPVuS4KY4CM/rGZR/yjMzPnvKqZIFoSh1mwZYG4ZstuM/vZJsf1z18cAeBXa77hcxf+g9oYYQhgF5Kvk3oMxijUNw2//NvOgaYf6jtVhGgU+ji7UlhFYfZG+pA31xeUvdlnUehjF4Zx/pWMVSzqsXi9Lg41BB0fYAD8Ko8KvlmzP3/PAUkYAsBZDMR4LM5HMehOdo49DZAPu8TB0a7jcIsonHrTs/nzXtHMG7a+bj7eE4czoZiNWQz6vslxjcSTjOedHX9oAfAeZiPsFWX3fS+/dwlDAHvmMRijsPrlHz+kjwLRP+DHODzadRx6CMYonAnC0Wrh0jfjpec/25o34y6ushCLsaZhqFsXilkQ6pZFYBWEAPAWnhSCsz9j89+h+vh+aygShgB2Y/Cvk5rVgejxaHaJuioO9YP/MYx+vdHfJxyFoSWjzmetuc4zrXnzrSKrCrIYhDEKu0CcicAsCKs4tGK+5vEDwMPdEYNrrzdr7e1P/76Nj/03RSJhCGDvPAY1CuMbi/7Sr+JQP/DHrQrBR0bh2jfHd1O9KVehpfMs2rIw1P1RCOq8+/MR71PpN30AAfB+VsTg0vM7s7e19vdkdfvt7f2mSCQMAezKglXD0S93jUOPP59rcIxWB0dBuCYMM7PnvbLRa2N2fY7OTzLPNg26Kg67/Xh9ncfNnYoRAH7UgiCcOW/mnHusvf3qd268vfb3sj9P7xqIhCEAnH/RH4qxE+PQ3zhOdvn6qMfAUeYagjNRaDL3/WzemT3vlY1eFzeKLp3rVsXdKBqz61Q/w8Ko8yvv+kEDwPuaDMLunLWXPZP/bs3uj/7enYrEdw3Ew5vdXwDYRPJml0VWFmXdVsXe7Oqgrzhml1ky6jyqjv9W3ZtZFl4aZqOtC74qAmeCMM6z8bzDmzWAJ5oIwiXvPd1tjX7Oo3W/W7PLqvPb39Hv8jucMASwWxvEYTzWxd7onG6zZtR5VB3/rWbf3LMA03m1ZaGnx7oYzH6WNeM/7/KhAsD7G0RhdpkemzlndLwyc/7S35fV+Xp8tF8du1z44r/L+SopAFyc7PKm43P/JR7neq7Z5eukuvl5xzDvApAoXG/pm3sVafduHofV7Vkz3nj1DxIAfoc7g1AvH+1Xx7rjs6rrV79L9f19xsmuf45f34+lt3c4HA6v/DudFUMAu1a8EWZvdhpvcV6F3exW3VY3uuz+z1h7vZ+y9s1Krxf3szjrgi6Lu9Gm51kz6pwoBPAUTRSO3nPi/sx8Zn/2shmj36Hl79/B/sw8279c8IK/3wlDALu3Mg597MKu2rrzrRl1nu3PWnu9n7L2zSq7XvYmrhEX56PImz2vG3V+PsCbNIAHWrBKuHY+urzaHx2f1f0OrX7/LpmPLq/2zwdf7Hc8YQhg9yb/S+ko2rrIOxTHuuvHUefZ/uxlv9nsBwDdH0VbFX2zl1kY47z7EPFyHxgA/C6T731xv3tPzI7NnKPHR8fWqH6XjqKuGmcv03m2fz74Ir/vCUMAsOk3yJk3uS4AuzfN2Z8T3fumee/1n+XeN6rs+qM38Wzsgq86lo3d/HyAN2cAD7TwPS/Otx67ebT0/ar6HVqFWvU+MPp9vub3fHrfXuH3PmEIAH/d8UYZ51nsLR27eXdsqS1u4xm2eKPKbuMnPiBUP/P6IG/OAB5g8n0u7s+M9xxzW7/njX7nd7+fR7/3Z4+56d//P/27nzAEgGDFm2acd8dm3hC7n9Edm7lsT0ZvbFt9YBidOzu/8tMfDAD8TpPvb9V70yj0dD5zXjbqvDvWmf09343dfOa8bOzml4M/9D5AGAKAWPCX8eP+zPHRG2B1G2rpGyQuqje9mTft7kPF7DzbPx/kDRnAgyyMwmzs5muPdaPOo+49MPs9OvrdPBN+2X52vJv7fhx1fjn4A+8JhCEAFBYEYhd0M/OZ/dnL0Ove9Lo36mq/O290G5cLeDMG8CB3RGE3r7al5+jc9+OouvfA7HdpF2Zd9GXHuk3Pt2Seje7mvj/7vYEwBIDGIA7Nbt+gsje32f3qWHcc61VvgNnx7g189OZevtE++00fwL5M/O+Y4n4Wdrq/9WbJ3FX3c6T7HV4F2yjyfPsujo+ub8ncVfPzgSe+TxCGADDhzkDM9qtj3fHMknP3aOmbXHW+Hh/tV8cuF/IGDOCBJqJQ5xpq2b5vx8lj3fHu57lq3ql+P1eBNrtpFM5GYvaznN43nV8OPuE9gzAEgEkTcWiWv3FV11t6HI/VvSFml1Xnt2+sz3hzB7BvC6IwG2e3o4zVsWxfN0vmcdR5R3/HapjF+Wj7XjjPrpv9PGtGnV8OPvj9gzAEgIXuCMTZy0fXxeN0b4qjN8zR5Q9/UweAB0ZhF4Bx7IJR53GzZO7WvC92AdZtWfSNxm6ebZbM46jzy8EHvo8QhgCw0mQgms2/oc2eh8ebfXOcOu+Rb+QA4B4QhaMY7OZVPOo8C8Pqfs7SwNIY67Ys/mbm2ajz6r5YMrqb949HvacQhgCwgQWRaLb8Da6z5W39Vlu+0U3f1qPeuAEgszIKsyDrgjCLv+pYF47ZzzM5ZskY+bHsd208VsVY3LrAy2KwOp7Nq58ZN5O572fz84EHvMd8jk4AAIzFX9ATkVj9Mh9dL7P5GwPMbOXz+og3agBYaU0ULg3CpZvenv4sk+OWjCOnZsy2mRXC0eb39/vvz4rz+HMqJ7t+fL7v9z3OzwcOh8PW7zmEIQBsbGEkRmt+wS+5/b1a87xO2/qNGQCWGrzXLI3CbMy2j8ljXSDGUTdrxkq2ylYFYRaGXRR+Fcey+66B+G3nx9kFYoxBHU3m5wMbxyFhCAAPpL+wF4bijM3eEDBnyzdhALjX4Cuk2ajzYzIfhd2HjKNjozjU+bPCcMnK4NFuAzGGXxaJMQL953SB+KNxSBgCwBNlv7wfEIvYyFZvtgDwCBN/rzDu3xuFGn4fC47NxGEVhrNR6E7JeE8Ueggew+jHPP48CqsVxBiJMRCzOLahFFoAACAASURBVHRPj0PCEAB+2Mwvc+JxWzPPOQC8ofhe0c1no1BX/zQAZ49ltzcKwzVxOBuFXRh+yfxDRr/vX2E8JGN1X/2+VauHHoJxPozDLRCGAPAGCBkAQJT8B8MsBLPA0k0jbUkQzm7HZJwJwywKR8GlURjn38m8i8IvmR9l0xVCX0X04yaX+znOf/4xOX6QucbhlS1WDQlDAAAA4I0s+MdmqjjJYkyjcBSDn4P9Kg5jGM7EoTWji0GkMZhto9XCLAp9+2O3gZiF4pedda9VjMKjXa8ejuLwJhLvjUPCEAAAAHhvXTiNojCGTRZwWfh9DkYNRg1MDaouDi0Z41xDaBSGVRRmYVhFoQaiPsdxGwWirhjG86o4dDdxeA/CEAAAAHgTg6+Qxv0uVmaiMAs8Hat5FYlVGM4EloWxckrGLgpPdvv1UQ3DP2GuUehjfE7j/XWjQNSVQ/1aqdl1BFbzu1YNCUMAAADgDUx+hdTnXRCuiUKNQN30si4O9efG+/PMMMyCMIbhHzvfR4/DYxhjHHb3N0Zhp4tDs/Hq4T9r45AwBAAAAN6TBlOMkniOxmG2VUHYxeBoq+IwRqGuGGZxaMmoqig0uwThktXCGIU6+v2torB7PToxCrPr6SqhjnchDAEAAIAXN/G/LYqXZ6GiwaWrdhqIVRT+b7C/JA7958X7o/fXwjGnz0WMomq1sFoxrFYLP8KYRaHe31EUjl4/51GYrRq6YQyuWTUkDAEAAID3c5DR592Wrc5Vq4Uahf8rxm4+CkON0ixgNbY62WphDMLsa6Qzq4WjKOzus9n4fmeqVUNXrR6uRhgCAAAAL2zhamE81sVhtVLYrRL+z/JAzGJxNg6rMDzaWRaG+nh1xTBbNfyWsfsaabZaqPd1JmRn6f2Pr1VcNdTwa2Nw6aohYQgAAAC8lyyQqhCMMXiU/WqlMMahBuBoX68Xb2u0Ypitwlk45rLwOiWjbt3fL/T9brUwW+HsorAKxCrWTnYJYrPrVcOT3d7epquGhCEAAADwvjQWYpyMIlEDMVvl0wjUrYrEbNXQbzP+zCUrcFV0ZVHoY7Zi2H2NdMlq4b1RmMWrH+8ee7yNNgaXrBoShgAAAMCLav6/hRoha4KwWjXMvg46CsSZMIyB2K0YanhZMqrZFcMYh9k/POP/awpdLcyiMLufszQEdT/qVg79XA/ENhQ7hCEAAADwnrJozOIwBpdGocahrhbGKMzi8P/sNgqzMIxRmMWh3s+lYWjWR2GMw9GKYfz/Fmbbmhjs7ttHmMdzDrLFKIzxtzoGI8IQAAAAeA9ZHGUBlW1L47CKwhh/MQpHq4ZLVgzj/bVwzGVBpqttVRRqHGoYehBmYVg9t6NA7EI1xqGf648/i8N4jvPH7IF4CMemv05KGAIAAAAvaPCvkeplVRxmsaUh2H2FtIrCauUwu04Mwk/5+TG6HrFiaHYJwhiFJ7v9O4X+VdKj3X6FdGkMmo1jMNuOyTz+zBiFMfauYnANwhAAAAB4fVmIZPGUbbrypdsoErOviVaBuHbFsApDk7nvq1MyjsLw+HfuUej3JcZh9Zx298Hn3ab3yb9OGsOwikMLx5xG4eJQJAwBAACA95EFSTzehUwWiVUUVnFYBWIVkN2Kod4XjUKNwzhmqjjUGPM49DDUr41W96MKQpf93CxKY5xqFGoYahQewrEs/BYHoSMMAQAAgBez4mukPi6NwioSP5NNg69bSdTrZiuGa8JQ5y7GkAdVnGdhGAPxy26fq5kg1IjTnxd/5qfd/uwPu9yHGIbf1r+WWRzq/uWCib9nSBgCAAAA72cUTqMozGJQ97OVxCwWs+NdGGZRWsWYRlkVaGbXMehj3I52vVIYt+pn6s83u73dmfj8lLGLw3gfq+fDIzDetzb8RghDAAAA4LV1MRQvz4ImC65RIFZBmAViFYxZGGqEzoahhVHnLkaRR1qc+xaDKvsZ1c/V6PS5RmEVhjEKP5LL9PnIolD5Y6mOV5enCEMAAADgPVRx4GMXOlmELYlDDUWdVwGYbccwahTGOLRmVBpsPsZI9Niqoqu7bb1ND70411VA3b5knr0G+pr5fTW5bFX8dQhDAAAA4H3EeKlCRgNCtywI10bizHZM5lmoxtGsjzbf9yiK+xpNVTSOnOx8X3VVUPfjSmAMxBiCM3Gor81o1TA+zih9bKO/Z0gYAgAAAC9k8A/PmN1GQhYPXRRqIGqQfDTzUehp9HUBWEWhPgYLoxs9R5EHlMdWHCNdFfyQMYZhXBmMK4TVaqHGYfccVZvJ/kzcTtMnAwAAAMB76+KiC8Juq4KvWv3T/SoANXycxlB2vNrPxnhu97yMnhN9vFkoH5tj2Ta6X9VzZMWxVY6jEwAAAAC8rCyo4nxmy0JlFDPZuTGuqqjp7m8l+ypodfmseH9Gz032GLfaRj8/PnfWzFX1fJf4KikAAADwumY+4GfxpdcbhcfspkGzJATj3GV/R86Pu0PYj/NIj2V/l3AmIJc8XxrDVfiNIrC6zGTU+czjmUYYAgAAAL+bRobPR9vMipYlY0X/Dl8Wb74fbyvux0CMl0dZDJ6STc+tzDxv1XM1G9Gzz2v2HB9s7nG0CEMAAADgPVWRMLo8C454mZ6jx/x4ZybIfJ7965sxBrPwiT8/iyINv+z+6L8wWt2/7Pb1+Ynz6jmcOSfu6zh6zu9CGAIAAAC/Sxcc1bnZ5V2IxFjqIjBG2MGu/1VQsz6UsnFGFnRZ9MU4/JZ59Tji7c0YReHotXkawhAAAABAJgurUWD53OMvi73T38tPf48dw+UaSluGoY/ZpmE4E4nZ8/G2CEMAAABgvzRu4nG9XEOqCqpDGDXoTn+P+ehzk/OzOFwiPh4Nt5nH823n/99g3M8eb/X86M+K9Jyl1jwfQ4QhAAAA8LtVAdIFS7V9F5v/T9u/7BwuX3K7PsbVRP/HWL7/Xv7sMPS5hp4+rq8wrx7/KBKrGFwTh2uuM0QYAgAAAL+Lho+Oenm1ZaFUxaCPZtchd7Lz/+T9ZLf/e4fsa6SWjCo7XsVSPF49LzHqqscZIzHGYheJa2PRmuMPQxgCAAAA7+lk+Vc1s3m1P9q6INT/P19228cwHpKxC8JHhKGO+li7x/xlZn/sNhBjJFbBOHqedXN6Xx+GMAQAAAB+r1GAjELoy84rfj7P/gfuukIYb/fj76jn63W7KLz3q6RxPxvj/dUxxt4oDmcCcSYWLZm7LA43CUbCEAAAAHhdJzuHkY/VOVlUZPsxeL6LYx6DMYbiV0arIPTRby/+XcJqpbCLQ7dFGMZj1fMS73v2nMTtTzFm4ZiFYvw52eb0Pj8MYQgAAAC8r5PlgRb3s+jJgjBGYRaEfywPOUtuL95OtsI4isIuBrPLRuGURaGPozjUFcMsDuOm51VxqFt1X7r7XNHrDBGGAAAAwO/SBUYMny4KqyDUKKtCqorCNX+3cM2KodltFMV9Da7s+YnPUxeH3VZFYhaFoziM99np/mqEIQAAAPBCTqfT6XA4dDHkMeDnZOHQhUW3ahVjToOwi8LPv+OHzUdhFoc6z/ZndRGlz1Ocf8s8PjcxELMVw/+sj8IsEE8yxuPd67opwhAAAAB4Hye7DsLqa5WzQagBp9uaKDyG0YNwJgq7OLxXFYlVdGmwxZDTFcOZlcMsFOPtZTFYxaGL+8NYPJ1O7eWEIQAAAPC+PA6rGIyR4f8YjEZhjMPRap5ZH4UehFutFlbHZmQhpDGlYxWHGnEzcfif1WGoQahbFoTdMdfGX4cwBAAAAF5bjL9qhVDj0GTfA+dgdRxm0aZiKM1EYRWGlszV2iDMaDBVUejzGGdZuOnq32wcdiuHMQqrOIyqCFwVh4QhAAAA8H7ih/8sGrMg9EDLIqdazYv8tjwIYxQeZfSfVa0UahDGn7dlEKr4vFVxmAW1h5p+BXQmDv/I5Tqv4nC0jcyc8w9hCAAAALyYwT9Ak0Wgx2EWNYdkvwvCmSiMq4UahbpKuGS18JFR6DQIfZ4Foq7g+fOm4ygO9ZhGYRaHXSBacWw1whAAAAB4HxqFerwKCQ9C375krG4zXn9pFL7aaqHTINSxev58XBKHMQh1hTG7DQ3CLA4tjFEZh6N/eMaMMAQAAADeQRaE8cO+R6Efz6Iwi0O/rspuYyYKs1XCOLdk7p4Rha6Kw+w51FDTONQwrAKxisRqxVBfgy4K3cw5KcIQAAAAeD8aih4CHohZ2PnlunkgRlUYehTGENR/dEZjMNssjDqflT3+Jbo4rDaNt2zVMM41ArNY1Cg8ybyKQz12F8IQAAAAeEHJ3zP0GPTRjzm/zI/HIPTLfdWwM4qhL6v/BdJRGJrMfX9k5hw1E0xL4zALuGzlMM6rlcQqDKsg7EKwfKwzXyM1IwwBAACAdxUD0ffj/NvOgZZFYmYmhjwKv6yOwioMTea+H60JwKW6sBrFoa7oZdGskTgKxXg9vW2NwS4S9TEsQhgCAAAA7ydGoYbAaOXQz1FZfHzbOQQ9VI62LgpjGMZR55WZc9RsIGVR6ONMHJ4sX/XTIBwdy2Iwi0M1+zhbhCEAAADwopqvk1b7fizOPeg0EDUoPD4+wrkehccw6ldGR18f3SIMo+r8NYGkz5WO3Zat8FVxqFGYXa7b6Od39/28M/k1UjPCEAAAAHhXJ6tXDeM5PsbAq87xzYMvzmMY/lQUmm0bhmZ9YHVR9p2Mo60LQQ3COLcwz+7v3QhDAAAA4IUtXDWMoXAI+zEKszg0qwNIQ1BjcMsoXBOKS1UxlUWhj6M4jGE4G4nZ+XqbWQya7Ov9XYUwBAAAAN7Xya6jUIMxilGoAZYFTxeCoxhcG4aZ2fMys7HUhdYpmVdhqGMXi9m52e1mm4UxteRrpGaEIQAAAPDyFqwajmLAozCuGp7s8rVR3/y2PBA1DEcxGEOwisHZ4Js9LzN6PqJ4rsaXBlncNOK66BuF4CgIs/sVx9UIQwAAAOA9newScDqOxFVDv06Mw4NdrzDGlcYsDk3247E46jy6JwCX6p6jUXyNwm0UfN2xbnN6f24sXS00MzusuA4AAACAHyCrhmZ5cFWhpsezlb/j4PLsuDX7rppH1fFHqCIoC7A411CrIi4LvaUhWP0sC6POV0WhGSuGAAAAwDs72SWofB7H6lyz6xVA3/ycY5jrptezZu772Tx6ZhRGWURlz5uOOl+zZXGY3aaFsZvfhTAEAAAA3kTydw3NznEwG4fKz6sCsdssmXejWxuBS643etwVvV4WYRpwcT7aZs7NbjuO7uYxrl0tNDO+SgoAAAC8myQOzfKVuWzMwi7bH11mE6POs/1ZS663NnKy62VhNhOH2bHseLdvYezmd0WhGSuGAAAAwG90snNIZWN3fpxnUZjNs1Hn2f7sZY/UPR/VvgZbNi6Jvi4GszjM9u/GiiEAAADwhopVQ7M8zvRYFnT3HHNdGFbHlpi5/r2Bk10/C8M4r+Iwm3fHutHd3L97VwvNCEMAAADgbS2Mwzjfeuzm3bGlZm5ji8AZxWHcr0JxFHpdAHbHrmwRhWaEIQAAAPDWJuMw7nehmB2bOUePV/tqdPkzjIJoFIld0N0Tf0+LQjPCEAAAAHh7TRyaza3qjeajy6v96ti7qGJpJuC2isCHR6EZYQgAAAD8CgviUPdnLxtFYPXzf2MY6mWjmMvCUPdHt3G54AERRxgCAAAAv8gdgTjaXxp+7xyEmZnVw+rYkgB8ahA6whAAAAD4ZQZxaDb3lc/R/uxlasm5z7Y0jmZjcbRfHbtc+OBwIwwBAACAX2plIFbHtwrD32Q2DKtj3fHzhU8KNsIQAAAA+OUmAtFsffjN3PZv1cXUKLRGlz8tCs0IQwAAAGA3JgPRbC72Zs7Zi9momjrvmUHoCEMAAABgZxYEotm2AbjlbT3KloE0fVs/EYMRYQgAAADs2MJIjNZe7zdaFVU/HYMRYQgAAADAzO6KxFmPvv0tPDSQXikGI8IQAAAAQOoJofjrvWoIKsIQAAAAwDRisfYuEZghDAEAAABs7rcF5DtH3wzCEAAAAAB27jg6AQAAAADwuxGGAAAAALBzhCEAAAAA7BxhCAAAAAA7RxgCAAAAwM4RhgAAAACwc4QhAAAAAOwcYQgAAAAAO0cYAgAAAMDOEYYAAAAAsHOEIQAAAADsHGEIAAAAADtHGAIAAADAzhGGAAAAALBzhCEAAAAA7BxhCAAAAAA7RxgCAAAAwM4RhgAAAACwc4QhAAAAAOwcYQgAAAAAO0cYAgAAAMDOEYYAAAAAsHOEIQAAAADsHGEIAAAAADtHGAIAAADAzhGGAAAAALBzhCEAAAAA7BxhCAAAAAA7RxgCAAAAwM4RhgAAAACwc4QhAAAAAOwcYQgAAAAAO0cYAgAAAMDOEYYAAAAAsHOEIQAAAADsHGEIAAAAADtHGAIAAADAzhGGAAAAALBzhCEAAAAA7BxhCAAAAAA7RxgCAAAAwM4RhgAAAACwc4QhAAAAAOwcYQgAAAAAO/c5OgHXDofDYXTOKzidTqfROQAAAABgZnagHy7eJfq2REACAAAA2G0Y7jECZxGLAAAAwL7sJgyfEIKPvv3OQ19EQhEAAAD43X51GN4Rg2uv905WvfBEIgAAAPD7/LowXBiDS84d2fK2ltryRZy+LSIRAAAA+B1+RRguiMGtz3tnsy/81HlEIgAAAPC+3joMJ4OwO2ftZe+se8FHfxhGlxOIAAAAwBt6yzCcCMLq8uz4nuJwaRRW57d/aIhDAAAA4L28XRgOonAm/GbOGR3PLDl3S0tfwNnYG+1Xxy4XvtsfLgAAAGCn3iYM7wzCURyOrj9z/NXcuzoY90eRWP4s4hAAAAB4fW8Rhk0UjgKvisN7o3Hmsp8yG4SjuDvJmJ03uo3LBe/wBw0AAADYqZcOwwWrhGvno8ur/dHxnzTzVdEl89HlOs/2zwdf+Q8bAAAAsGMvG4YrVglnw68aq2PZfnVs5rItjF607PJR1I3G0bFufjn4qn/gAAAAgB17yTCcjMIl0bd27ObdsZ8wisG4X8XeKA6XRmO2fz74in/wAAAAgJ16uTAsonC0StiN9xxzrx6HoyjsAq4Kw9Hl3djNLwdf7Q8fAAAAsFMvFYYTUZjNq7jr5jPnZaPOs/3Zy9boXqxupW4UclkUjvar6+txPXaFOAQAAAB+3suE4YIo1GOj6Mv2u+M2Meo823+W7AXswmwm+rL9USBmsRhHnV8OvsofQgAAAGCnXiIMV0ThkvgbXdadq3Pfj6MefzZ9AasY9HFJ9I02PdeSeRzdzR864hAAAAD4OZ+jEx5tZRRWsafHjsk5SzZL5q6aR9Xxtap4qsJrSRCu3SonOz9+H/2YheP/HA6HA3EIAAAA/IwfDcM7ojDbzzYNwywUu3jUn2Fh7OZRdXypUYTpfE0Ufi84/m3LxFA0mZ8PEIcAAADAj/ixr5LeGYXZsS4Cj82x7Lrdz7Jk1HlUHV9qTRjOBqHG39J5t1kz6vx84Kf+UAIAAAA79SMrhhtEYbVpAC4ddR43S+aumneq85ZEURZYcdS5blnofdv5OfD9Qzh+CMfj7Xarh/E2dDSZnw+wcggAAAA81dPD8AFRWK0M6ry7rLqduFmxH0edd6rzZoOoWnWbCcIuCn3zODzYJeZiHPrt+7mjQDQjDgEAAICX9NQw3DgKuyCswnB0ud5uvH2TY9aMqjo+qwqkLA5HYfjdjN/FvgfhwS7xF+dLAnFRHJoZXy0FAAAAHuxpYfigKJyJvyXbQeb680yOWzLq/BGyIPT50iisQtC3L8tfg5lAjFvUxaFf/g+rhwAAAMBjPSUMN4zCo8yzoPuQy/RYdnkViPozsyjU++6yx7yVLAy7IMzCsIrCL5kfksuzQDS7vg/dqmHkcehzs+tYPB8gDgEAAICHeXgYPiAKNQSz7WNy7EJRo1AD0ZK5m4nC6pyZ+FkThqOVQd+OdgnAo+WhOFpFjKuGVSDGIIz7bRyaGV8tBQAAADb20DB8YhRmofcxOZ+NQw3D7L7HMaoicAmNIY3DLgxno9Bj0MNO4zBGoY8V/3l+e9U5h2I0mf/D6iEAAACwrYeF4ZOiUINQ40+37LIuDLNAXBuHqjpnJniWROEoDL/C6M+FzzUO/THHOLRwPG5LVg/N+jj0y/9h9RAAAADYzkPC8MlROIrBuH0mx7LbWhKG2WOJ49Y0Cn3UQPwOx6owjFEYN3382VdHYxRusXro58U49GMWjl9h9RAAAAC43+Zh+KAojPNslXA2BD+L+VHmS+PQklHnWzk145LVwioK4/bH8uehCsQ4Rn4f4+qhxmwmhqLJ/B/iEAAAALjPpmH4wCjUTWOuC7/RGDcNz5k4tGTufjoMR18h9e3bzhGYRWGcV69XFYi+xVVCvz8jMQpjEPrtX4UgcQgAAACst1kY/nAUdvH3mexXEam330Xh0c7ifbcw6nwLMXw0CON8FIXZSuGHXSLQv04ag3AUhi57HqJ4X0eBqHHoxywc/4c4BAAAANbZJAyfHIUx3KoQ1CjMtmrVMP4M3bpAsmTU+b1mw/A7GWe+QvrHzo89foX0TxhnorD7aunBLjEYv1bqx25iT2Srh8QhAAAAcKe7w/AHolC3mRj8X3G8i8NjGPW+VXFUxdAWqij0MduqlcIsDLMojHFYvW7V65oFoq4Q+n30QMyCTlcNiUMAAABgY3eHYSKLoioefLs3CmeCcDYOP8PPzO6L3mcLxywZne4voZEzCsNsxdCDcGkUZo89bpbsR93z4aF4sttojIhDAAAA4IHuCsNktTDuZ6GUbWuisFsh/N9grLZ4u8dkjPdxFEguC6XRZV3MxMs0Cs0uIRgDcWa1MEbhbBBXm8l+9Y/SxBCMj2Xp3zss4xAAAADAnNVhWHyF9N/FyZhtRxl9viYKsxDM5lkk6u3Gn61xpKNZHkZR91zNuGfFMIvDbLXQtyoQ9XXKNkv2XfXcmF2H7dHyQPQQjPM2Dlk1BAAAAOasDsOEfvDPQiFuWWiticKZINRj3aph3GYDyZK5y0JojWq10McsDj0Qu6+Regx2ITwKQt1MxoPVK4cHu1099EBcE4dXiEMAAABgbFUYDr5CGvdHAaHxEWNsaRRWMViFYbdqWIWh3mcNIY0ip/tLbbViGL9K6v9bCl0pzGJ4aRhmz4tvGogqRuHRblcPR3HoIwAAAIBJi8Nw8iukPq+2o8zj9iHz2Sic2bow1DjUUI33dRRCcYy65y6TBY6uHGocfidj9vcLP+wSiFkMHpIxe/2y58BVx/2yOEb+mDwIj3Ybh2bn62YxeLXPqiEAAADQWxyGiZkg8uMaG6Mg1BXDpVH4f8kxjcL/ye3vfcWwC0QNvSoMZ45nXy31MUagz7PnUKMwzgEAAABMWhSGG3+FNItCDUENwtko/L9i3q0axp+pcaihdJC5ydz3s/kaW4Whrhh6HGaPUQN+9JqajNll2TEPRKd/r9CPHYvLqtVDH88XsmoIAAAAlBaFYUMjqNo0rDQOu1XCmSisgtDn8bozYagraNljsLBvYdR5dyxTRUwWhT7GKKxWDD/sEodHy8Owerz3bhZGs9so7J6bLg7NkhgEAAAAMGc6DJvVwuwDv+9nm0ZHDC9dLazicGkQZlu8PV2hzKKwiyWTue9HXfR0stBZsmLoY4xCXy2swlBfry72ZjdL5iMee34/vu32erpiGOfxGKuGAAAAQGE6DAf0g7/uZ7ERt9nVwtH2f8WYRWGMw27FcEkYZmM0E0NKQ2a0YqirhT6PURhXC//Y7WtySMaZzZrjfpnKjuljM7tEoQeiX34Ic/u7T/wBAAAAC6wNwyx+RqGQhceSMKwisYrBauUwi8IYhzNh6Pff7PaxxlHn9+gCUef6dwxPdolB/0qmvhYeiFtuJqM+L6PnJgs8D8QYhRGrhgAAAMBCU2G44H9REY9Vm4ZW/NpmDMI4X7tSOLtiWK0Wxjg8JGMWPfp8jOJn1szKoa4Yxjj0KMy+OqqrhvG10sc7u1kyd/G4i48vxq7vVz/DL48OyTEAAAAAhakwFFnodFHgmwZWFoWj1cIqCEdRqP/4TBw/klGjsAomjZ4sgrL9paoo9HkWhxqGMRBjHMbH9Ef2sxhbulkyV/p4PmQ/Pj697WzlsF01BAAAAHBtTRi6KnayIPAtRlYXhVkcxqBbE4dZGOoKZfc10kMYq/iJo863oAGlY9yOlodhjMP4umSPa8utkoVtnGePKXveNfwOybHzBXydFAAAALgyDMOJr5FmAaDHdGUqxmG2zUZhFofdFm8rBqGGoa4YZo/B7PoxWhhd99ytoTHTBZWvGPr9jWEYo3DpZhOXzxiF7UdyzOPQdX/fkPADAAAAJg3DUFQBVJ3jUeJjt1VfIc2icCYSu9VC3boojHEYH5OFfUtGtSSaVBY5VVj53IPJo9Dvt0ehH5vZTMb4WHS/OlbJ4m8Uh9V9zJ4nfx58BAAAACCWhmHUxYB+aM8icMmqYYy7KgqrENQojLcTf9YoCjUOq2iKZuNoloZNFYcxhGIA+vGZVUMr5tW+nl/pQjDbjsVxvb9Z/On+5QK+TgoAAAD804bh4GukZn0sVJsGYRaH8SueWSBmW3Z5tuoYo1BXC0dR6KPZ7eOOoxs9f0tVYehz33xfv2oZI9FkrF5LV12m53VGEej32Ud/DeJj0fvp4uN0hB8AAAAwYcmK4ejDfxYZ2Vcws9XDmSjs4jBbHazicBSFMQ4PyVjFkeuep9FzqDRsNHoOMtcwPNj13zX0fefHvmTfZIxbdpmrHt8oBLPtwy5x6ONJ5vF+xedAjS4HAAAAdm1JGLrsw38VEQe7XX2LxzTMYhxWUdjFYnaO72fxuWS1MNssGV0VSUtkt+Fh45fF4PFRIygGY/YY/PiXXVSPa3RZNBOEn3YdhF0c+uuhUehbFn+6DwAAAECsCUOzPpCyy33T4OpWDmcisVsV9E1vo/o52WpmvN/Ze+G5EgAADvVJREFU49XHnM23VoVOPB7nHlIxElV3f5c+ri4GNQpn4jC+HtnXSXXzOHREIQAAADBQhuHE3y/MZOGkUZht8Wuc2cpeFYezWxWDVRDGeM3iw2Tu+9Ga52+Gho+KUej7/vVRj8R4WfUYsp9xkFFpgI6CMFsd/LL+z0oWh5E+/hL/AA0AAABwVobhQhoUo62KwyoKs0jsji2JwSwOD2HMNktGnXdG53WxEq8bI9HnWTh6EMZANLusyFlynezxmV2HXnXcf1YVhF+Wvy5fYdTXY/Ra+DYKvZlzAAAAgF2ZDcMqEvSyKpbilgVhF4hVLHahqPMYhBqHB5kfZF6FSBx13h2bUV1PgyZGjs/jaDLX+3u06xU4j0R3KkY9p9uyIPy0Pg71z4i+NrpqqOLz0h0DAAAAYPNhWOkCSUPKP9Qfkn2NRI24LA5Hx7MIjPMqOvQ+a3x0j/knxODxeRxNLncxsDQIza4j6qO4rArBTxmzIIzz7DXJ/qzE+xzpaxWfA5M5AAAAALEmDLNYisf1HN30g38VbVngdcGYXUdvW++H3jeTeaTHqlU5Pcdll88YBc3oclfd/yoQNf58/hH249dFqyjUCMxeoywOsz8r1VaJrw8AAACAwpowHIkf1LsP89kHf40DjT2NiVEILokMC2OmWonL4iO7nS3jpLqtJT9DH0N8Dvz58sudR6GO+o/HfNh1COrrpa9ptXURWB33y5Y8FwAAAMCupWG48F8k1Q/rPup8tI0ioQu/LCh0noVEdt8zuiKYBaFe/iz6s+L+zP3Q5yJ7XeLqou/ra/Mtc4/DmddLX7v4s7PXLHut4nkzj9vMjH+ZFAAAALDtVgy7D+vx8mo7ynw29qrwy0JwFBcaf37MbGFsPFl2v06DcaR6jTwIPQKXbF0cHmSMx7vN6esGAAAAYIGtwjDKIiyOes7sdpyYj37uiIbTITlWya77CKP7U60Yxjg8WX072WOOz6U/5/HvJfqooVcdqy6f2aLsGAAAAICFHhGGHQ23an+0zZxvYVQaRzOxdQjn6Twzus2t6c+rotDHbB7PUVWUZVsWeqOYz14//zlRdk4mvkYAAAAAGveG4cyH8+xDvH7wrz7o62WzUeBGsdSFoceg2XVkaHDM3pdnGD1eHXVexWLlkIzZazVz+ey5lZnXwV87fQ0BAACAXbs3DO+RfdDPAiFeNqMKoLj535Hzc+K+Bkkcdd4de6YqcrIw9HkViPq/oNAtqn5u9nxlwQcAAADgBWwdho/8wK/Blx0fxeDBLhH4/fdcDZUsDHWe7b+SLuD0eeyes9FmyTyOj/bKrwEAAADwNrYOwyVBEINCj3URMjrv267jL65Aegya3a5gneRYNrp3CJLsudV59bx+y/w7HNPLuteoGvW+rbXV7QAAAAC7tnUYZrIP712kxOOjTWPlW7YYhXrb+tXR7NwqDKtjr6CKpew5j/PuOc7isAvF7LK4ZT/PZfdfzwEAAACwoXvD0MOqu1zHUYhUWxV/vvn/TD3uZ/fN77OPWRS+80qh0qDKIqx7TfR5/7Lr5z3bsutltzm7OX0sUXeZ08cLAAAAwO4Pw4p+oPdjenkVIqMIzILwaJcYjGEXY+Dj73X0/8Pn51YrhtX+O8giKIutUzLPIu8rjNV89Jrp69xtVsyzxxWNLgcAAADw1yPCUKNj5gN/FiBVBMat+5+ku4+/t+mjx+AhzE2u18Xh6PhPGwWRXj6Kr+w1iRH4x65fE718NhC7SHTZfR89XgAAAAADaRieTqfT4XBYEj7Zh3i9TM/TEOgCIkZH3NcojOJt+wphFZExCEdRuOR5+WldNGXBpa9VfI2q10TjUENRt1EgVnGYba57nAAAAAAGtlgxPNl1LHUf3rOti8EsQDzs/li+Qmh2e/sfdh2EXRhmo3qXOOyCKQurOMbnT0d9bf4kox7LAlHHLBi7KMzm3WO+cTqdFp0PAAAA/EZrw9A/TFeBVH1g160KwiwKNQ6zsLPk9uNq4SEZs9VCfVzvEoIzNIROzejPoY7Za+MB+J9dwnBJHGZROIpEVz0mAAAAABPWhOHJLqHk8+4D+0wMdoGYBaFGncnt+/hl59XCL1u3Yuh+cxjGYxqF2et2sjrcPQarOMwCsfszoD8/u196n6PsGAAAAACxNAxPdhtJ1bEuLJaEYBV0Sn/Oh91GYfX3C7MgzH7GOwdiFUhZFPqYhVl8veLrpgH4n10C8b/k8iWhOArB+Ni6YwAAAAASs2GYxV92TvbB3WPiGOYah3FlL0ahrhRqyMWfE4PwM9xmFoV7/RpppLFUBVd8zbowjHEYVwyruV4n3t5sHFqYj0KQOAQAAAAKZRgu/JdJNRz1Q7xu2Qd/X9n7I2O1uqfx8mmXMPQo1DDUKMyCcLRq+Jt0gZW9Ztlrl630aghmq4f6NdNqxVB/ZvVnysLYmTkHAAAA2JXZFcNM/IB9sHUxGIMwbjEOu1VCvd0P2UarhaMwzPZ/Cw2kURhmcVitGur2n13HYRWFcZuJQd0sjEP8i6QAAADA2dowPNltqGkcmuxrxMUorOKwirLs9r7s/Hg8CGfD0JJR5+rdYnEUQPHytWH4bdexV8WhHq+icHb1MKruOwAAAIDGkjCM8ZeFURaLMSg8yrI47FbyVAyUGArdV0hjGGabJaN7twiclQWThlWcfyejxmFcNYyBqF8jnYlE/XMSY7DaLIwRcQgAAAA0loRhJn7g1hVDjcI4j0HYxZrS2/ZVwhiE3ddIswC1ZNT5bxZfQ42r6vUchWEWh1Ugjr5WOhOEXRRGo8sBAACAXWrDcPAP0JzsOp58v/rg7lF4sEsUdoGo9LbiKqF/hbT6GmkWhIThRRWHMbqyVdq4xUCMgZd9rXTm66Tx9mZXDS3ZT/H3CwEAAICLpSuGMf6qePMxi8IYhzEKNQ5VFoUehtVXSD/sEoS6YmiW/7wsEPegCkMf9fnv4nBm5XAmELso1Dg0mTviDwAAAJiwNAwz+uHbw3EUhzEK/XoZvQ3/31LEMOz+wZnRPzqz1Wrhmus8ypogyoLQx9k4jEE3isNsf00UZnEYH78+HgAAAABiGIYLv07qx3zMgsLsEmQehX5MZdePcbg2CrMgvCcMl57/DEtDqIqpGFoxyu6Jw26McTgThSZzVz5+vkYKAAAAXBuGYcJjMEahftCOq4Yeg/Gyb7uNsxiJZrdBqBEyE4UahNXXSH9bFLolAVSFoY+6VWHYxaFGYjePcVhFYRaE0ZLHDwAAAOzWmjBUMRDjMR01FON1PAq7AIkR8mX9/5ZiyWrh0iicOecVzURSFYcxwDTQsjCMr9OSSNQg1DjMgrA6Hs08dgAAAGC3psIw+TrpyeZXDf0yj4h4udIwyQLEo9DHURBmYWgy9/3oXQNwqS6kuviKr0t8faqVQ43DKhY1CLOti0BrjvM1UgAAACAxFYYTYiDGY3H+bedIyyJRr3Oyc/RlYXi06zjsvj46Wi2Mo84rM+e8otkgyqLQx3viMAvE6lgVh919iJslIwAAAIDCPWF4svGqofOgy2Iw8g/2HoUekt9/j/nteByOVgurzZJR57PWXOdZlkZRPL8LQ9+PsdbF4UwozgRhnFuYx2N+HAAAAMCk6TAc/OukZtehWF3uYwxE/XCvcXiyS1RmITj79dEYglUMrom8Ndd5ljWBNBuHun0n48w287XRGIRZCDo9drXP10gBAACA3HQYFk52vWroo1+m55pdAs/jMAurGISjEFwbhTNB+MrRt5VRYK2JwxiGSyJRzz0lY7U54g8AAABYaFEYLvh/Gmbz+IH92/JA8w/5hzCPoVFFYReEVRj6fjbvzJ73imajKQutOGqQZeGWhV0Xi9m5elvVZmF0V/usFgIAAAC1RWFoZt2/UKr7/kFcozDy2PMP/kcZYxjEr5NqCM5GYTbqfGTJua9mNo7ieTo/ydz3s3jTuOsCMAvBURDG+5KNAAAAACYsDsPCyS4BqGN1rgfdd9i3cL0Yhwe7jsOZGNTbXBqE7xyAS1UhFY9n8aVxplu16pcFYnW9anN6v26wWggAAAD0Dms/MydfKc2CqwqzbMtCb/ZYtlkyj6PT/dHx36j6Q9AFWBaEul9F3pKviWabJXMX50QhAAAAMGH1iuHgK6U+j6PJXMWVQ99Osn+UY5Zchyhcp3pdsujKRp2PtpkgzG7PkrmrHgMAAACAxuowLJxsHIcZP2e0xfMsudySeRx1nu3PWnu9n7A2mPR6GmPZWAXd7KbXrfbVzTFWCwEAAIA5q79K6op/pTQLsWzs5l30jfYtjHE+uq9LrL3eT1j7IldhGOezcaj7M/FXzePorvaJQgAAAGDe3SuGxf/C4mSXcPJ5HDPZdUbhp8FXjTrP9mcv+626kOoirAq2UeRVsTcTg20UAgAAAFjm7jBsVHFoMldVIJqMS2Mwi717A/De6z/DvdGUXT8Lwzgfxd3ocr0su1znl4OsFgIAAACL3P1VUtf8j++rlbos2paMM7ejx0fHltriNh5tixf43jjcYuzml4Nb/YEGAAAAdmSzMDRr49BsbiVvZvVv5hydd8eWXL4H3R+IUSDG/SWxOHtZtX8+uOUfZgAAAGBHNg1Dt2L1sJqPLp+dR8TfOtUflC7auvCbmU8FoRlRCAAAANzjIWFotmj1UPdn5qP9JT8bc5asJC6NRZ3P7J8PPuoPMAAAALAjDwtDd0cg6v7o3OrYzGVYZnb1MDs2G43dsfMFj/7DCwAAAOzEw8PQbBiHZnORN9ofHa8sPX9Plvzh2DIWq2PnC57xhxYAAADYkaeEoVsZiNVxVgdfy5KvmlbHuuPnC5/5BxYAAADYiaeGodlUHJqNw44ofE1L43DmsvMJz/6DCgAAAOzI08PQTQai2XzozZ6Hx5r9AzV1HkEIAAAAPN6PhWG0IBLNtg/ArW/vN9nyD8f0bRGDAAAAwHO9RBhGCyMxWns9bGvVHyhiEAAAAPg5LxeG0R2RuMQzfsa7eugfDmIQAAAAeA0vHYbqSaGIByEEAQAAgNf0VmGYIRZfExEIAAAAvI+3D8NZBOR2iD4AAADgd9lNGAIAAAAAcsfRCQAAAACA340wBAAAAICdIwwBAAAAYOcIQwAAAADYuf8HItil/G12FHQAAAAASUVORK5CYII="
              style={{
                mixBlendMode: "multiply",
              }}
              opacity={0.5}
            />
            <g id="prefix__cloud">
              <rect
                className="prefix__cls-4"
                x={874.87}
                y={50.28}
                width={123.01}
                height={36.28}
                rx={11.73}
              />
              <rect
                className="prefix__cls-4"
                x={893.9}
                y={31.44}
                width={80.85}
                height={35.17}
                rx={11.37}
              />
            </g>
            <g id="prefix__cloud-2" data-name="cloud">
              <rect
                className="prefix__cls-4"
                x={568.79}
                y={293.99}
                width={123.01}
                height={36.28}
                rx={11.73}
              />
              <rect
                className="prefix__cls-4"
                x={587.82}
                y={275.15}
                width={80.85}
                height={35.17}
                rx={11.37}
              />
            </g>
            <g id="prefix__cloud-3" data-name="cloud">
              <rect
                className="prefix__cls-4"
                x={1306.59}
                y={136.96}
                width={88.39}
                height={26.07}
                rx={8.43}
              />
              <rect
                className="prefix__cls-4"
                x={1320.26}
                y={123.42}
                width={58.1}
                height={25.27}
                rx={8.17}
              />
            </g>
          </g>
          <path
            className="prefix__cls-5"
            transform="rotate(-80.45 1000.883 438.607)"
            d="M1061.67 498.84h23.3v2.94h-23.3z"
          />
          <path
            className="prefix__cls-5"
            transform="rotate(-17.79 638.42 440.506)"
            d="M1029.35 491.72h2.94v23.3h-2.94z"
          />
          <path
            className="prefix__cls-5"
            d="M1035.17 529.75s-1.11 0-10.26.68c-10.57.76-14.11-7.45-14.15-7.53l2.71-1.14c.12.26 2.82 6.35 11.23 5.74 9.24-.66 10.37-.69 10.51-.69zM1074.72 524.57l-.43-2.9c.13 0 1.25-.16 10.49-.83 8.41-.6 10.22-7 10.29-7.28l2.85.73c0 .09-2.35 8.72-12.93 9.48-9.14.66-10.23.8-10.27.8z"
            transform="translate(.44 -122.9)"
          />
          <path
            className="prefix__cls-5"
            d="M1036.11 534.55l36.9-2.66a4.62 4.62 0 004.16-3.52l3.64-15a4.61 4.61 0 00-4.82-5.69l-46.31 3.32a4.62 4.62 0 00-4 6.33l5.77 14.37a4.6 4.6 0 004.66 2.85z"
            transform="translate(.44 -122.9)"
          />
          <circle
            className="prefix__cls-5"
            cx={1027.97}
            cy={368.92}
            r={13.79}
          />
          <g clipPath="url(#prefix__clip-path)" id="prefix__eye">
            <circle
              className="prefix__cls-7"
              cx={1027.97}
              cy={368.92}
              r={10.91}
            />
            <circle cx={1036.64} cy={365.22} r={10.91} />
          </g>
          <circle
            className="prefix__cls-5"
            cx={1076.19}
            cy={365.44}
            r={13.79}
          />
          <g
            clipPath="url(#prefix__clip-path-2)"
            id="prefix__eye-2"
            data-name="eye"
          >
            <circle
              className="prefix__cls-7"
              cx={1076.19}
              cy={365.44}
              r={10.91}
            />
            <circle cx={1084.86} cy={361.75} r={10.91} />
          </g>
          <path
            className="prefix__cls-5"
            d="M1094.12 502h21.72v17.38h-21.72a5.92 5.92 0 01-5.92-5.92v-5.56a5.92 5.92 0 015.92-5.9z"
            transform="rotate(-42.29 943.224 448.644)"
          />
          <path
            className="prefix__cls-5"
            d="M1102.68 489.75h9.32a6.48 6.48 0 016.48 6.48v3h-15.84v-9.52l.04.04z"
            transform="rotate(-67.1 1018.106 432.732)"
          />
          <path
            className="prefix__cls-5"
            d="M1111.68 498.59h13.22v2.89a6.17 6.17 0 01-6.17 6.17h-7v-9.06h-.05z"
            transform="rotate(-21.19 789.768 440.43)"
          />
          <g>
            <path
              className="prefix__cls-5"
              d="M998.58 508h21.72v17.38h-21.72a5.92 5.92 0 01-5.92-5.92v-5.56a5.92 5.92 0 015.92-5.92z"
              transform="rotate(-131.29 978.912 455.105)"
            />
            <path
              className="prefix__cls-5"
              d="M991.85 494.89h15.84v3a6.48 6.48 0 01-6.48 6.48h-9.36v-9.52.04z"
              transform="rotate(-106.48 954.09 438.033)"
            />
            <path
              className="prefix__cls-5"
              d="M984.55 502.82h7a6.17 6.17 0 016.17 6.17v2.89h-13.17v-9.06z"
              transform="rotate(-152.39 976.295 445.833)"
            />
          </g>
          <path
            d="M1061.24 513.23l-12.4.9a1.16 1.16 0 00-.9 1.78c1 1.63 3.19 3.84 7.6 3.56s6.24-2.92 6.89-4.71a1.16 1.16 0 00-1.19-1.53z"
            transform="translate(.44 -122.9)"
            fill="#cf6d5a"
          />
          <g id="prefix__bee">
            <path
              className="prefix__cls-5"
              d="M352.13 367.36l2.27-.55.02-6.64-2.27.56-.02 6.63zM347.81 368.42l2.28-.56.02-6.63-2.28.56-.02 6.63zM337.82 370.87l2.27-.55.02-6.63-2.27.55-.02 6.63zM333.5 371.93l2.27-.55.02-6.64-2.27.56-.02 6.63z"
            />
            <rect
              x={315.32}
              y={460.99}
              width={54.35}
              height={24.8}
              rx={10.08}
              transform="rotate(-14 -131.306 418.883)"
              fill="#fbec6d"
            />
            <g clipPath="url(#prefix__clip-path-3)">
              <path
                className="prefix__cls-12"
                d="M377.64 475.17a10.6 10.6 0 01-17.9 0l-1.16.74a12 12 0 0020.22 0z"
                transform="translate(.44 -122.9)"
              />
            </g>
            <g clipPath="url(#prefix__clip-path-4)">
              <path
                d="M351.92 465.61a6.54 6.54 0 114.9 7.92 6.59 6.59 0 01-4.9-7.92"
                transform="translate(.44 -122.9)"
                fill="#f1f2f2"
              />
              <path
                className="prefix__cls-12"
                d="M358.29 467.06a6.54 6.54 0 114.91 7.94 6.6 6.6 0 01-4.91-7.92"
                transform="translate(.44 -122.9)"
              />
            </g>
            <path
              className="prefix__cls-15"
              transform="rotate(-14.73 -135.23 410.894)"
              d="M330.93 461.66h18.03v24.8h-18.03z"
            />
            <path
              className="prefix__cls-5"
              d="M305 440.79h7a20.76 20.76 0 0120.76 20.76v4.79h-7A20.76 20.76 0 01305 445.57v-4.79.01z"
              transform="rotate(-14.73 -156.296 390.394)"
            />
            <path
              d="M327.35 433.74h7a20.76 20.76 0 0120.76 20.76v4.79h-7a20.76 20.76 0 01-20.76-20.76v-4.79z"
              transform="rotate(75.27 421.166 385.357)"
              fill="#f5b1a6"
            />
          </g>
          <g>
            <path
              className="prefix__cls-17"
              d="M581.34 142.24v-6a2.51 2.51 0 00-2.51-2.51 2.51 2.51 0 00-2.51 2.51v6c0 16.79 11.06 25.87 23 27.24a2.51 2.51 0 002.76-2.51v-.24a2.48 2.48 0 00-2.2-2.47c-9.64-1.13-18.54-8.47-18.54-22.02z"
              transform="translate(.44 -122.9)"
            />
            <path
              className="prefix__cls-17"
              d="M574.86 153.85h6a2.51 2.51 0 012.5 2.51 2.5 2.5 0 01-2.5 2.51h-6c-16.8 0-25.88-11.05-27.24-23a2.51 2.51 0 012.5-2.76h.24a2.5 2.5 0 012.48 2.2c1.16 9.69 8.46 18.54 22.02 18.54z"
              transform="translate(.44 -122.9)"
            />
            <path
              d="M598.74 31.63h1.43a6.21 6.21 0 016.21 6.21V51H605a6.21 6.21 0 01-6.21-6.21V31.63h-.05z"
              fill="#a5ccc6"
            />
            <path
              className="prefix__cls-19"
              d="M585.08 46.8h25.2v11.88h-21.64a6.61 6.61 0 01-6.64-6.61v-2.23a3 3 0 013.08-3.04z"
            />
            <path
              d="M596.1 25.51l5.1 14.89 14.25.83 2.06-10.13a.73.73 0 00-1.36-.49l-2.17 4.05-5.71-10.26a.73.73 0 00-1.37.35v7.35l-9.67-7.36a.73.73 0 00-1.13.77z"
              stroke="#eb826a"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1.23}
              fill="#eb826a"
            />
            <path
              className="prefix__cls-19"
              d="M600.52 40.24h9.56a5.94 5.94 0 015.92 5.94v7.19h-22v-6.65a6.48 6.48 0 016.52-6.48z"
            />
            <path
              className="prefix__cls-19"
              d="M608.39 31.63h1.43a6.21 6.21 0 016.18 6.21V51h-1.4a6.21 6.21 0 01-6.21-6.21V31.63z"
            />
            <path
              className="prefix__cls-17"
              d="M632.83 142.24v-6a2.51 2.51 0 012.51-2.51 2.51 2.51 0 012.51 2.51v6c0 16.79-11.05 25.87-23 27.24a2.51 2.51 0 01-2.76-2.51v-.24a2.49 2.49 0 012.2-2.47c9.64-1.13 18.54-8.47 18.54-22.02z"
              transform="translate(.44 -122.9)"
            />
            <path
              className="prefix__cls-17"
              d="M639.32 153.85h-6a2.51 2.51 0 00-2.51 2.51 2.51 2.51 0 002.51 2.51h6c16.79 0 25.87-11.05 27.24-23a2.51 2.51 0 00-2.51-2.76h-.24a2.49 2.49 0 00-2.47 2.2c-1.13 9.69-8.47 18.54-22.02 18.54z"
              transform="translate(.44 -122.9)"
            />
            <g
              clipPath="url(#prefix__clip-path-5)"
              id="prefix__eye-3"
              data-name="eye"
            >
              <circle
                className="prefix__cls-7"
                cx={606.39}
                cy={46.8}
                r={4.54}
              />
              <circle cx={602.56} cy={45.95} r={4.54} />
            </g>
          </g>
          <g>
            <path
              className="prefix__cls-22"
              d="M382.88 143.88l.33-6a2.69 2.69 0 012.65-2.51 2.34 2.34 0 012.37 2.51l-.34 6c-.95 16.79-12.51 25.87-24.54 27.24a2.35 2.35 0 01-2.62-2.51v-.24a2.68 2.68 0 012.35-2.47c9.71-1.13 19.03-8.47 19.8-22.02z"
              transform="translate(.44 -122.9)"
            />
            <path
              className="prefix__cls-22"
              d="M388.7 155.49h-6a2.68 2.68 0 00-2.64 2.51 2.34 2.34 0 002.36 2.51h6c16.79 0 26.5-11 28.54-23a2.36 2.36 0 00-2.35-2.77h-.24a2.67 2.67 0 00-2.6 2.2c-1.67 9.65-9.51 18.55-23.07 18.55z"
              transform="translate(.44 -122.9)"
            />
            <path
              d="M358 167.65l-1-1.78c-2.89-5-1.06-11.56 4.08-14.73l1.82-1.14 1 1.79c2.89 5 1.06 11.56-4.08 14.72z"
              transform="translate(.44 -122.9)"
              fill="#dd6f3e"
            />
            <path
              className="prefix__cls-15"
              d="M350.62 168.85l-1.7-1.17a10.42 10.42 0 01-2.18-14.78l1.27-1.77 1.7 1.17a10.43 10.43 0 012.18 14.78z"
              transform="translate(.44 -122.9)"
            />
            <path
              className="prefix__cls-15"
              d="M353.88 163.14h7.68a7.43 7.43 0 016.83 4.31 3.72 3.72 0 003.38 2.25h5.66a3.48 3.48 0 013.54 3.56 8.71 8.71 0 01-8.6 8.31h-19.94a5.51 5.51 0 01-5.57-5.3h-.06v-.63l.33-5.94v-.23a6.79 6.79 0 016.75-6.33z"
              transform="translate(.44 -122.9)"
            />
            <path
              className="prefix__cls-22"
              d="M331.38 143.88l.34-6a2.34 2.34 0 00-2.37-2.51 2.67 2.67 0 00-2.64 2.51l-.34 6c-.95 16.79 9.59 25.87 21.47 27.24a2.68 2.68 0 002.9-2.51v-.24a2.32 2.32 0 00-2.07-2.47c-9.56-1.13-18.05-8.47-17.29-22.02z"
              transform="translate(.44 -122.9)"
            />
            <path
              className="prefix__cls-22"
              d="M324.25 155.49h6a2.35 2.35 0 012.36 2.51 2.68 2.68 0 01-2.65 2.51h-6c-16.79 0-25.25-11-25.94-23a2.68 2.68 0 012.66-2.77h.24a2.33 2.33 0 012.35 2.2c.58 9.65 7.42 18.55 20.98 18.55z"
              transform="translate(.44 -122.9)"
            />
            <g
              clipPath="url(#prefix__clip-path-6)"
              id="prefix__eye-4"
              data-name="eye"
            >
              <ellipse
                className="prefix__cls-7"
                cx={356.82}
                cy={169.7}
                rx={4.67}
                ry={4.41}
                transform="rotate(-44.19 205.701 107.708)"
              />
              <ellipse
                cx={360.69}
                cy={168.85}
                rx={4.67}
                ry={4.41}
                transform="rotate(-44.19 209.576 106.857)"
              />
            </g>
          </g>
          <path
            id="prefix__bee_path"
            data-name="bee path"
            d="M0 370.53C60.24 486 151.14 615 272 623.81c180 13.08 245.27-253.72 452.15-268.29 240.43-16.93 382.18 327.27 555.33 277.67 87.51-25.08 134.18-136.75 160.57-236.39"
            transform="translate(.44 -122.9)"
            stroke="#e1f4ec"
            strokeMiterlimit={10}
            fill="none"
          />
        </g>
      </g>
    </svg>
  )
}

export default SvgComponent

