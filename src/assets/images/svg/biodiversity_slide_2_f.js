import * as React from 'react';

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 1440 797"
      {...props}
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path
            className="prefix__cls-1"
            d="M358.53 617.37a5.72 5.72 0 014.24-6.73 5.47 5.47 0 016.25 4.7 5.7 5.7 0 01-4.23 6.73 5.48 5.48 0 01-6.26-4.7"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <path
            className="prefix__cls-1"
            d="M370.57 631a5.71 5.71 0 014.24-6.73 5.47 5.47 0 016.25 4.7 5.69 5.69 0 01-4.23 6.72 5.48 5.48 0 01-6.26-4.69"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-3">
          <rect
            className="prefix__cls-1"
            x={400.6}
            y={238.86}
            width={27.85}
            height={27.85}
            rx={13.92}
            transform="rotate(-17.04 414.493 252.786)"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-4">
          <circle className="prefix__cls-2" cx={416.13} cy={249.5} r={7.78} />
        </clipPath>
        <style>
          {
            '.prefix__cls-1{fill:none}.prefix__cls-2{fill:#fff}.prefix__cls-4{opacity:.5;mix-blend-mode:multiply}.prefix__cls-6{fill:#4480b5}.prefix__cls-8{fill:#98f4f4}.prefix__cls-12{fill:#f1f2f2}.prefix__cls-13{fill:#414042}.prefix__cls-15{fill:#e9fcff}.prefix__cls-16{fill:#c6baff}.prefix__cls-17{fill:#eed7ff}.prefix__cls-18{fill:#ff825c}.prefix__cls-19{fill:#fff861}.prefix__cls-20{fill:#c6edf5}.prefix__cls-22{fill:#c43f82}.prefix__cls-23{fill:#f4b5f4}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_1" data-name="Layer 1">

          <g id="prefix__Bug-2" data-name="Bug">

            <path
              className="prefix__cls-6"
              d="M363.86 654l5.8 6.93a1.13 1.13 0 001.65.11 1.26 1.26 0 00.11-1.73l-5.8-6.93a1.14 1.14 0 00-1.66-.11 1.27 1.27 0 00-.1 1.73zM354.52 658.74l.65 9.19a1.19 1.19 0 001.25 1.14 1.22 1.22 0 001.09-1.32l-.66-9.19a1.19 1.19 0 00-1.25-1.14 1.21 1.21 0 00-1.08 1.32zM370.64 649.12l8.6 1.91a1.17 1.17 0 001.39-.95 1.22 1.22 0 00-.9-1.45l-8.61-1.92a1.18 1.18 0 00-1.39.95 1.23 1.23 0 00.91 1.46zM341.5 625.89l-5.8-6.89a1.28 1.28 0 01.1-1.74 1.14 1.14 0 011.66.11l5.8 6.93a1.27 1.27 0 01-.11 1.74 1.14 1.14 0 01-1.65-.15zM335.35 635.39l-8.61-1.87a1.21 1.21 0 01-.91-1.45 1.17 1.17 0 011.38-1l8.61 1.86a1.22 1.22 0 01.91 1.46 1.17 1.17 0 01-1.38 1zM347 619.47l-.7-9.19a1.22 1.22 0 011.08-1.32 1.19 1.19 0 011.26 1.13l.69 9.19a1.2 1.2 0 01-1.07 1.32 1.18 1.18 0 01-1.26-1.13z"
            />
            <path
              d="M351 613l-14.3 13.22a21.54 21.54 0 00-6.83 14.46c0 .56-.05 1.12 0 1.68a21.69 21.69 0 004.83 13.55l.19.23c.17.21.36.42.54.63l.05.05a19.8 19.8 0 0012.52 6.42l1.67.1a19.52 19.52 0 0013.71-5.25l14.32-13.18z"
              fill="#58e0ed"
            />
            <path
              className="prefix__cls-8"
              d="M355.66 618.58l-25.83 23.78v-1.68l25-23zM364.95 629.67l-29.51 27.15-.05-.05-.73-.86 29.51-27.17.78.93zM374.23 640.75l-24.55 22.6-1.67-.11 25.45-23.41.77.92z"
            />
            <path
              d="M378.91 615.59a19.59 19.59 0 00-28.55-1.94l26.69 31.87a21.93 21.93 0 001.86-29.93z"
              fill="#6ec2e8"
            />
            <path
              d="M373.56 616.4c2.84-1.76 5.89 1.89 3.87 4.61"
              stroke="#000"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={0.6}
              fill="none"
            />
            <path
              className="prefix__cls-8"
              d="M350.58 613.45l26.69 31.86-.88.82-26.69-31.87.88-.81z"
            />
            <g id="prefix__eye-9" data-name="eye">
              <g clipPath="url(#prefix__clip-path)">
                <path
                  className="prefix__cls-12"
                  d="M358.53 617.37a5.72 5.72 0 014.24-6.73 5.49 5.49 0 016.26 4.7 5.71 5.71 0 01-4.24 6.73 5.48 5.48 0 01-6.26-4.7"
                />
                <path
                  className="prefix__cls-13"
                  d="M363.66 617.36a5.69 5.69 0 014.24-6.73 5.48 5.48 0 016.25 4.7 5.71 5.71 0 01-4.24 6.73 5.47 5.47 0 01-6.25-4.7"
                />
              </g>
            </g>
            <g id="prefix__eye-10" data-name="eye">
              <g clipPath="url(#prefix__clip-path-2)">
                <path
                  className="prefix__cls-12"
                  d="M370.57 631a5.71 5.71 0 014.24-6.73 5.47 5.47 0 016.25 4.7 5.69 5.69 0 01-4.23 6.72 5.48 5.48 0 01-6.26-4.69"
                />
                <path
                  className="prefix__cls-13"
                  d="M375.7 631a5.7 5.7 0 014.24-6.73 5.48 5.48 0 016.25 4.7 5.7 5.7 0 01-4.24 6.73 5.47 5.47 0 01-6.25-4.7"
                />
              </g>
            </g>
          </g>
          <g id="prefix__FLOAT">
            <g id="prefix__cloud">
              <rect
                className="prefix__cls-15"
                x={904.15}
                y={187.06}
                width={109.2}
                height={32.21}
                rx={11.58}
              />
              <rect
                className="prefix__cls-15"
                x={921.05}
                y={170.33}
                width={71.77}
                height={31.22}
                rx={11.23}
              />
            </g>
            <g id="prefix__cloud-2" data-name="cloud">
              <rect
                className="prefix__cls-15"
                x={262.71}
                y={160.75}
                width={109.2}
                height={32.21}
                rx={11.58}
              />
              <rect
                className="prefix__cls-15"
                x={279.61}
                y={144.03}
                width={71.77}
                height={31.22}
                rx={11.23}
              />
            </g>
            <rect
              className="prefix__cls-16"
              x={93.24}
              y={239.79}
              width={8.06}
              height={22.64}
              rx={2.52}
              transform="rotate(-16.45 97.262 251.083)"
            />
            <rect
              className="prefix__cls-17"
              x={91.74}
              y={247.92}
              width={22.64}
              height={8.06}
              rx={2.52}
              transform="matrix(.5 -.87 .87 .5 -166.66 215.23)"
            />
            <rect
              className="prefix__cls-16"
              x={95.51}
              y={252.73}
              width={5.54}
              height={15.63}
              rx={1.73}
              transform="rotate(-84.74 98.287 260.553)"
            />
            <rect
              className="prefix__cls-16"
              x={143.97}
              y={206.65}
              width={8.06}
              height={22.64}
              rx={2.52}
              transform="rotate(-16.45 147.994 217.947)"
            />
            <rect
              className="prefix__cls-17"
              x={142.48}
              y={214.78}
              width={22.64}
              height={8.06}
              rx={2.52}
              transform="rotate(-60 153.798 218.814)"
            />
            <rect
              className="prefix__cls-16"
              x={146.24}
              y={219.59}
              width={5.54}
              height={15.63}
              rx={1.73}
              transform="rotate(-84.74 149.014 227.411)"
            />
            <g id="prefix__cloud-3" data-name="cloud">
              <rect
                className="prefix__cls-15"
                x={366.02}
                y={466.15}
                width={109.2}
                height={32.21}
                rx={11.58}
              />
              <rect
                className="prefix__cls-15"
                x={382.92}
                y={449.43}
                width={71.77}
                height={31.22}
                rx={11.23}
              />
            </g>
            <g>
              <rect
                className="prefix__cls-18"
                x={1093.24}
                y={441.45}
                width={10.15}
                height={28.51}
                rx={5.07}
                transform="rotate(-16.45 1098.138 455.648)"
              />
              <rect
                className="prefix__cls-19"
                x={1091.35}
                y={451.7}
                width={28.51}
                height={10.15}
                rx={5.07}
                transform="rotate(-60 1105.609 456.77)"
              />
              <rect
                className="prefix__cls-18"
                x={1096.09}
                y={457.75}
                width={6.98}
                height={19.68}
                rx={3.49}
                transform="rotate(-84.74 1099.612 467.584)"
              />
            </g>
            <g>
              <rect
                className="prefix__cls-18"
                x={1126.6}
                y={378.29}
                width={10.15}
                height={28.51}
                rx={5.07}
                transform="rotate(-16.45 1131.522 392.53)"
              />
              <rect
                className="prefix__cls-19"
                x={1124.72}
                y={388.53}
                width={28.51}
                height={10.15}
                rx={5.07}
                transform="rotate(-60 1138.969 393.608)"
              />
              <rect
                className="prefix__cls-18"
                x={1129.45}
                y={394.59}
                width={6.98}
                height={19.68}
                rx={3.49}
                transform="rotate(-84.74 1132.98 404.416)"
              />
            </g>
            <g>
              <rect
                className="prefix__cls-18"
                x={496.94}
                y={193.21}
                width={10.15}
                height={28.51}
                rx={5.07}
                transform="rotate(-16.45 501.94 207.451)"
              />
              <rect
                className="prefix__cls-19"
                x={495.06}
                y={203.45}
                width={28.51}
                height={10.15}
                rx={5.07}
                transform="rotate(-60 509.313 208.523)"
              />
              <rect
                className="prefix__cls-18"
                x={499.79}
                y={209.51}
                width={6.98}
                height={19.68}
                rx={3.49}
                transform="rotate(-84.74 503.297 219.34)"
              />
            </g>
          </g>
          <path
            className="prefix__cls-20"
            d="M854.15 288.83a3.59 3.59 0 01-.35-4.87l24-29.45a3.17 3.17 0 014.63-.37 3.59 3.59 0 01.35 4.87l-24 29.45a3.17 3.17 0 01-4.63.37zM868.45 208.7a3.24 3.24 0 014.18 2.11l12.23 36.61a3.51 3.51 0 01-2 4.41 3.26 3.26 0 01-4.19-2.12l-12.23-36.61a3.5 3.5 0 012.01-4.4zM927.23 261.79a3.29 3.29 0 01-3.83 2.76l-36.23-7.16a3.45 3.45 0 01-2.63-4 3.3 3.3 0 013.84-2.76l36.23 7.16a3.44 3.44 0 012.62 4z"
          />
          <ellipse
            className="prefix__cls-20"
            cx={883.51}
            cy={253.67}
            rx={3.79}
            ry={3.98}
          />
          <g id="prefix__Bee-2" data-name="Bee">

            <path
              d="M389 252.18a36.57 36.57 0 0021.4-47 1.28 1.28 0 00-1.65-.75 36.58 36.58 0 00-21.41 47 1.28 1.28 0 001.66.75z"
              fill="#d4e8e1"
            />
            <rect
              className="prefix__cls-22"
              x={387.55}
              y={271.02}
              width={16.63}
              height={3.48}
              rx={1.74}
              transform="rotate(-76.48 395.845 272.762)"
            />
            <rect
              className="prefix__cls-22"
              x={382.28}
              y={269.75}
              width={16.63}
              height={3.48}
              rx={1.74}
              transform="matrix(.23 -.97 .97 .23 35.3 587.78)"
            />
            <rect
              className="prefix__cls-22"
              x={377.39}
              y={268.58}
              width={16.63}
              height={3.48}
              rx={1.74}
              transform="rotate(-76.48 385.697 270.318)"
            />
            <path
              className="prefix__cls-22"
              d="M423.36 224a1.4 1.4 0 01-1 1.55 11.93 11.93 0 00-9 12.77 1.38 1.38 0 01-1.12 1.48 1.41 1.41 0 01-1.69-1.24 14.78 14.78 0 0111.11-15.77 1.42 1.42 0 011.7 1.21z"
            />
            <path
              className="prefix__cls-22"
              d="M431.54 228.15a1.38 1.38 0 01-1.45 1.15 11.94 11.94 0 00-12.54 9.32 1.38 1.38 0 01-1.52 1.06 1.41 1.41 0 01-1.22-1.71 14.79 14.79 0 0115.49-11.51 1.41 1.41 0 011.24 1.69z"
            />
            <rect
              className="prefix__cls-23"
              x={339.05}
              y={255.29}
              width={43.7}
              height={27.85}
              rx={13.92}
              transform="rotate(-17.04 360.883 269.188)"
            />
            <rect
              className="prefix__cls-23"
              x={380.95}
              y={248.92}
              width={21.66}
              height={21.66}
              rx={10.83}
              transform="rotate(-17.04 391.746 259.752)"
            />
            <g
              clipPath="url(#prefix__clip-path-3)"
              id="prefix__head-5"
              data-name="head"
            >
              <rect
                className="prefix__cls-23"
                x={400.6}
                y={238.86}
                width={27.85}
                height={27.85}
                rx={13.92}
                transform="rotate(-17.04 414.493 252.786)"
              />
              <path
                className="prefix__cls-22"
                d="M424.66 266.27h-.41c-5.43-.15-7.44-6.53-7.52-6.8a.93.93 0 01.61-1.16.94.94 0 011.17.62c0 .05 1.7 5.37 5.79 5.48s5.73-3.81 5.8-4a.93.93 0 011.73.69c-.09.24-2.07 5.06-7.17 5.17z"
              />
            </g>
            <path
              d="M388.93 250.43a41 41 0 00-51.12-27.14 1.44 1.44 0 00-.95 1.8A41 41 0 00388 252.23a1.45 1.45 0 00.93-1.8z"
              fill="#eefff7"
            />
            <path
              d="M347.17 258.22q8.64-2.42 17.6-4.75c-.95 8.49 1.2 18 6.46 26.74L357 285c-6.36-8.53-9.65-18-9.83-26.78z"
              fill="#fdd7ff"
            />
            <circle className="prefix__cls-2" cx={416.13} cy={249.5} r={7.78} />
            <g clipPath="url(#prefix__clip-path-4)">
              <circle cx={421} cy={250.94} r={7.78} id="prefix__eyes" />
            </g>
          </g>
          <path
            d="M491.77 313.89a56.61 56.61 0 11-16.58-40 56.47 56.47 0 0116.58 40z"
            stroke="#c43f82"
            strokeMiterlimit={10}
            fill="none"
          />
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
