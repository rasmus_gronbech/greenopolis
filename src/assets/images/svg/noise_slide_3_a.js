import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 1440 797"
      {...props}
    >
      <defs>
        <linearGradient
          id="prefix__linear-gradient"
          x1={835.85}
          y1={253.16}
          x2={828.32}
          y2={289.61}
          gradientTransform="rotate(-10.93 863.295 265.76)"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset={0} stopColor="#80cfd3" />
          <stop offset={1} stopColor="#c0ffff" />
        </linearGradient>
        <linearGradient
          id="prefix__linear-gradient-2"
          x1={802}
          y1={254.02}
          x2={794.47}
          y2={290.45}
          gradientTransform="rotate(-10.93 826.216 264.69)"
          xlinkHref="#prefix__linear-gradient"
        />
        <clipPath id="prefix__clip-path">
          <path
            className="prefix__cls-1"
            d="M817.15 262.83l-21.29-4.56-3.74-.8-59.9-12.85 3.59-16.62a9.72 9.72 0 00-7.47-11.54l-2.81-.6a2.72 2.72 0 00-3.22 2.08l-5.06 23.5-1.87-.4a11.45 11.45 0 008.81 13.61l57.35 20.12 2 22.33a7.26 7.26 0 005.58 8.61l36.89 7.91a12.45 12.45 0 0014.78-9.53l.75-3.5a31.76 31.76 0 00-24.39-37.76z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <ellipse
            className="prefix__cls-1"
            cx={1133.52}
            cy={198.86}
            rx={5.27}
            ry={5.62}
          />
        </clipPath>
        <style>
          {
            ".prefix__cls-1,.prefix__cls-14{fill:none}.prefix__cls-2{fill:#4d81ba}.prefix__cls-3{fill:#6bb8ed}.prefix__cls-7{fill:#7dd3f4}.prefix__cls-8{fill:#ffe26e}.prefix__cls-14{stroke-linecap:round;stroke-linejoin:round;stroke:#ffe26e;stroke-width:3px}"
          }
        </style>
      </defs>
      <g id="prefix__FLOAT">
        <g id="prefix__helicopter">
          <rect
            className="prefix__cls-2"
            x={771.8}
            y={317.86}
            width={63.17}
            height={1.91}
            rx={0.91}
            transform="rotate(12.11 803.096 318.646)"
          />
          <path
            className="prefix__cls-2"
            d="M790.26 305.25l1.87.4-5.42 10.56-1.88-.4 5.43-10.56zM820.06 311.64l-2.3-.49 1.28 12 2.29.49-1.27-12z"
          />
          <rect
            className="prefix__cls-2"
            x={801.47}
            y={242.48}
            width={3.83}
            height={16.24}
            rx={1.81}
            transform="rotate(12.11 803.111 250.48)"
          />
          <rect
            className="prefix__cls-3"
            x={786.38}
            y={251.65}
            width={28.71}
            height={13.37}
            rx={6.35}
            transform="rotate(12.11 800.45 258.199)"
          />
          <path
            className="prefix__cls-3"
            d="M817.15 262.83l-21.29-4.56-3.74-.8-59.9-12.85 3.59-16.62a9.72 9.72 0 00-7.47-11.54l-2.81-.6-1.87-.4a2.7 2.7 0 00-3.22 2.07L715.38 241v.31a11.47 11.47 0 008.87 13.3l57.35 20.12 2 22.33a7.26 7.26 0 005.58 8.61l36.89 7.91a12.45 12.45 0 0014.78-9.53l.75-3.5a31.76 31.76 0 00-24.45-37.72z"
          />
          <g clipPath="url(#prefix__clip-path)">
            <path
              d="M818.4 276.38h35.25v22.93h-23.38a16.82 16.82 0 01-16.82-16.82v-1.15a5 5 0 014.95-4.96z"
              transform="rotate(12.11 833.229 287.707)"
              fill="url(#prefix__linear-gradient)"
            />
            <rect
              x={790.96}
              y={270.28}
              width={18.19}
              height={32.48}
              rx={7.6}
              transform="rotate(11.5 796.712 294.98)"
              fill="url(#prefix__linear-gradient-2)"
            />
          </g>
          <ellipse
            className="prefix__cls-7"
            cx={718.85}
            cy={247.61}
            rx={15.32}
            ry={15.28}
          />
          <ellipse
            id="prefix__propeller"
            className="prefix__cls-7"
            cx={807}
            cy={240.63}
            rx={9.07}
            ry={60.78}
            transform="rotate(-77.9 806.96 240.653)"
          />
          <g id="prefix__noise_lines_helicopter">
            <path
              className="prefix__cls-8"
              d="M881.55 249.82a15.92 15.92 0 00-12.83-18.54 2.34 2.34 0 10-1.24 4.51 14.23 14.23 0 012.54.68s.67.28.23.08l.58.28c.38.19.75.4 1.11.62l.52.35c.08.06.57.42.2.13q.45.36.87.75c.3.28.59.58.87.89l.4.47v-.06l.18.25a14.14 14.14 0 011.21 2.05s.29.67.1.2c.08.19.16.39.23.58.13.4.26.8.36 1.21s.18.82.25 1.23c-.07-.42 0 .07 0 .23v2.81c0-.26 0-.24 0 0a2.38 2.38 0 001.63 2.87 2.35 2.35 0 002.87-1.63zM899.51 249.06a28.29 28.29 0 00-23-32.77 2.4 2.4 0 00-2.88 1.63 2.35 2.35 0 001.63 2.87 23.65 23.65 0 0119.75 27 2.41 2.41 0 001.63 2.87 2.35 2.35 0 002.88-1.63z"
            />
          </g>
        </g>
        <g id="prefix__bird_3">
          <path
            d="M1124.46 228.71l18.32 3.84c1.83.38 2.6-1 1.73-3.16l-.79-1.92c-6.59-16-23.07-31.31-36.82-34.19h-.05l12.71 30.89c.87 2.12 3.07 4.16 4.9 4.54z"
            strokeLinecap="round"
            strokeLinejoin="round"
            stroke="#4d81ba"
            strokeWidth={3.26}
            fill="#4d81ba"
          />
          <path
            className="prefix__cls-3"
            d="M1134 183c7.65 2 13.46 10.1 13 18.06l-1.44 24.28-27.21-6.87 1.1-24.49c.37-7.98 6.9-12.98 14.55-10.98z"
          />
          <path
            className="prefix__cls-3"
            d="M1154.14 248.51l-12.67-1.59c-12.72-1.59-23.83-12.33-23.29-24.29l.28-6.24c0-.79.85-1.22 1.82-1l58.07 14.69a1.12 1.12 0 01.79 1.25c-1.88 11.2-12.41 18.76-25 17.18z"
          />
          <path
            className="prefix__cls-2"
            d="M1139.4 231.84l20.66 4.33c2.06.43 5-1 6.67-3.31l1.46-2c12.2-17 9.51-33.38-6-36.63h-.06L1138.6 227c-1.6 2.23-1.27 4.41.8 4.84z"
          />
          <g
            clipPath="url(#prefix__clip-path-2)"
            id="prefix__eye_grp"
            data-name="eye grp"
          >
            <ellipse
              cx={1133.52}
              cy={198.86}
              rx={5.27}
              ry={5.62}
              fill="#fff"
              id="prefix__eye_shape"
              data-name="eye shape"
            />
            <ellipse
              id="prefix__iris"
              cx={1130.95}
              cy={198.2}
              rx={4.45}
              ry={4.74}
              fill="#414042"
            />
          </g>
          <path
            d="M1122.48 203.46l-12.84 4.38c-1.44.5-2.92-.54-3.31-2.32a3.52 3.52 0 011.92-4.11l12.84-4.38c1.44-.5 2.93.54 3.31 2.32a3.52 3.52 0 01-1.92 4.11z"
            fill="#efb45e"
          />
          <path
            className="prefix__cls-8"
            d="M1120.32 202.9l-11.08-8.9a3.84 3.84 0 01-.76-4.62 2.44 2.44 0 013.76-1l11.07 8.9a3.8 3.8 0 01.76 4.61 2.43 2.43 0 01-3.75 1.01z"
          />
          <g id="prefix__sound_bird_3">
            <g id="prefix__heart">
              <path
                className="prefix__cls-8"
                d="M1077.7 193.77l4.31 8.35a2.07 2.07 0 002.89.89l12.27-7.21a5.68 5.68 0 002-7.45l-.38-.72a5 5 0 00-7-2.16z"
              />
              <path
                className="prefix__cls-8"
                d="M1088.74 200.71l3.94-2.31-7.91-15.32a5 5 0 00-7-2.17l-.62.37a5.71 5.71 0 00-2 7.49l4.75 9.19a6.34 6.34 0 008.84 2.75z"
              />
            </g>
            <path
              className="prefix__cls-8"
              d="M1104 220.06l-14.62-4.42a.93.93 0 00-1.16.66l-3.65 12.86a3.12 3.12 0 00-2.16-.88 3.49 3.49 0 103.23 4l4.11-14.44 12.86 3.9-2.79 11.46a3.16 3.16 0 00-2.09-.8 3.49 3.49 0 000 7 3.33 3.33 0 003.21-2.85l3.7-15.23a1 1 0 00-.64-1.26z"
            />
          </g>
        </g>
        <g id="prefix__sound_lines_bird_1">
          <path
            className="prefix__cls-14"
            d="M435.17 351.57l-9.98 3.03M436.27 362.59l-10.38-1M433.07 373.2l-9.21-4.89"
          />
        </g>
        <g id="prefix__sound_lines_bird_2">
          <path
            className="prefix__cls-14"
            d="M283.22 162.54l-7.41 5.34M287.27 171.38l-8.9 2.1M287.62 181.09l-9.03-1.46"
          />
        </g>
        <g id="prefix__noise_lines_car_1">
          <path
            className="prefix__cls-8"
            d="M574.42 588.41a15.92 15.92 0 00-13.17 18.3 2.36 2.36 0 002.88 1.63 2.4 2.4 0 001.63-2.88c.07.5 0-.13 0-.24v-.64-1.28-.62c.05-.57-.07.3 0-.21s.15-.81.26-1.21a11.37 11.37 0 01.37-1.17c0-.15.23-.58.07-.2.08-.19.16-.38.25-.56a14.08 14.08 0 011.33-2.16c.1-.13.08-.11-.06.07.07-.08.13-.16.2-.23s.26-.31.4-.46.57-.59.87-.86l.46-.4.24-.19q-.27.2-.06.06.5-.36 1-.69c.36-.21.72-.4 1.1-.58.06 0 .63-.28.39-.18l.41-.15a13.53 13.53 0 012.6-.64 2.35 2.35 0 001.63-2.88 2.39 2.39 0 00-2.87-1.63z"
          />
          <path
            className="prefix__cls-8"
            d="M568.22 571.58a28.28 28.28 0 00-23.65 32.31 2.34 2.34 0 002.87 1.63 2.39 2.39 0 001.63-2.87 23.73 23.73 0 014.66-17.47 24.18 24.18 0 0115.73-9.09 2.36 2.36 0 001.63-2.88 2.39 2.39 0 00-2.87-1.63z"
            id="prefix__yellow_soundwaves"
            data-name="yellow soundwaves"
          />
        </g>
        <g id="prefix__noise_lines_car_2">
          <path
            className="prefix__cls-8"
            d="M911.69 666.76a15.91 15.91 0 00-10.29-20.06 2.34 2.34 0 10-1.25 4.5 11.37 11.37 0 016.51 5.73c.09.17.17.34.25.52s-.07-.21.07.17.28.79.39 1.19.21.81.29 1.22c0 .05.1.64.06.37s0 .32 0 .38a14.48 14.48 0 010 2.55v.2c0 .13-.07.39-.11.59-.1.47-.24.94-.38 1.4a2.33 2.33 0 104.5 1.24z"
          />
          <path
            className="prefix__cls-8"
            d="M929.55 668.76a28.27 28.27 0 00-18.17-35.69c-2.85-1-4.08 3.54-1.24 4.5a23.62 23.62 0 0114.91 29.95c-.93 2.86 3.58 4.09 4.5 1.24z"
            id="prefix__yellow_soundwaves-2"
            data-name="yellow soundwaves"
          />
        </g>
        <g id="prefix__sound_bird_4">
          <path
            className="prefix__cls-14"
            d="M973.63 544.45l9.97-3.04M972.52 533.42l10.38 1M975.73 522.81l9.2 4.89"
          />
        </g>
      </g>
    </svg>
  )
}

export default SvgComponent

