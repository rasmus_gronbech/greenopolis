import * as React from 'react';

function AnimationSlide5(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 1440 797"
      {...props}
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path
            className="prefix__cls-1"
            d="M919.45 533.84l-1.23-4.14a17 17 0 0111.44-21.19l5.6-1.67 1.24 4.16a17 17 0 01-11.44 21.19z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <path
            className="prefix__cls-1"
            d="M964.66 518.22l-1.24-4.14a17 17 0 00-21.19-11.45l-5.6 1.68 1.23 4.14a17 17 0 0021.2 11.44z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-3">
          <path
            className="prefix__cls-1"
            d="M1077.25 447.54l-.46-3.41a13.55 13.55 0 0111.59-15.27l4.61-.63.47 3.41a13.57 13.57 0 01-11.6 15.27z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-4">
          <path
            className="prefix__cls-1"
            d="M1114.73 440.78l-.47-3.41a13.57 13.57 0 00-15.27-11.6l-4.61.63.46 3.41a13.57 13.57 0 0015.27 11.6z"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-1{fill:#eed7ff}.prefix__cls-4{fill:#e07070}.prefix__cls-5{fill:#ec987e}.prefix__cls-6{fill:#efc4b9}.prefix__cls-7{fill:#f99c43}.prefix__cls-8{fill:#f4cb5d}.prefix__cls-9{fill:#fced6f}.prefix__cls-10{fill:#e5a25e}.prefix__cls-11{fill:#f1e1b0}.prefix__cls-12{fill:#f7cb6b}.prefix__cls-13{fill:#efc1b3}.prefix__cls-14{fill:#f68b61}.prefix__cls-15{fill:#db5b4d}.prefix__cls-16{fill:#f7e1dc}.prefix__cls-19{fill:#c6baff}.prefix__cls-22{fill:#fff}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_1" data-name="Layer 1">
          <g id="prefix__animals">
            <g id="prefix__animals-2" data-name="animals">
              <g id="prefix__ANIMALS-3" data-name="ANIMALS">
                <rect
                  className="prefix__cls-4"
                  x={461.06}
                  y={183.63}
                  width={6.58}
                  height={18.49}
                  rx={3.29}
                  transform="rotate(-11.19 464.351 192.864)"
                />
                <rect
                  className="prefix__cls-5"
                  x={459.75}
                  y={190.7}
                  width={18.49}
                  height={6.58}
                  rx={3.29}
                  transform="rotate(-54.74 469.014 193.987)"
                />
                <rect
                  className="prefix__cls-6"
                  x={462.19}
                  y={194.24}
                  width={4.53}
                  height={12.77}
                  rx={2.26}
                  transform="rotate(-79.48 464.49 200.616)"
                />
                <rect
                  className="prefix__cls-4"
                  x={467.14}
                  y={257.55}
                  width={6.58}
                  height={18.49}
                  rx={3.29}
                  transform="rotate(-11.19 470.456 266.788)"
                />
                <rect
                  className="prefix__cls-5"
                  x={465.84}
                  y={264.62}
                  width={18.49}
                  height={6.58}
                  rx={3.29}
                  transform="rotate(-54.74 475.112 267.917)"
                />
                <rect
                  className="prefix__cls-6"
                  x={468.28}
                  y={268.17}
                  width={4.53}
                  height={12.77}
                  rx={2.26}
                  transform="rotate(-79.48 470.58 274.547)"
                />
                <g>
                  <rect
                    className="prefix__cls-7"
                    x={654.86}
                    y={498.31}
                    width={6.58}
                    height={18.49}
                    rx={3.29}
                    transform="rotate(-42.93 658.162 507.565)"
                  />
                  <rect
                    className="prefix__cls-8"
                    x={653.45}
                    y={502.77}
                    width={18.49}
                    height={6.58}
                    rx={3.29}
                    transform="rotate(-86.48 662.723 506.058)"
                  />
                  <rect
                    className="prefix__cls-9"
                    x={655.94}
                    y={511.83}
                    width={12.77}
                    height={4.53}
                    rx={2.26}
                    transform="rotate(-21.23 662.16 514.006)"
                  />
                </g>
                <g>
                  <rect
                    className="prefix__cls-7"
                    x={675.19}
                    y={439.34}
                    width={6.58}
                    height={18.49}
                    rx={3.29}
                    transform="rotate(-42.93 678.493 448.58)"
                  />
                  <rect
                    className="prefix__cls-8"
                    x={673.78}
                    y={443.8}
                    width={18.49}
                    height={6.58}
                    rx={3.29}
                    transform="rotate(-86.48 683.056 447.082)"
                  />
                  <rect
                    className="prefix__cls-9"
                    x={676.27}
                    y={452.85}
                    width={12.77}
                    height={4.53}
                    rx={2.26}
                    transform="rotate(-21.23 682.49 455.045)"
                  />
                </g>
                <g>
                  <rect
                    className="prefix__cls-4"
                    x={441.49}
                    y={225.37}
                    width={6.58}
                    height={18.49}
                    rx={3.29}
                    transform="rotate(-11.19 444.753 234.59)"
                  />
                  <rect
                    className="prefix__cls-5"
                    x={440.18}
                    y={232.45}
                    width={18.49}
                    height={6.58}
                    rx={3.29}
                    transform="rotate(-54.74 449.447 235.735)"
                  />
                  <rect
                    className="prefix__cls-6"
                    x={442.63}
                    y={235.99}
                    width={4.53}
                    height={12.77}
                    rx={2.26}
                    transform="rotate(-79.48 444.925 242.365)"
                  />
                </g>
              </g>
              <path
                className="prefix__cls-10"
                d="M186 648l5.92 34.41 24.52-9.48-6-34.78a11.1 11.1 0 00-5.2-7.95 10.66 10.66 0 00-9.51-.46c-6.73 2.71-11 10.96-9.73 18.26z"
              />
              <path
                className="prefix__cls-11"
                d="M169.53 654.35l21.56 28.37 24.52-9.49-21.79-28.67a18.61 18.61 0 00-9.44-6.31 17.29 17.29 0 00-11.27.23c-6.7 2.7-8.11 9.86-3.58 15.87z"
              />
              <path
                className="prefix__cls-12"
                d="M187.38 691.41c1.73 4.45 7 6.57 11.75 4.73l19.08-7.37a9.55 9.55 0 005.18-4.73 8.34 8.34 0 00.32-6.68c-1.72-4.46-7-6.58-11.75-4.74L192.89 680a9.53 9.53 0 00-5.18 4.72 8.36 8.36 0 00-.33 6.69z"
              />
              <path
                className="prefix__cls-10"
                d="M302.56 623.73l3.07 17.86 12.73-4.92-3.11-18a5.76 5.76 0 00-2.7-4.13 5.52 5.52 0 00-4.93-.23 8.94 8.94 0 00-5.06 9.42z"
              />
              <path
                className="prefix__cls-11"
                d="M294 627l11.18 14.72 12.73-4.92-11.28-14.8a9.69 9.69 0 00-4.9-3.27 8.93 8.93 0 00-5.85.12c-3.47 1.35-4.22 5.06-1.88 8.15z"
              />
              <path
                className="prefix__cls-12"
                d="M303.29 646.26a4.65 4.65 0 006.1 2.45l9.89-3.82a4.93 4.93 0 002.69-2.45 4.32 4.32 0 00.17-3.47 4.67 4.67 0 00-6.1-2.46l-9.89 3.83a4.93 4.93 0 00-2.69 2.45 4.32 4.32 0 00-.17 3.47z"
              />
              <path
                className="prefix__cls-10"
                d="M408.63 615.13l2.44 14.15 10.08-3.9-2.46-14.3a4.53 4.53 0 00-2.14-3.27 4.39 4.39 0 00-3.91-.19 7.1 7.1 0 00-4.01 7.51z"
              />
              <path
                className="prefix__cls-11"
                d="M401.87 617.75l8.86 11.66 10.09-3.9-9-11.79a7.64 7.64 0 00-3.89-2.59 7 7 0 00-4.63.09c-2.71 1.11-3.3 4.06-1.43 6.53z"
              />
              <path
                className="prefix__cls-12"
                d="M409.21 633a3.68 3.68 0 004.83 1.95l7.85-3A4 4 0 00424 630a3.46 3.46 0 00.13-2.75 3.69 3.69 0 00-4.83-2l-7.84 3a4 4 0 00-2.14 2 3.43 3.43 0 00-.11 2.75z"
              />
              <rect
                className="prefix__cls-13"
                x={99.53}
                y={130.78}
                width={69.14}
                height={21.35}
                rx={10.68}
                transform="rotate(-76.47 134.095 141.449)"
              />
              <rect
                className="prefix__cls-14"
                x={116.31}
                y={167.89}
                width={23.39}
                height={13.22}
                rx={6.61}
              />
              <rect
                className="prefix__cls-15"
                x={137.66}
                y={167.89}
                width={13.22}
                height={13.22}
                rx={6.61}
              />
              <rect
                className="prefix__cls-14"
                x={76.66}
                y={167.89}
                width={40.67}
                height={13.22}
                rx={6.61}
              />
              <rect
                className="prefix__cls-16"
                x={99.02}
                y={109.93}
                width={21.35}
                height={69.14}
                rx={10.68}
                transform="rotate(-33.34 109.697 144.492)"
              />
              <g>
                <rect
                  className="prefix__cls-13"
                  x={218.08}
                  y={216.61}
                  width={34.1}
                  height={10.53}
                  rx={5.26}
                  transform="rotate(-76.47 235.117 221.878)"
                />
                <rect
                  className="prefix__cls-14"
                  x={226.35}
                  y={234.91}
                  width={11.53}
                  height={6.52}
                  rx={3.26}
                />
                <rect
                  className="prefix__cls-15"
                  x={236.88}
                  y={234.91}
                  width={6.52}
                  height={6.52}
                  rx={3.26}
                />
                <rect
                  className="prefix__cls-14"
                  x={206.8}
                  y={234.91}
                  width={20.06}
                  height={6.52}
                  rx={3.26}
                />
                <rect
                  className="prefix__cls-16"
                  x={217.83}
                  y={206.33}
                  width={10.53}
                  height={34.1}
                  rx={5.26}
                  transform="rotate(-33.34 223.068 223.363)"
                />
              </g>
              <g>
                <path
                  className="prefix__cls-1"
                  d="M935.41 507.32l-6.74 2a28.56 28.56 0 01-35.53-19.19L890.4 481l5.76-1.73A29.59 29.59 0 01933 499.12z"
                />
                <path
                  className="prefix__cls-1"
                  d="M919.45 533.84l-1.23-4.14a17 17 0 0111.44-21.19l5.6-1.67 1.24 4.16a17 17 0 01-11.44 21.19z"
                />
                <g clipPath="url(#prefix__clip-path)">
                  <path
                    className="prefix__cls-1"
                    d="M941.35 527.21l-.3.18.32-.1-.02-.08z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.31 527.08l-.73.45.47-.14.3-.18-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.27 526.95l-1.16.72.47-.14.73-.45-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.23 526.82l-1.59.99.47-.14 1.16-.72-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.19 526.69l-2.02 1.26.47-.14 1.59-.99-.04-.13zM941.15 526.56l-2.44 1.53.46-.14 2.02-1.26-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.12 526.43l-2.88 1.8.47-.14 2.44-1.53-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.08 526.3l-3.31 2.07.47-.14 2.88-1.8-.04-.13zM941.04 526.17l-3.74 2.34.47-.14 3.31-2.07-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941 526.05l-4.17 2.6.47-.14 3.74-2.34-.04-.12z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.96 525.92l-4.6 2.87.47-.14 4.17-2.6-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.92 525.79l-5.03 3.14.47-.14 4.6-2.87-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.88 525.66l-5.45 3.41.46-.14 5.03-3.14-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.85 525.53l-5.89 3.68.47-.14 5.45-3.41-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.81 525.4l-6.32 3.95.47-.14 5.89-3.68-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.77 525.27l-6.75 4.22.47-.14 6.32-3.95-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.73 525.14l-7.18 4.49.47-.14 6.75-4.22-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.69 525.01l-7.61 4.76.47-.14 7.18-4.49-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.65 524.88l-8.03 5.03.46-.14 7.61-4.76-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.62 524.76l-8.47 5.29.47-.14 8.03-5.03-.03-.12z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.58 524.63l-8.9 5.56.47-.14 8.47-5.29-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.54 524.5l-9.33 5.83.47-.14 8.9-5.56-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.5 524.37l-9.76 6.1.47-.14 9.33-5.83-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.46 524.24l-10.19 6.37.47-.14 9.76-6.1-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.42 524.11l-10.62 6.64.47-.14 10.19-6.37-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.38 523.98l-11.04 6.91.46-.14 10.62-6.64-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.35 523.85l-11.48 7.18.47-.14 11.04-6.91-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.31 523.73l-11.91 7.44.47-.14 11.48-7.18-.04-.12z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.27 523.6l-12.34 7.71.47-.14 11.91-7.44-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.23 523.47l-12.77 7.98.47-.14 12.34-7.71-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.19 523.34l-13.2 8.25.47-.14 12.77-7.98-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.15 523.21l-13.63 8.52.47-.14 13.2-8.25-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.11 523.08l-14.05 8.79.46-.14 13.63-8.52-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.08 522.95l-14.49 9.06.47-.14 14.05-8.79-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940.04 522.82l-14.92 9.33.47-.14 14.49-9.06-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M940 522.69l-15.35 9.6.47-.14 14.92-9.33-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.96 522.57l-15.78 9.86.47-.14 15.35-9.6-.04-.12zM939.92 522.44l-16.21 10.13.47-.14 15.78-9.86-.04-.13zM939.88 522.31l-16.63 10.4.46-.14 16.21-10.13-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.85 522.18l-17.07 10.67.47-.14 16.63-10.4-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.81 522.05l-17.5 10.94.47-.14 17.07-10.67-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.77 521.92l-17.93 11.21.47-.14 17.5-10.94-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.73 521.79l-18.36 11.48.47-.14 17.93-11.21-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.69 521.66l-18.79 11.75.47-.14 18.36-11.48-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M937.98 515.95l-20.11 12.58 1.58 5.31 1.45-.43 18.79-11.75-1.71-5.71zM917.87 528.53l20.11-12.58-2.72-9.11-21.92 6.54 4.53 15.15z"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M936.77 504.79l6.74-2a28.56 28.56 0 0019.19-35.53l-2.7-9.19-5.77 1.73a29.57 29.57 0 00-19.87 36.79z"
                />
                <path
                  className="prefix__cls-1"
                  d="M964.66 518.22l-1.24-4.14a17 17 0 00-21.19-11.45l-5.6 1.68 1.23 4.14a17 17 0 0021.2 11.44z"
                />
                <g clipPath="url(#prefix__clip-path-2)">
                  <path
                    className="prefix__cls-1"
                    d="M942.71 524.67h.35l-.32.09-.03-.09zM942.67 524.55l.86-.02-.47.14h-.35l-.04-.12z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.64 524.42l1.36-.03-.47.14-.86.02-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.6 524.29l1.87-.04-.47.14-1.36.03-.04-.13zM942.56 524.16l2.38-.05-.47.14-1.87.04-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.52 524.03l2.89-.06-.47.14-2.38.05-.04-.13zM942.48 523.9l3.4-.07-.47.14-2.89.06-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.44 523.77l3.9-.08-.46.14-3.4.07-.04-.13zM942.4 523.64l4.41-.09-.47.14-3.9.08-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.37 523.51l4.91-.1-.47.14-4.41.09-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.33 523.39l5.42-.12-.47.14-4.91.1-.04-.12zM942.29 523.26l5.93-.13-.47.14-5.42.12-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.25 523.13l6.44-.14-.47.14-5.93.13-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.21 523l6.94-.15-.46.14-6.44.14-.04-.13zM942.17 522.87l7.45-.16-.47.14-6.94.15-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.13 522.74l7.96-.17-.47.14-7.45.16-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.1 522.61l8.46-.18-.47.14-7.96.17-.03-.13zM942.06 522.48l8.97-.19-.47.14-8.46.18-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M942.02 522.36l9.48-.21-.47.14-8.97.19-.04-.12z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.98 522.23l9.99-.22-.47.14-9.48.21-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.94 522.1l10.49-.23-.46.14-9.99.22-.04-.13zM941.9 521.97l11-.24-.47.14-10.49.23-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.87 521.84l11.5-.25-.47.14-11 .24-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.83 521.71l12.01-.26-.47.14-11.5.25-.04-.13zM941.79 521.58l12.52-.27-.47.14-12.01.26-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.75 521.45l13.03-.28-.47.14-12.52.27-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.71 521.32l13.54-.29-.47.14-13.03.28-.04-.13zM941.67 521.2l14.04-.31-.46.14-13.54.29-.04-.12zM941.63 521.07l14.55-.32-.47.14-14.04.31-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.6 520.94l15.05-.33-.47.14-14.55.32-.03-.13zM941.56 520.81l15.56-.34-.47.14-15.05.33-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.52 520.68l16.07-.35-.47.14-15.56.34-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.48 520.55l16.58-.36-.47.14-16.07.35-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.44 520.42l17.08-.37-.46.14-16.58.36-.04-.13zM941.4 520.29l17.59-.38-.47.14-17.08.37-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.37 520.16l18.09-.39-.47.14-17.59.38-.03-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.33 520.03l18.6-.4-.47.14-18.09.39-.04-.13zM941.29 519.91l19.11-.42-.47.14-18.6.4-.04-.12z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.25 519.78l19.62-.43-.47.14-19.11.42-.04-.13zM941.21 519.65l20.13-.44-.47.14-19.62.43-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.17 519.52l20.63-.45-.46.14-20.13.44-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.13 519.39l21.14-.46-.47.14-20.63.45-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M941.1 519.26l21.64-.47-.47.14-21.14.46-.03-.13zM941.06 519.13l22.15-.48-.47.14-21.64.47-.04-.13z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M939.35 513.42l23.72-.52 1.59 5.32-1.45.43-22.15.48-1.71-5.71z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M963.07 512.9l-23.72.52-2.72-9.11 21.92-6.55 4.52 15.14z"
                  />
                </g>
                <rect
                  className="prefix__cls-19"
                  x={932.74}
                  y={497.89}
                  width={6.61}
                  height={14.23}
                  rx={3.3}
                  transform="rotate(-16.63 936.175 505.075)"
                />
              </g>
              <g>
                <path
                  className="prefix__cls-1"
                  d="M1093.05 428.63l-5.55.76a22.73 22.73 0 01-25.6-19.39l-1-7.56 4.75-.64a23.53 23.53 0 0126.51 20.13z"
                />
                <path
                  className="prefix__cls-1"
                  d="M1077.25 447.54l-.46-3.41a13.55 13.55 0 0111.59-15.27l4.61-.63.47 3.41a13.57 13.57 0 01-11.6 15.27z"
                />
                <g clipPath="url(#prefix__clip-path-3)">
                  <path
                    className="prefix__cls-1"
                    d="M1095.29 445l-.26.11.27-.04-.01-.07zM1095.27 444.89l-.63.27.39-.05.26-.11-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.26 444.78l-1 .43.38-.05.63-.27-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.24 444.68l-1.37.58.39-.05 1-.43-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.23 444.57l-1.74.75.38-.06 1.37-.58-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.21 444.46l-2.11.91.39-.05 1.74-.75-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.2 444.36l-2.49 1.06.39-.05 2.11-.91-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.18 444.25l-2.85 1.22.38-.05 2.49-1.06-.02-.11zM1095.17 444.15l-3.23 1.38.39-.06 2.85-1.22-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.16 444.04l-3.6 1.54.38-.05 3.23-1.38-.01-.11zM1095.14 443.93l-3.97 1.7.39-.05 3.6-1.54-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.13 443.83l-4.34 1.86.38-.06 3.97-1.7-.01-.1zM1095.11 443.72l-4.71 2.02.39-.05 4.34-1.86-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.1 443.62l-5.08 2.17.38-.05 4.71-2.02-.01-.1zM1095.08 443.51l-5.45 2.33.39-.05 5.08-2.17-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.07 443.4l-5.83 2.5.39-.06 5.45-2.33-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.05 443.3l-6.19 2.65.38-.05 5.83-2.5-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.04 443.19l-6.57 2.81.39-.05 6.19-2.65-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.03 443.08l-6.94 2.98.38-.06 6.57-2.81-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.01 442.98l-7.31 3.13.39-.05 6.94-2.98-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095 442.87l-7.68 3.29.38-.05 7.31-3.13-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.98 442.77l-8.05 3.44.39-.05 7.68-3.29-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.97 442.66l-8.43 3.61.39-.06 8.05-3.44-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.95 442.55l-8.79 3.77.38-.05 8.43-3.61-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.94 442.45l-9.17 3.92.39-.05 8.79-3.77-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.92 442.34l-9.53 4.08.38-.05 9.17-3.92-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.91 442.24l-9.91 4.24.39-.06 9.53-4.08-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.89 442.13l-10.27 4.4.38-.05 9.91-4.24-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.88 442.02l-10.65 4.56.39-.05 10.27-4.4-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.87 441.92l-11.02 4.71.38-.05 10.65-4.56-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.85 441.81l-11.39 4.88.39-.06 11.02-4.71-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.84 441.7l-11.77 5.04.39-.05 11.39-4.88-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.82 441.6l-12.13 5.19.38-.05 11.77-5.04-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.81 441.49l-12.51 5.36.39-.06 12.13-5.19-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.79 441.39l-12.87 5.51.38-.05 12.51-5.36-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.78 441.28l-13.25 5.67.39-.05 12.87-5.51-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.76 441.18l-13.62 5.82.39-.05 13.25-5.67-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.75 441.07l-13.99 5.99.38-.06 13.62-5.82-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.73 440.96l-14.36 6.15.39-.05 13.99-5.99-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.72 440.86l-14.73 6.3.38-.05 14.36-6.15-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.71 440.75l-15.11 6.46.39-.05 14.73-6.3-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.69 440.64l-15.47 6.63.38-.06 15.11-6.46-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.68 440.54l-15.85 6.78.39-.05 15.47-6.63-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.66 440.43l-16.22 6.94.39-.05 15.85-6.78-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1094.02 435.73l-17.37 7.43.6 4.38 1.19-.17 16.22-6.94-.64-4.7z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1076.65 443.16l17.37-7.43-1.03-7.5-18.04 2.47 1.7 12.46z"
                  />
                </g>
                <path
                  className="prefix__cls-1"
                  d="M1094.43 426.8l5.55-.75a22.74 22.74 0 0019.44-25.61l-1-7.55-4.75.65a23.54 23.54 0 00-20.13 26.51z"
                />
                <path
                  className="prefix__cls-1"
                  d="M1114.73 440.78l-.47-3.41a13.57 13.57 0 00-15.27-11.6l-4.61.63.46 3.41a13.57 13.57 0 0015.27 11.6z"
                />
                <g clipPath="url(#prefix__clip-path-4)">
                  <path
                    className="prefix__cls-1"
                    d="M1096.67 443.17l.28.04-.27.04-.01-.08z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.66 443.06l.67.1-.38.05-.28-.04-.01-.11zM1096.64 442.96l1.08.14-.39.06-.67-.1-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.63 442.85l1.48.2-.39.05-1.08-.14-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.61 442.75l1.88.25-.38.05-1.48-.2-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.6 442.64l2.28.3-.39.06-1.88-.25-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.58 442.54l2.68.35-.38.05-2.28-.3-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.57 442.43l3.08.41-.39.05-2.68-.35-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.56 442.32l3.47.47-.38.05-3.08-.41-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.54 442.22l3.88.51-.39.06-3.47-.47-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.53 442.11l4.28.57-.39.05-3.88-.51-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.51 442l4.68.63-.38.05-4.28-.57-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.5 441.9l5.08.67-.39.06-4.68-.63-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.48 441.79l5.48.73-.38.05-5.08-.67-.02-.11zM1096.47 441.69l5.88.78-.39.05-5.48-.73-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.45 441.58l6.28.84-.38.05-5.88-.78-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.44 441.47l6.68.89-.39.06-6.28-.84-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.42 441.37l7.09.94-.39.05-6.68-.89-.02-.1zM1096.41 441.26l7.48 1-.38.05-7.09-.94-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.39 441.15l7.89 1.06-.39.05-7.48-1-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.38 441.05l8.28 1.1-.38.06-7.89-1.06-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.37 440.94l8.68 1.16-.39.05-8.28-1.1-.01-.11zM1096.35 440.84l9.08 1.21-.38.05-8.68-1.16-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.34 440.73l9.48 1.27-.39.05-9.08-1.21-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.32 440.63l9.88 1.31-.38.06-9.48-1.27-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.31 440.52l10.28 1.37-.39.05-9.88-1.31-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.29 440.41l10.69 1.43-.39.05-10.28-1.37-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.28 440.31l11.08 1.47-.38.06-10.69-1.43-.01-.1zM1096.26 440.2l11.49 1.53-.39.05-11.08-1.47-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.25 440.09l11.88 1.59-.38.05-11.49-1.53-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.23 439.99l12.29 1.64-.39.05-11.88-1.59-.02-.1zM1096.22 439.88l12.68 1.69-.38.06-12.29-1.64-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.21 439.78l13.08 1.74-.39.05-12.68-1.69-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.19 439.67l13.48 1.8-.38.05-13.08-1.74-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.18 439.56l13.88 1.85-.39.06-13.48-1.8-.01-.11zM1096.16 439.46l14.29 1.9-.39.05-13.88-1.85-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.15 439.35l14.68 1.96-.38.05-14.29-1.9-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.13 439.25l15.09 2.01-.39.05-14.68-1.96-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.12 439.14l15.48 2.06-.38.06-15.09-2.01-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.11 439.03l15.88 2.12-.39.05-15.48-2.06-.01-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.09 438.93l16.29 2.17-.39.05-15.88-2.12-.02-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.08 438.82l16.68 2.23-.38.05-16.29-2.17-.01-.11zM1096.06 438.71l17.09 2.28-.39.06-16.68-2.23-.02-.11z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1096.05 438.61l17.48 2.33-.38.05-17.09-2.28-.01-.1z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1095.4 433.9l18.73 2.5.59 4.38-1.19.16-17.48-2.33-.65-4.71z"
                  />
                  <path
                    className="prefix__cls-1"
                    d="M1114.13 436.4l-18.73-2.5-1.02-7.5 18.04-2.46 1.71 12.46z"
                  />
                </g>
                <rect
                  className="prefix__cls-19"
                  x={1091.2}
                  y={421.22}
                  width={5.26}
                  height={11.33}
                  rx={2.63}
                  transform="rotate(-7.79 1093.827 426.909)"
                />
              </g>
              <path
                className="prefix__cls-22"
                d="M979.53 113.12a13.92 13.92 0 0112.69-13.88 13.94 13.94 0 0113.47-10.41h1.84A13.93 13.93 0 011021 99.18h4a14 14 0 0113.93 13.94v1.45a13.53 13.53 0 01-.69 4.32 2.18 2.18 0 01-2.07 1.49h-53.88a2.18 2.18 0 01-2.07-1.49 13.53 13.53 0 01-.69-4.32v-.73-.72zM1074.89 86.16a9.89 9.89 0 019-9.84 9.88 9.88 0 019.55-7.37h1.3a9.86 9.86 0 019.53 7.34h2.83a9.88 9.88 0 019.87 9.87v1.03a9.75 9.75 0 01-.48 3.06 1.55 1.55 0 01-1.47 1.06h-38.16a1.55 1.55 0 01-1.47-1.06 9.76 9.76 0 01-.49-3.06v-.51c0-.17-.01-.35-.01-.52zM1000.47 470.14a13.94 13.94 0 0112.68-13.88 14 14 0 0113.48-10.4h1.83a13.94 13.94 0 0113.46 10.35h4a14 14 0 0113.93 13.93v1.45a13.63 13.63 0 01-.69 4.33 2.17 2.17 0 01-2.07 1.48h-53.86a2.18 2.18 0 01-2.07-1.48 13.91 13.91 0 01-.68-4.33v-.72c0-.24-.01-.49-.01-.73zM1071.41 505.64a8.28 8.28 0 017.55-8.27 8.3 8.3 0 018-6.19h1.09a8.3 8.3 0 018 6.16h2.38a8.31 8.31 0 018.3 8.3v.86a7.93 7.93 0 01-.42 2.58 1.29 1.29 0 01-1.23.89h-32.07a1.29 1.29 0 01-1.23-.89 7.92 7.92 0 01-.41-2.58v-.43c0-.15.04-.28.04-.43zM669 60.72a20.73 20.73 0 0118.88-20.65A20.74 20.74 0 01708 24.59h2.73a20.74 20.74 0 0120 15.4h5.94a20.75 20.75 0 0120.73 20.73v2.17a20.32 20.32 0 01-1 6.43 3.24 3.24 0 01-3.09 2.22h-80.18a3.25 3.25 0 01-3.08-2.22 20.32 20.32 0 01-1-6.43v-1.08c0-.36-.05-.72-.05-1.09zM312 271.22a20.75 20.75 0 0118.89-20.66 20.74 20.74 0 0120.05-15.48h2.73a20.73 20.73 0 0120 15.4h5.95a20.76 20.76 0 0120.73 20.74v2.16a20.11 20.11 0 01-1 6.44 3.24 3.24 0 01-3.08 2.21h-80.16a3.24 3.24 0 01-3.08-2.21 20.39 20.39 0 01-1-6.44v-1.08q0-.54-.03-1.08zM596.92 93.35a13.92 13.92 0 0112.69-13.88 13.94 13.94 0 0113.48-10.41h1.83a14 14 0 0113.46 10.35h4a14 14 0 0113.92 13.94v1.45a13.53 13.53 0 01-.69 4.32 2.18 2.18 0 01-2.07 1.49h-53.86a2.18 2.18 0 01-2.07-1.49 13.81 13.81 0 01-.69-4.32v-.73-.72z"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
}

export default AnimationSlide5;
