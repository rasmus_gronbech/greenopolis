// import React from 'react';
// import renderer from 'react-test-renderer';
// import { render, screen, act } from '@testing-library/react';
// import { GlobalStateContext, GlobalDispatchContext } from '../../context/GlobalContextProvider';

// import Header from '../Header';

// describe('Header', () => {
//   it('renders correctly', () => {
//     const tree = renderer
//       .create(<Header siteTitle="Default Starter" />)
//       .toJSON();
//     expect(tree).toMatchSnapshot();
//   });
// });

// describe('header contains the blog name', () => {
//   const customRender = (ui, {
//     dispatchProviderProps, providerProps, ...renderOptions
//   }) => {
//     render(
//       <GlobalDispatchContext.Provider {...dispatchProviderProps}>
//         <GlobalStateContext.Provider {...providerProps}>{ui}</GlobalStateContext.Provider>
//       </GlobalDispatchContext.Provider>,
//       renderOptions,
//     );
//   };

//   const headerData = {
//     node: {
//       id: 'dGVybTozMg==',
//       slug: 'main-header-en',
//       name: 'Main header en',
//       menuItems: {
//         nodes: [
//           {
//             label: 'En – water chapter',
//             url: 'http://rasmusgronbech.dk/dev/regreen/water_post/en-water-chapter/',
//           },
//           {
//             label: 'Chapter two',
//             url: 'http://rasmusgronbech.dk/dev/regreen/chapter_two_post/this-is-a-chapter-two-post-in-en/',
//           },
//         ],
//       },
//     },
//   };

//   it('NameConsumer shows value from provider', () => {
//     const providerProps = {
//       value: {
//         lang: 'en',
//         translationLinks: [],
//       },
//     };

//     console.log(headerData);
//     customRender(<Header headerData={headerData} />, { providerProps });
//     expect(screen.getByText('My Great Blog')).toBeTruthy();
//   });
// });
