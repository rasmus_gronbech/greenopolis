import * as React from 'react';

function ModalIcon(props) {
  return (
    <svg
      width={13}
      height={13}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M0 1.162A.675.675 0 01.19.695.642.642 0 01.645.5h11.71c.356 0 .645.297.645.662v10.676a.675.675 0 01-.19.467.642.642 0 01-.455.195H.645a.638.638 0 01-.456-.194.67.67 0 01-.189-.468V1.162zm5.85.671H1.3v9.334h4.55V1.833zm1.3 0v9.334h4.55V1.833H7.15zm.65 1.334h3.25V4.5H7.8V3.167zm0 2h3.25V6.5H7.8V5.167z"
        fill="#E0F4EC"
      />
    </svg>
  );
}

export default ModalIcon;
