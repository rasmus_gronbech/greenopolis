import * as React from 'react';

function TextBubbleIcon(props) {
  return (
    <svg
      className="text-bubble-icon"
      width={62}
      height={69}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g className="bubble">
        <path
          d="M31 53.24c13.806 0 25-10.262 25-21.928C56 19.648 44.806 11 31 11S6 20.456 6 32.122c0 4.553 1.706 8.772 4.61 12.219L7.562 58.656 19.8 51.01A28.691 28.691 0 0031 53.241z"
          fill="#E0F4EC"
        />
        <path
          d="M31 61.159c17.12 0 31-14.859 31-31.75C62 12.52 48.12 0 31 0 13.88 0 0 13.691 0 30.582c0 6.592 2.116 12.7 5.716 17.69L1.937 69l15.175-11.072c4.397 2.153 9.12 3.252 13.888 3.23z"
          fill="#E0F4EC"
        />
        <circle cx={31} cy={31} r={24.5} fill="#E0F4EC" stroke="#294C35" />
      </g>
      <path
        className="icon"
        d="M18 20.324a1.35 1.35 0 01.38-.933c.242-.248.568-.388.91-.391h23.42c.713 0 1.29.593 1.29 1.324v21.352a1.35 1.35 0 01-.38.933 1.284 1.284 0 01-.91.391H19.29c-.342 0-.67-.14-.912-.388a1.341 1.341 0 01-.378-.936V20.324zm11.7 1.343h-9.1v18.666h9.1V21.667zm2.6 0v18.666h9.1V21.667h-9.1zm1.3 2.666h6.5V27h-6.5v-2.667zm0 4h6.5V31h-6.5v-2.667z"
        fill="#294C35"
      />
    </svg>
  );
}

export default TextBubbleIcon;
