import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import {
  Button,
} from 'react-bootstrap';
import parse from 'html-react-parser';
import { CgClose } from 'react-icons/cg';
import './slide-content.scss';

const SlideContent = ({
  click, open, chapterTitle, slideTitle, slideText,
  slideAdditionalFieldsContent, slideIndex, slideAnimation,
}) => {
  const [show, setShow] = useState(false);
  const [activePopUpText, setActivePopUpText] = useState();

  useEffect(() => {
    if (!open) {
      document.querySelector('.btn-modal-close').style.display = 'none';
      setShow(false);
    } else {
      document.querySelector('.btn-modal-close').style.display = 'block';
    }
  }, [open]);

  const handleClose = () => {
    document.querySelector('.btn-modal-close').style.display = 'none';
    setShow(false);
    setShow(false);
    click();
  };

  return (
    <div className="slide-container">
      <div className={`custom-modal  ${open ? 'show' : ''}`}>
        <div className="header d-flex justify-content-between align-items-center">
          {chapterTitle ? <div className="label-desktop">{chapterTitle}</div> : ''}
          <Button variant="primary btn-modal-close" onClick={handleClose}>
            <CgClose stroke="white" size="1.4rem" />
          </Button>
        </div>
        {slideText ? <h2>{slideTitle}</h2> : ''}
        {slideTitle ? <div className="modal-text">{parse(slideText)}</div> : ''}
      </div>
      {slideAdditionalFieldsContent ? (
        slideAdditionalFieldsContent.map((item, index) => (
          <div key={`item_${index.toString()}`} className={`interaction-container position-absolute slide-${slideIndex} pop-up-text-${index}`}>
            <button
              type="button"
              className="interaction-btn"
              onMouseEnter={() => setActivePopUpText(index)}
              onMouseLeave={() => setActivePopUpText(null)}
            >
              <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                <motion.circle
                  cx="25"
                  cy="25"
                  r="24.5"
                  stroke="#E0F4EC"
                  style={{
                    originX: 'center',
                    originY: 'center',
                    transformBox: 'fill-box',
                  }}
                  animate={{
                    scale: [0.5, 1],
                    opacity: [1, 0],
                  }}
                  transition={{ duration: 2, repeat: Infinity }}
                />
                <circle cx="25" cy="25" r="11.5" fill="#E0F4EC" stroke="#E0F4EC" />
              </svg>
            </button>
            { index === activePopUpText
              ? (
                <div
                  className="pop-up-text-container"
                  onMouseEnter={() => setActivePopUpText(index)}
                  onMouseLeave={() => setActivePopUpText(null)}
                >
                  <p>
                    <strong>
                      {item.title}
                    </strong>
                  </p>
                  <p>
                    {parse(item.text)}
                  </p>
                </div>
              )
              : ''}
          </div>
        ))

      ) : ''}
      <div className="animation">
        {slideAnimation || ''}
      </div>
    </div>
  );
};

export default SlideContent;
