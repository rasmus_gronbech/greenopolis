import React, { useState, useEffect } from 'react';
import {
  Carousel, Button,
} from 'react-bootstrap';
import parse from 'html-react-parser';
import SlideContent from '../SlideContent/SlideContent';
import TextBubbleIcon from '../../icons/TextBubbleIcon/TextBubbleIcon';
import ModalIcon from '../../icons/ModalIcon/ModalIcon';

const ChapterCarousel = ({
  chapterTitle, acfChaptersFields, handleCarouselOrSummary, eboti, activeIndex,
  setActiveIndex, slideAnimations, slideNoAnimations, slideBg,
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [openTextBubble, setOpenTextBubble] = useState(true);
  const [isLastSlide, setIsLastSlide] = useState(false);

  useEffect(() => {
    function handleKeyDown(e) {
      if (e.keyCode === 37) {
        // Previous
        if (document.querySelector('.carousel-control-prev')) {
          document.querySelector('.carousel-control-prev').click();
          console.log('pre');
        }
        return false;
      }
      if (e.keyCode === 39) {
        // Next
        if (document.querySelector('.carousel-control-next')) {
          document.querySelector('.carousel-control-next').click();
          console.log('next');
        }
      }
      return false;
    }
    document.addEventListener('keydown', handleKeyDown);

    return () => window.removeEventListener('keydown', handleKeyDown);
  }, []);

  useEffect(() => {
    if (isLastSlide) {
      document.addEventListener('keydown', goToSummary);
    } else {
      return false;
    }
    return () => {
      document.removeEventListener('keydown', goToSummary, true);
    };
  }, [isLastSlide]);

  function goToSummary(e) {
    if (e.keyCode === 39) {
      // Next
      if (document.querySelector('.go-to-summary-btn')) {
        document.querySelector('.go-to-summary-btn').click();
      }
    }
  }

  const handleSelect = (selectedIndex, e) => {
    setActiveIndex(selectedIndex);
    setOpenTextBubble(true);
    setOpenModal(false);
    // check if last slide is active to change the carousel layout
    if (selectedIndex === acfChaptersFields.slidesContent.length - 1) {
      setTimeout(() => {
        setIsLastSlide(true);
        console.log('LAST SLIDE');
      }, 100);
    } else {
      setIsLastSlide(false);
    }
  };

  const handleOpenOrClosedModal = () => {
    if (openModal) {
      setOpenModal(false);
      setOpenTextBubble(false);
    } else {
      setOpenModal(true);
      setOpenTextBubble(false);
    }
  };

  const handleOpenOrClosedTextBubble = () => {
    if (openTextBubble) {
      setOpenTextBubble(false);
    } else {
      setOpenTextBubble(true);
    }
  };

  const handleOnClick = () => {
    setActiveIndex(0);
    handleCarouselOrSummary();
  };

  return (
    <>
      <Carousel
        onSelect={handleSelect}
        interval={null}
        controls
        wrap={false}
      >
        {acfChaptersFields.slidesContent
          ? acfChaptersFields.slidesContent.map((slide, i) => (
            <Carousel.Item key={`item_${i.toString()}`}>
              <div
                className="slide-content"
              >
                {slideBg[i] ? slideBg[i] : ''}
                {slideBg[i]
                  ? (
                    <SlideContent
                      click={handleOpenOrClosedModal}
                      open={openModal}
                      chapterTitle={chapterTitle}
                      slideTitle={slide.title}
                      slideText={slide.text}
                      slideAdditionalFieldsContent={slide.additionalFieldsContent}
                      slideIndex={i}
                  // eslint-disable-next-line no-nested-ternary
                      slideAnimation={slideAnimations && activeIndex === i ? slideAnimations[i] : slideNoAnimations && activeIndex !== i ? slideNoAnimations[i] : ''}
                    />
                  )
                  : null}
              </div>
            </Carousel.Item>
          ))
          : ''}
      </Carousel>
      { (acfChaptersFields.slidesContent[activeIndex])
        ? (
          <div className={`eboti-text-bubble ${!openTextBubble ? 'hide' : ''}`}>
            <div className="d-flex">
              <div className="mr-2">
                { (acfChaptersFields.slidesContent[activeIndex].introductionText)
                  ? parse(acfChaptersFields.slidesContent[activeIndex].introductionText) : ''}
              </div>
              <div>
                <Button
                  className="close-eboti-text-bubble-btn align-self-start"
                  onClick={handleOpenOrClosedTextBubble}
                >
                  <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.69574 5.49929L10.2724 1.92723C10.4291 1.77044 10.5171 1.55779 10.5171 1.33605C10.5171 1.11432 10.4291 0.901665 10.2724 0.744875C10.1158 0.588084 9.90338 0.5 9.68187 0.5C9.46036 0.5 9.24793 0.588084 9.0913 0.744875L5.52291 4.32526L1.95453 0.744875C1.7979 0.588084 1.58547 0.5 1.36396 0.5C1.14245 0.5 0.930016 0.588084 0.773386 0.744875C0.616757 0.901665 0.528763 1.11432 0.528763 1.33605C0.528763 1.55779 0.616757 1.77044 0.773386 1.92723L4.35009 5.49929L0.773386 9.07135C0.695424 9.14875 0.633543 9.24084 0.591314 9.34231C0.549086 9.44377 0.527344 9.5526 0.527344 9.66252C0.527344 9.77244 0.549086 9.88128 0.591314 9.98274C0.633543 10.0842 0.695424 10.1763 0.773386 10.2537C0.850712 10.3317 0.942709 10.3937 1.04407 10.436C1.14543 10.4782 1.25415 10.5 1.36396 10.5C1.47376 10.5 1.58248 10.4782 1.68385 10.436C1.78521 10.3937 1.8772 10.3317 1.95453 10.2537L5.52291 6.67332L9.0913 10.2537C9.16863 10.3317 9.26062 10.3937 9.36198 10.436C9.46335 10.4782 9.57207 10.5 9.68187 10.5C9.79168 10.5 9.9004 10.4782 10.0018 10.436C10.1031 10.3937 10.1951 10.3317 10.2724 10.2537C10.3504 10.1763 10.4123 10.0842 10.4545 9.98274C10.4967 9.88128 10.5185 9.77244 10.5185 9.66252C10.5185 9.5526 10.4967 9.44377 10.4545 9.34231C10.4123 9.24084 10.3504 9.14875 10.2724 9.07135L6.69574 5.49929Z" fill="#fff" />
                  </svg>
                </Button>
              </div>
            </div>
            <Button variant="custom" className="open-modal-btn" onClick={handleOpenOrClosedModal}>
              <ModalIcon />
              {acfChaptersFields.readMoreButtonLabel ? acfChaptersFields.readMoreButtonLabel : 'Read more'}
            </Button>
          </div>
        ) : ''}
      { (!openTextBubble && !openModal)
        ? (
          <Button className="open-text-bubble-btn" onClick={handleOpenOrClosedTextBubble}>
            <TextBubbleIcon />
          </Button>
        ) : ''}
      {eboti || ''}
      { (isLastSlide)
        ? (
          <button type="button" className="go-to-summary-btn" onClick={handleOnClick}>
            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M1.10352 8.25146H15.4713" stroke="#5EB36E" strokeWidth="2.08333" strokeLinecap="round" strokeLinejoin="round" />
              <path d="M9.18555 1.96582L15.4715 8.25174L9.18555 14.5377" stroke="#5EB36E" strokeWidth="2.08333" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
          </button>
        )
        : ''}
    </>
  );
};

export default ChapterCarousel;
