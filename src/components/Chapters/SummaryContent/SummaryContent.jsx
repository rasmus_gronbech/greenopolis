import React, { useState, useEffect } from 'react';
import {
  Button,
} from 'react-bootstrap';
import parse from 'html-react-parser';
import { Link } from 'gatsby';

import './summary-content.scss';

const SummaryContent = ({
  summaryTitle, summaryText, exercisesLinkLabelText, exercisesPageLink,
  handleCarouselOrSummary, lastSlideImage,
}) => {
  useEffect(() => {
    document.addEventListener('keydown', (e) => {
      if (e.keyCode === 37) {
        // Previous
        handleCarouselOrSummary();
      }
      return false;
    });
    return () => window.removeEventListener('keydown', (e) => {
      if (e.keyCode === 37) {
        // Previous
        handleCarouselOrSummary();
      }
      return false;
    });
  }, []);

  const handleOnClick = () => {
    handleCarouselOrSummary();
  };
  return (
    <div className="container-fluid summary-container">
      <div className="container h-100 d-flex align-items-center">
        <div className="row align-items-center">
          <div className="col-12 col-md-6">
            {lastSlideImage}
          </div>
          <div className="col-12 col-md-6 text">
            {summaryTitle ? <h2>{summaryTitle}</h2> : ''}
            {summaryText ? <div>{parse(summaryText)}</div> : ''}
            {
              exercisesPageLink
                ? <Link href={exercisesPageLink} className="btn-primary">{exercisesLinkLabelText}</Link>
                : ''
            }
          </div>
          <button type="button" className="back-to-carousel-btn" onClick={handleOnClick}>
            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M15.4707 8.25146H1.10289" stroke="#5EB36E" strokeWidth="2.08333" strokeLinecap="round" strokeLinejoin="round" />
              <path d="M7.38867 1.96582L1.10275 8.25174L7.38867 14.5377" stroke="#5EB36E" strokeWidth="2.08333" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
};

export default SummaryContent;
