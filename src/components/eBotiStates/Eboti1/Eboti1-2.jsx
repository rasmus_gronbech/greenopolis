import * as React from 'react';
import { motion } from 'framer-motion';

function Eboti1(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200" {...props} className="eboti">
      <defs>
        <clipPath id="prefix__clip-path">
          <path fill="none" d="M20.4 17.07h171.75v159.52H20.4z" />
        </clipPath>
        <style>
          {
            '.prefix__cls-2{clip-path:url(#prefix__clip-path)}.prefix__cls-3{fill:#2bb673}.prefix__cls-4{fill:#199b69}.prefix__cls-7{fill:#006838}'
          }
        </style>
      </defs>
      <g id="prefix__Layer_2" data-name="Layer 2">
        <g className="prefix__cls-2" id="prefix__boti">
          <g className="prefix__cls-2">
            <path
              className="prefix__cls-3 hand"
              d="M169.6 175.23a27.81 27.81 0 00-27.81-27.8 1.37 1.37 0 00-1.36 1.36 27.83 27.83 0 0027.8 27.8 1.37 1.37 0 001.37-1.36"
            />
            <path
              className="prefix__cls-3 body"
              d="M131 134.77l1.68 15.48a22.27 22.27 0 11-44.28 4.8l-1.7-15.47a19.46 19.46 0 0117.24-21.44l5.6-.61A19.46 19.46 0 01131 134.77"
            />
            <path
              className="prefix__cls-4 body"
              d="M131.24 137.1l1.21 11.22a99.28 99.28 0 01-44.3 4.61l-1.2-11a19.45 19.45 0 0117.24-21.44l5.6-.61a19.46 19.46 0 0121.45 17.24"
            />
            <motion.path
              animate={{
                y: [0, -35, 0],
              }}
              transition={{
                duration: 2, repeat: Infinity, repeatDelay: 5,
              }}
              style={{
                originX: 0.5,
                originY: 0.5,

              }}
              className="prefix__cls-3 hand"
              d="M54.62 133.28L22 125.73a1.13 1.13 0 00-1.35.84c-.74 3.23.44 6.67 3.32 9.68a23.07 23.07 0 0011.38 6.11 26.42 26.42 0 005.85.68 20.63 20.63 0 007.06-1.18c3.92-1.44 6.48-4 7.23-7.24a1.12 1.12 0 00-.84-1.34"
            />
            <motion.g
              className="head"
              animate={{
                rotate: [0, 10, 0],
              }}
              transition={{
                duration: 2, repeat: Infinity, repeatDelay: 5,
              }}
              style={{
                originX: 0.5,
                originY: 0.5,

              }}
            >
              <motion.path
                animate={{
                  rotate: [5, -5, 5, -5, 5, -5, 5],
                }}
                transition={{
                  delay: 2, duration: 2, repeat: Infinity, repeatDelay: 5,
                }}
                style={{
                  originX: 0,
                  originY: 1,
                }}
                className="prefix__cls-3"
                d="M117.46 34.84A41.54 41.54 0 0090.8 17.11a2 2 0 00-2.41 1.61 41.89 41.89 0 0033 49.13 1.72 1.72 0 00.39 0 2.05 2.05 0 001.71-.92 2 2 0 00.31-.78 41.54 41.54 0 00-6.33-31.35"
              />
              <motion.path
                animate={{
                  rotate: [-5, 5, -5, 5, -5, 5, -5],
                }}
                transition={{
                  delay: 2, duration: 2, repeat: Infinity, repeatDelay: 5,
                }}
                style={{
                  originX: 0,
                  originY: 1,
                }}
                className="prefix__cls-4"
                d="M154.55 41.49a3.39 3.39 0 00-2.17-1.44 29.85 29.85 0 00-35 23.54 3.39 3.39 0 002.69 4 29.8 29.8 0 0035.05-23.53 3.42 3.42 0 00-.52-2.56"
              />
              <path
                className="prefix__cls-3"
                d="M191.05 112l-6.36-9.48A100.89 100.89 0 0044.7 75l-9.47 6.36a6.54 6.54 0 00-1.78 9.07l6.36 9.47a100.22 100.22 0 0064.29 42.75 101.74 101.74 0 0019.67 1.93 100.13 100.13 0 0056-17.16l9.35-6.28a6.72 6.72 0 002.8-3.85 6.5 6.5 0 00-.89-5.29"
              />
              <path
                d="M176.75 112.19l-.75-1.29a89.21 89.21 0 00-24.85-27.54 80.65 80.65 0 00-66.29-13 89.15 89.15 0 00-33.43 16.05l-1.2.91a2.41 2.41 0 00-.6 3.05l.76 1.3a89.35 89.35 0 0024.86 27.54 80.73 80.73 0 0066.28 13A89.42 89.42 0 00175 116.16l1.19-.92a2.4 2.4 0 00.6-3"
                fill="#217b38"
              />
              <path
                d="M176.48 113.58l-.76-1.3a89.41 89.41 0 00-24.85-27.53 80.67 80.67 0 00-66.29-13A89 89 0 0051.16 87.8l-1.2.91a2.4 2.4 0 00-.6 3.05l.76 1.3A89.3 89.3 0 0075 120.59a80.72 80.72 0 0066.29 13 89.3 89.3 0 0033.42-16.08l1.2-.92a2.31 2.31 0 00.86-1.41 2.38 2.38 0 00-.26-1.64"
                fill="#beeda2"
              />
              <motion.path
                // animate={{
                //   scaleY: [1, 1.4, 1.4, 1.4, 1],
                // }}
                // style={{
                //   originX: 0,
                //   originY: 1,
                // }}
                // transition={{ duration: 1.5, repeat: Infinity, repeatDelay: 5.5 }}
                className="prefix__cls-7"
                d="M119.49 113.33a1.47 1.47 0 00-1.69 1.19s-.8 4.46-3.91 6.6A8.23 8.23 0 01103.3 119c-2.09-3.13-1.24-7.55-1.22-7.63a1.47 1.47 0 00-2.87-.59c0 .23-1.13 5.65 1.64 9.82a10.14 10.14 0 006.8 4.29 13.12 13.12 0 002.44.25 9.38 9.38 0 005.45-1.65c4.12-2.83 5.1-8.28 5.14-8.51a1.46 1.46 0 00-1.19-1.69"
              />
              <motion.path
                // animate={{
                //   x: [0, 5, 5, 5, 5, 0, 0, 0],
                //   y: [0, -13, -15, -13, -15, 0, 0, 0],
                // }}
                // transition={{ duration: 2, repeat: Infinity, repeatDelay: 5 }}
                // style={{
                //   originX: 0,
                //   originY: 1,
                // }}
                className="prefix__cls-7"
                d="M151.88 113.31l-4.86-14a1.51 1.51 0 00-.58-.78 1.49 1.49 0 00-1.74.08l-11.95 9.75a1.46 1.46 0 001.85 2.27l10.33-8.43 4.19 12.11a1.46 1.46 0 001.38 1 1.34 1.34 0 00.48-.08 1.45 1.45 0 00.9-1.86M83.35 82.72a1.48 1.48 0 00-1.45-1 1.52 1.52 0 00-.9.36l-11.91 9.74a1.46 1.46 0 101.85 2.26l10.33-8.42 4.19 12.11a1.46 1.46 0 001.38 1 1.61 1.61 0 00.48-.08 1.46 1.46 0 00.9-1.86z"
              />
            </motion.g>
          </g>
        </g>
      </g>
    </svg>
  );
}

export default Eboti1;
