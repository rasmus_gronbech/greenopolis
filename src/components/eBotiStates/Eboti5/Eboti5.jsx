import * as React from 'react';
import { motion } from 'framer-motion';

function Eboti5(props) {
  return (
    <svg className="eboti" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200" {...props}>
      <defs>
        <style>
          {
            '.prefix__cls-3{fill:#2bb673}.prefix__cls-4{fill:#199b69}.prefix__cls-7{fill:none;stroke:#006838;stroke-linecap:round;stroke-linejoin:round;stroke-width:6.49px}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_2" data-name="Layer 2">
          <g id="prefix__boti">
            <g id="prefix__top">
              <path
                className="prefix__cls-3"
                d="M117.28 34.77a42 42 0 0131.91-5.27 2.07 2.07 0 011.55 2.5A42.31 42.31 0 01100 63.55a1.9 1.9 0 01-.38-.13 2.06 2.06 0 01-1.19-1.55 2 2 0 010-.85 42 42 0 0118.85-26.25z"
              />
              <path
                className="prefix__cls-4"
                d="M80.36 25.52a3.39 3.39 0 012.6-.43 30.15 30.15 0 0122.52 36.21 3.43 3.43 0 01-4.13 2.57 30.12 30.12 0 01-22.53-36.21 3.46 3.46 0 011.54-2.14z"
              />
            </g>
            <path
              className="prefix__cls-3"
              d="M119 133.64l2.06 15.59a22.49 22.49 0 01-19.35 25.25 22.5 22.5 0 01-25.25-19.35l-2.07-15.59a19.66 19.66 0 0116.91-22.06l5.64-.75A19.66 19.66 0 01119 133.64z"
            />
            <path
              className="prefix__cls-4"
              d="M119.31 136l1.49 11.31A100.26 100.26 0 0176.17 153l-1.46-11.11a19.63 19.63 0 0116.9-22l5.64-.75A19.65 19.65 0 01119.31 136z"
            />
            <path
              className="prefix__cls-3"
              d="M179.11 109.24l-6.64-9.42a101.9 101.9 0 00-142-24.48L21 82a6.6 6.6 0 00-1.58 9.19l6.65 9.42A101.21 101.21 0 0092 142.25a102.19 102.19 0 0019.9 1.48 101.14 101.14 0 0056.17-18.65l9.3-6.56a6.73 6.73 0 002.73-4 6.54 6.54 0 00-.99-5.28z"
            />
            <path
              id="prefix__HAND_L"
              data-name="HAND L"
              className="prefix__cls-3"
              d="M47.13 151.32a29.24 29.24 0 00-34.51-22.77 1.44 1.44 0 00-1.11 1.7A29.26 29.26 0 0046 153a1.42 1.42 0 001.13-1.68z"
            />
            <path
              id="prefix__HAND_L-2"
              data-name="HAND L"
              className="prefix__cls-3"
              d="M46.8 152.5a17.9 17.9 0 00-6.8-24.41.88.88 0 00-1.2.33 18 18 0 006.78 24.42.88.88 0 001.22-.34z"
            />
            <path
              id="prefix__HAND_R"
              data-name="HAND R"
              className="prefix__cls-3"
              d="M159 173.59a28.09 28.09 0 00-28.74-27.42 1.37 1.37 0 00-1.34 1.4A28.12 28.12 0 00157.6 175a1.39 1.39 0 001.4-1.41z"
            />
            <path
              d="M164.68 109.76l-.8-1.29a90.07 90.07 0 00-25.74-27.22 81.57 81.57 0 00-67.25-11.6 90.32 90.32 0 00-33.37 17l-1.19.95a2.43 2.43 0 00-.53 3.09l.8 1.3a90.09 90.09 0 0025.75 27.22 81.5 81.5 0 0067.24 11.59 89.87 89.87 0 0033.37-17l1.19-1a2.31 2.31 0 00.84-1.44 2.34 2.34 0 00-.31-1.6z"
              fill="#217b38"
            />
            <path
              d="M164.44 111.17l-.8-1.29a90 90 0 00-25.74-27.22 81.47 81.47 0 00-67.25-11.6 90 90 0 00-33.37 17l-1.19 1a2.43 2.43 0 00-.53 3.09l.8 1.29a90 90 0 0025.74 27.2 81.47 81.47 0 0067.25 11.6 90.17 90.17 0 0033.37-17l1.19-.95a2.43 2.43 0 00.53-3.09z"
              fill="#beeda2"
            />
            <g>
              <motion.path
                animate={{
                  scale: [1, 0, 1],
                }}
                style={{
                  originX: 1,
                  originY: 1,
                }}
                transition={{ duration: 0.5, repeat: Infinity, repeatDelay: 5.5 }}
                className="prefix__cls-7 eye"
                d="M75.34 79.24l-5.99 12.95"
              />
              <motion.path
                animate={{
                  scale: [1, 0, 1],
                }}
                style={{
                  originX: 1,
                  originY: 1,
                }}
                transition={{ duration: 0.5, repeat: Infinity, repeatDelay: 5.5 }}
                className="prefix__cls-7 eye"
                d="M123.66 90l-5.98 12.95"
              />
              <path
                d="M76.67 106.47a92.79 92.79 0 0013.6.64 52.38 52.38 0 0012.66-2.69 1.25 1.25 0 011.32.92c.92 3.22 2.79 13.25-9 14.1-10.85.79-17.25-7.31-19.59-11a1.27 1.27 0 011.01-1.97z"
                fill="#006838"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
}

export default Eboti5;
