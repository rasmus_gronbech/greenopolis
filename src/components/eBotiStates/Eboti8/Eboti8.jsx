import * as React from 'react';
import { motion } from 'framer-motion';

function SvgComponent(props) {
  return (
    <svg
      className="eboti"
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 200 200"
      {...props}
    >
      <defs>
        <filter id="shadow-eboti-8" y="-40%" x="-40%" height="180%" width="180%" colorInterpolationFilters="sRGB">
          <feDropShadow dx="0" dy="0" stdDeviation="6" floodOpacity="0.4" />
        </filter>
        <clipPath id="prefix__clip-path">
          <rect
            className="prefix__cls-1"
            x={49.88}
            y={139.64}
            width={95.38}
            height={23.54}
            rx={11.77}
            transform="rotate(6.79 97.535 151.364)"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <path
            className="prefix__cls-1"
            d="M159.12 100.91l-.82-1.11a80.72 80.72 0 00-25.2-22.5 71.73 71.73 0 00-60.94-4.67A81.12 81.12 0 0043.79 91l-1 1a2.26 2.26 0 00-.23 2.86l.85 1.14a81 81 0 0025.2 22.5 71.68 71.68 0 0060.94 4.66 81 81 0 0028.37-18.39l1-1a2.2 2.2 0 00.62-1.39 2.14 2.14 0 00-.42-1.47z"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-1,.prefix__cls-9{fill:none}.prefix__cls-2{fill:#199b69}.prefix__cls-3{fill:#2bb673}.prefix__cls-6{fill:#f9bd74}.prefix__cls-9{stroke:#006838;stroke-linecap:round;stroke-linejoin:round;stroke-width:5.5px}'
          }
        </style>
      </defs>
      <g
        filter="url(#shadow-eboti-8)"
        id="prefix__BOTI"
      >
        <path
          className="prefix__cls-2"
          d="M122.7 45.39a21.53 21.53 0 01-21.53 21.53 21.53 21.53 0 0121.53-21.53z"
          transform="rotate(9.39 111.933 56.144)"
        />
        <path
          className="prefix__cls-3"
          d="M73.37 29.49a33.76 33.76 0 0133.76 33.76 33.76 33.76 0 01-33.76-33.76z"
          transform="rotate(-170.61 90.247 46.369)"
        />
        <g id="prefix__boti-2" data-name="boti">
          <path
            className="prefix__cls-3"
            d="M117.89 143.32l-2 13.31A19.26 19.26 0 0194 172.82a19.25 19.25 0 01-16.19-21.9l2-13.32a16.82 16.82 0 0119.13-14.14l4.82.72a16.83 16.83 0 0114.13 19.14z"
          />
          <path
            className="prefix__cls-2"
            d="M118.55 134.35L117.1 144a86 86 0 01-38.1-5.88l1.42-9.49a16.83 16.83 0 0119.14-14.14l4.82.72a16.83 16.83 0 0114.17 19.14z"
          />
        </g>
        <motion.g
          animate={{
            y: [0, 20, 0],
          }}
          transition={{
            duration: 4, times: [0, 0.2, 1], repeat: Infinity, repeatDelay: 4,
          }}
          clipPath="url(#prefix__clip-path)"
          className="swimming-ring"
        >
          <rect
            x={49.88}
            y={139.64}
            width={95.38}
            height={23.54}
            rx={11.77}
            transform="rotate(6.79 97.535 151.364)"
            fill="#f8a547"
          />
          <ellipse
            className="prefix__cls-6"
            cx={134.58}
            cy={148.36}
            rx={8.46}
            ry={9.43}
            transform="rotate(-83.21 134.594 148.361)"
          />
          <ellipse
            className="prefix__cls-6"
            cx={111.08}
            cy={163.35}
            rx={8.46}
            ry={9.43}
            transform="rotate(-83.21 111.085 163.358)"
          />
          <ellipse
            className="prefix__cls-6"
            cx={96.1}
            cy={142.17}
            rx={8.46}
            ry={9.43}
            transform="rotate(-83.21 96.104 142.169)"
          />
          <ellipse
            className="prefix__cls-6"
            cx={62.22}
            cy={135.13}
            rx={8.46}
            ry={9.43}
            transform="rotate(-83.21 62.227 135.137)"
          />
          <ellipse
            className="prefix__cls-6"
            cx={69.38}
            cy={158.02}
            rx={8.46}
            ry={9.43}
            transform="rotate(-83.21 69.393 158.017)"
          />
        </motion.g>
        <path
          className="prefix__cls-3"
          d="M168.78 98.88l-6.49-7.43A87.25 87.25 0 0039.18 83.2l-7.43 6.5a5.65 5.65 0 00-.54 8l6.5 7.43a86.69 86.69 0 0059.85 29.62 87.58 87.58 0 0017.08-.51 86.61 86.61 0 0046.18-20.86l7.33-6.41a5.83 5.83 0 002-3.61 5.63 5.63 0 00-1.37-4.48z"
        />
        <g clipPath="url(#prefix__clip-path-2)">
          <path
            className="prefix__cls-2"
            d="M159.12 100.91l-.82-1.11a80.72 80.72 0 00-25.2-22.5 71.73 71.73 0 00-60.94-4.67A81.12 81.12 0 0043.79 91l-1 1a2.26 2.26 0 00-.23 2.86l.85 1.14a81 81 0 0025.2 22.5 71.68 71.68 0 0060.94 4.66 81 81 0 0028.37-18.39l1-1a2.2 2.2 0 00.62-1.39 2.14 2.14 0 00-.42-1.47z"
          />
        </g>
        <path
          d="M157.78 102.29l-.81-1.06a79 79 0 00-24.7-21.73 71.22 71.22 0 00-59.66-4.6 78.83 78.83 0 00-27.74 17.68l-1 .93a2.15 2.15 0 00-.22 2.76l.81 1.07a79.11 79.11 0 0024.71 21.72 71.21 71.21 0 0059.66 4.6A79 79 0 00156.61 106l1-.93a2.09 2.09 0 00.61-1.34 2.12 2.12 0 00-.44-1.44z"
          fill="#beeda2"
        />
        <motion.path
          animate={{
            scale: [1, 0, 1],
          }}
          transition={{
            duration: 0.4, repeat: Infinity, repeatDelay: 5,
          }}
          className="prefix__cls-9 eye"
          d="M74.24 89.18l-.46 12.21"
        />
        <motion.path
          animate={{
            scale: [1, 0, 1],
          }}
          transition={{
            duration: 0.4, repeat: Infinity, repeatDelay: 5,
          }}
          className="prefix__cls-9 eye"
          d="M119.73 88.13l-.46 12.21"
        />
        <ellipse
          cx={95.1}
          cy={111.92}
          rx={5.06}
          ry={7.36}
          transform="rotate(-69.11 95.108 111.922)"
          fill="#006838"
        />
        <motion.g
          animate={{
            y: [0, 30, 0],
            rotate: [0, 120, 0],
          }}
          transition={{
            duration: 4, repeat: Infinity, repeatDelay: 4,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'center',
            originY: 'center',
          }}
        >
          <path
            className="prefix__cls-3 hand-r"
            d="M64 117.8a24.12 24.12 0 0124.12 24.12A24.12 24.12 0 0164 117.8z"
            transform="matrix(-.3 -.96 .96 -.3 -25.45 240.97)"
          />
        </motion.g>
        <motion.g
          animate={{
            y: [0, 30, 0],
          }}
          transition={{
            duration: 4, repeat: Infinity, repeatDelay: 4,
          }}
        >
          <path
            className="prefix__cls-3 hand-l"
            d="M113.77 130.93A24.12 24.12 0 01137.88 155a24.12 24.12 0 01-24.12-24.12z"
            transform="rotate(-83.9 125.824 142.983)"
          />
        </motion.g>
      </g>
    </svg>
  );
}

export default SvgComponent;
