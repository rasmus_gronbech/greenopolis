import * as React from 'react';
import { motion } from 'framer-motion';

function Eboti2(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200" {...props} className="eboti">
      <defs>
        <style>
          {
            '.prefix__cls-1{fill:#2bb673}.prefix__cls-2{fill:#199b69}.prefix__cls-5{fill:none;stroke:#006838;stroke-linecap:round;stroke-linejoin:round;stroke-width:5.39px}'
          }
        </style>
      </defs>
      <g id="prefix__Layer_2" data-name="Layer 2">
        <g id="prefix__boti">
          <path
            className="prefix__cls-1"
            d="M113.45 142.62l-3 14.7A21.45 21.45 0 0185.12 174a21.46 21.46 0 01-16.72-25.29l3-14.7a18.75 18.75 0 0122.12-14.6l5.32 1.09a18.75 18.75 0 0114.61 22.12z"
          />
          <path
            className="prefix__cls-2"
            d="M114.54 137l-2.18 10.66a95.69 95.69 0 01-42-8.8l2.15-10.47a18.74 18.74 0 0122.12-14.6l5.31 1.09a18.74 18.74 0 0114.6 22.12z"
          />
        </g>
        <path
          id="prefix__HAND_R"
          data-name="HAND R"
          className="prefix__cls-1"
          d="M142.91 174a26.78 26.78 0 00-18.59-33 1.3 1.3 0 00-1.61.91 26.8 26.8 0 0018.58 33 1.31 1.31 0 001.62-.91z"
        />
        <g id="prefix__top">
          <path
            className="prefix__cls-2"
            d="M74.41 25.59a3.28 3.28 0 012.45-.54 28.78 28.78 0 0123.21 33.41 3.27 3.27 0 01-3.82 2.64 28.05 28.05 0 01-5.37-1.51A28.8 28.8 0 0173.05 27.7a3.24 3.24 0 011.36-2.11z"
          />
          <path
            className="prefix__cls-1"
            d="M110 32.62a40 40 0 0130.15-6.57 2 2 0 011.59 2.29 40.36 40.36 0 01-46.8 32.53 2.26 2.26 0 01-.37-.11 2 2 0 01-1.21-1.42 2 2 0 010-.81A40 40 0 01110 32.62z"
          />
        </g>
        <path
          className="prefix__cls-1"
          d="M172.52 100.57l-6.79-8.65A97.2 97.2 0 0029.28 75.45l-8.65 6.79a6.29 6.29 0 00-1.06 8.83l6.78 8.65a96.56 96.56 0 0064.82 36.5 97.86 97.86 0 0019 .44 96.5 96.5 0 0052.6-20.47l8.54-6.7a6.52 6.52 0 002.42-3.9 6.3 6.3 0 00-1.21-5.02z"
        />
        <path
          d="M158.8 101.76l-.83-1.2a86 86 0 00-25.83-24.68 77.73 77.73 0 00-64.62-7.8 85.91 85.91 0 00-31 17.83l-1.09 1a2.33 2.33 0 00-.36 3l.87 1.09a86 86 0 0025.83 24.69 77.77 77.77 0 0064.62 7.79 85.93 85.93 0 0031-17.83l1.09-1a2.25 2.25 0 00.73-1.42 2.3 2.3 0 00-.41-1.47z"
          fill="#217b38"
        />
        <path
          d="M157.58 102.79l-.83-1.2a85.9 85.9 0 00-25.84-24.68 77.7 77.7 0 00-64.61-7.8 85.89 85.89 0 00-31 17.83l-1.08 1a2.32 2.32 0 00-.36 3l.82 1.19a85.9 85.9 0 0025.84 24.68 77.7 77.7 0 0064.61 7.8 85.89 85.89 0 0031-17.83l1.09-1a2.33 2.33 0 00.36-3z"
          fill="#beeda2"
        />
        <motion.path
          animate={{
            scale: [1, 0, 1],
          }}
          style={{
            originX: 1,
            originY: 1,
          }}
          transition={{ duration: 0.5, repeat: Infinity, repeatDelay: 5.5 }}
          className="prefix__cls-5 eye"
          d="M70.55 80.78l-3.18 13.23"
        />
        <motion.path
          animate={{
            scale: [1, 0, 1],
          }}
          style={{
            originX: 1,
            originY: 1,
          }}
          transition={{ duration: 0.5, repeat: Infinity, repeatDelay: 5.5 }}
          className="prefix__cls-5 eye"
          d="M120.46 89.62l-3.18 13.24"
        />
        <motion.ellipse
          animate={{
            scale: [1, 1.5, 1.5, 1.5, 1.5, 1],
          }}
          style={{
            originX: 0.5,
            originY: 0.5,
          }}
          transition={{ duration: 2, repeat: Infinity, repeatDelay: 5 }}
          cx={88.34}
          cy={110.19}
          rx={5.64}
          ry={8.2}
          transform="rotate(-57.74 88.33 110.192)"
          fill="#006838"
        />
        <motion.path
          animate={{
            y: [0, -15, -15, -15, -15, -15, 0],
            x: [0, 15, 15, 15, 15, 0],
          }}
          transition={{
            duration: 2, repeat: Infinity, repeatDelay: 5,
          }}
          style={{
            originX: 0.5,
            originY: 0.5,

          }}
          id="prefix__HAND_L"
          data-name="HAND L"
          className="prefix__cls-1"
          d="M52.69 146.57a27.88 27.88 0 0025.61-30 1.37 1.37 0 00-1.46-1.26 27.92 27.92 0 00-25.62 30 1.37 1.37 0 001.47 1.26z"
        />
      </g>
    </svg>
  );
}

export default Eboti2;
