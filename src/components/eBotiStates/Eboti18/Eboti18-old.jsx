import * as React from 'react';
import { motion } from 'framer-motion';

function SvgComponent(props) {
  return (
    <svg
      className="eboti"
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 200 600"
      {...props}
      style={{
        height: 'auto',
        width: 200,
      }}
    >
      <defs>
        <filter id="shadow-eboti-18" y="-40%" x="-40%" height="180%" width="180%" colorInterpolationFilters="sRGB">
          <feDropShadow dx="0" dy="0" stdDeviation="6" floodOpacity="0.4" />
        </filter>
        <clipPath id="prefix__clip-path">
          <path
            className="prefix__cls-1"
            d="M117.91 533.76v14.08C118 559 110 568 100.21 568c-9.8 0-17.76-9-17.78-20.13v-14.08c0-9.72 6.92-17.61 15.48-17.62h4.49c8.6 0 15.5 7.83 15.51 17.59z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <path
            d="M158.86 497.48l-.89-1a80.45 80.45 0 00-26.52-20.63 71.29 71.29 0 00-60.79-.53 80.54 80.54 0 00-26.91 20.16l-.91 1a2.25 2.25 0 000 2.85l.89 1A80.49 80.49 0 0070.22 521a71.38 71.38 0 0060.79.53 80.57 80.57 0 0026.91-20.17l.91-1a2.2 2.2 0 00.53-1.42 2.24 2.24 0 00-.5-1.46z"
            fill="none"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-1{fill:#2bb673}.prefix__cls-4{fill:#199b69}.prefix__cls-7{fill:#006838}'
          }
        </style>
      </defs>
      <motion.g
        animate={{
          y: [0, 30, -150, 0],
          rotate: [0, 0, 360],
        }}
        transition={{
          duration: 2, repeat: Infinity, repeatDelay: 3,
        }}
        style={{
          transformBox: 'fill-box',
          originX: 'center',
          originY: 'center',
        }}
        filter="url(#shadow-eboti-18)"
      >
        <motion.g
          className="body"
          animate={{
            scaleY: [1, 0.8, 1],
          }}
          transition={{
            duration: 2, repeat: Infinity, repeatDelay: 3,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'center',
            originY: 'top',
          }}
        >
          <path
            className="prefix__cls-1"
            d="M117.91 533.76v14.08C118 559 110 568 100.21 568c-9.8 0-17.76-9-17.78-20.13v-14.08c0-9.72 6.92-17.61 15.48-17.62h4.49c8.6 0 15.5 7.83 15.51 17.59z"
          />
          <g clipPath="url(#prefix__clip-path)">
            <path
              className="prefix__cls-4"
              d="M117.61 528.74l1.3 10.94a75.49 75.49 0 01-38 4.34l-1.28-10.75c-1.24-10.42 5.19-19.74 14.37-20.84l4.8-.57c9.13-1.09 17.57 6.47 18.81 16.88z"
            />
          </g>
        </motion.g>
        <g id="prefix__head">
          <motion.path
            animate={{
              rotate: [10, -30, 10, -30, 10, -30, 0],
            }}
            transition={{
              delay: 3, duration: 1, repeat: Infinity, repeatDelay: 4,
            }}
            style={{
              transformBox: 'fill-box',
              originX: 'center',
              originY: 'bottom',
            }}
            className="prefix__cls-4 hair"
            d="M120.41 446.64A21.41 21.41 0 0199 468a21.41 21.41 0 0121.41-21.36z"
            transform="rotate(9.39 109.697 457.3)"
          />
          <motion.path
            animate={{
              rotate: [-10, 30, -10, 30, -10, 30, 0],
            }}
            transition={{
              delay: 3, duration: 1, repeat: Infinity, repeatDelay: 4,
            }}
            style={{
              transformBox: 'fill-box',
              originX: 'center',
              originY: 'bottom',
            }}
            className="prefix__cls-1 hair"
            d="M71.34 430.82a33.57 33.57 0 0133.57 33.57 33.57 33.57 0 01-33.57-33.57z"
            transform="rotate(-170.61 88.13 447.61)"
          />
          <path
            id="prefix__hrad"
            className="prefix__cls-1"
            d="M170 495.22l-7-7.21a88.64 88.64 0 00-125.67-1.16l-7.18 7.07a5.81 5.81 0 00-.07 8.17l7 7.2A88.12 88.12 0 0099.66 536a89.84 89.84 0 0017.34-1.52 88.09 88.09 0 0045.73-24l7.18-7.07a5.79 5.79 0 00.09-8.19z"
          />
          <g clipPath="url(#prefix__clip-path-2)">
            <path
              className="prefix__cls-4"
              d="M158.86 497.48l-.89-1a80.45 80.45 0 00-26.52-20.63 71.29 71.29 0 00-60.79-.53 80.54 80.54 0 00-26.91 20.16l-.91 1a2.25 2.25 0 000 2.85l.89 1A80.49 80.49 0 0070.22 521a71.38 71.38 0 0060.79.53 80.57 80.57 0 0026.91-20.17l.91-1a2.2 2.2 0 00.53-1.42 2.24 2.24 0 00-.5-1.46z"
            />
          </g>
          <path
            d="M157.62 499l-.87-1a78.59 78.59 0 00-26-19.89 70.86 70.86 0 00-59.51-.56 78.83 78.83 0 00-26.34 19.42l-.89 1a2.14 2.14 0 000 2.75l.87 1a78.59 78.59 0 0026 19.89 70.86 70.86 0 0059.51.56 78.83 78.83 0 0026.34-19.42l.89-1a2.11 2.11 0 00.51-1.37 2 2 0 00-.51-1.38z"
            fill="#beeda2"
          />
          <path
            className="prefix__cls-7"
            d="M109 507.78a1.3 1.3 0 00-1.26 1.32s.08 4-2.24 6.38a6.35 6.35 0 01-4.74 1.8 6.43 6.43 0 01-4.76-1.75c-2.35-2.35-2.38-6.32-2.38-6.39a1.29 1.29 0 00-2.58 0c0 .2 0 5.08 3.12 8.21a8.91 8.91 0 006.62 2.52 10.7 10.7 0 002.16-.21 8.28 8.28 0 004.43-2.37c3.06-3.16 3-8 3-8.25a1.29 1.29 0 00-1.37-1.26z"
          />
          <g>
            <path
              className="prefix__cls-7"
              d="M138.4 500.38l-6.65-11.3a1.34 1.34 0 00-.63-.57 1.31 1.31 0 00-1.5.38l-8.62 10.5a1.29 1.29 0 002 1.64l7.46-9.08 5.73 9.74a1.28 1.28 0 001.37.61 1.49 1.49 0 00.4-.15 1.3 1.3 0 00.44-1.77zM73.82 489.05a1.3 1.3 0 00-1.42-.6 1.4 1.4 0 00-.72.47l-8.61 10.47a1.3 1.3 0 00.18 1.82 1.29 1.29 0 001.81-.18l7.47-9.03 5.73 9.74a1.27 1.27 0 001.36.61 1.38 1.38 0 00.4-.15 1.29 1.29 0 00.46-1.77z"
            />
          </g>
        </g>
        <motion.g
          filter="url(#shadow-eboti-18)"
          id="prefix__hand"
          animate={{
            rotate: [0, 0, -40, 0],
            y: [0, 0, -80, 0],
            x: [0, -20, 0, 0],
          }}
          transition={{
            duration: 2, repeat: Infinity, repeatDelay: 3,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'center',
            originY: 'center',
          }}
        >
          <path
            className="prefix__cls-1"
            d="M166.42 530.68a24.12 24.12 0 01-24.12 24.12 24.12 24.12 0 0124.12-24.12z"
            transform="rotate(48.08 154.353 542.804)"
          />
          <path
            className="prefix__cls-1"
            d="M155.62 526.51a16.93 16.93 0 01-16.93 16.93 16.93 16.93 0 0116.93-16.93z"
            transform="rotate(10.12 147.207 535.14)"
          />
        </motion.g>
        <motion.g
          filter="url(#shadow-eboti-18)"
          id="prefix__hand-2"
          data-name="hand"
          animate={{
            rotate: [0, 0, 40, 0],
            y: [0, 0, -80, 0],
            x: [0, 20, 0, 0],
          }}
          transition={{
            duration: 2, repeat: Infinity, repeatDelay: 3,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'center',
            originY: 'center',
          }}
        >
          <path
            className="prefix__cls-1"
            d="M32.35 530.2a24.12 24.12 0 0124.12 24.12 24.12 24.12 0 01-24.12-24.12z"
            transform="rotate(156.17 44.394 542.262)"
          />
          <path
            className="prefix__cls-1"
            d="M45.71 529.68a16.93 16.93 0 0116.93 16.93 16.93 16.93 0 01-16.93-16.93z"
            transform="rotate(-165.86 54.193 538.143)"
          />
        </motion.g>
      </motion.g>
    </svg>
  );
}

export default SvgComponent;
