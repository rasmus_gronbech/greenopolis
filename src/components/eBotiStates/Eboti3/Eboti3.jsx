import * as React from 'react';
import { motion } from 'framer-motion';

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 300.05 200"
      {...props}
      className="eboti"
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path fill="none" d="M290.6 13.04h221.88v176.55H290.6z" />
        </clipPath>
        <style>
          {
            '.prefix__cls-8{fill:none}.prefix__cls-4{fill:#2bb673}.prefix__cls-5{fill:#199b69}.prefix__cls-8{stroke:#006838;stroke-linecap:round;stroke-linejoin:round;stroke-width:5.65px}.prefix__cls-10{fill:#3994bc}.prefix__cls-13{clip-path:url(#prefix__clip-path)}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_2" data-name="Layer 2">
          <g id="prefix__boti">

            <path
              className="prefix__cls-4 body"
              d="M220.09 128.21l2.07 15.58a22.51 22.51 0 01-19.35 25.26 22.49 22.49 0 01-25.25-19.35l-2.07-15.59a19.66 19.66 0 0116.9-22.06l5.65-.75a19.65 19.65 0 0122.05 16.91z"
            />
            <path
              className="prefix__cls-5 body"
              d="M220.41 130.55l1.49 11.31a100.4 100.4 0 01-44.63 5.7l-1.47-11.11a19.66 19.66 0 0116.91-22.06l5.64-.74a19.65 19.65 0 0122.06 16.9z"
            />
            <path
              id="prefix__HAND_R"
              data-name="HAND R"
              className="prefix__cls-4"
              d="M260 168.15a28.06 28.06 0 00-28.73-27.41 1.37 1.37 0 00-1.34 1.4 28.12 28.12 0 0028.73 27.42 1.37 1.37 0 001.34-1.41z"
            />
            <motion.g
              className="head"
              animate={{
                rotate: [0, 15, 15, 15, 15, 15, 15, 0],
              }}
              transition={{
                duration: 4, repeat: Infinity, repeatDelay: 5,
              }}
              style={{
                originX: 0.8,
                originY: 0.2,

              }}
            >
              <g id="prefix__top">
                <path
                  className="prefix__cls-4"
                  d="M218.37 29.34a42 42 0 0131.92-5.27 2.08 2.08 0 011.55 2.48 42.31 42.31 0 01-50.76 31.57 1.71 1.71 0 01-.38-.13 2.06 2.06 0 01-1.19-1.55 2 2 0 010-.85 42 42 0 0118.86-26.25z"
                />
                <path
                  className="prefix__cls-5"
                  d="M181.46 20.09a3.39 3.39 0 012.6-.43 30.15 30.15 0 0122.52 36.21 3.43 3.43 0 01-4.13 2.57 29.68 29.68 0 01-5.55-1.87 30.21 30.21 0 01-17-34.34 3.46 3.46 0 011.56-2.14z"
                />
              </g>
              <path
                className="prefix__cls-4"
                d="M280.21 103.81l-6.65-9.42a101.89 101.89 0 00-142-24.48l-9.42 6.65a6.6 6.6 0 00-1.58 9.19l6.64 9.42a101.24 101.24 0 0065.93 41.65A103 103 0 00213 138.3a101.14 101.14 0 0056.17-18.65l9.29-6.56a6.74 6.74 0 002.74-4 6.54 6.54 0 00-.99-5.28z"
              />
              <path
                d="M265.78 104.33L265 103a90 90 0 00-25.74-27.22A81.47 81.47 0 00172 64.22a90.25 90.25 0 00-33.37 17l-1.19 1a2.43 2.43 0 00-.53 3.09l.8 1.3a90.18 90.18 0 0025.74 27.22 81.52 81.52 0 0067.25 11.59 90.1 90.1 0 0033.37-17l1.19-1a2.37 2.37 0 00.83-1.45 2.32 2.32 0 00-.31-1.64z"
                fill="#217b38"
              />
              <path
                d="M265.54 105.74l-.8-1.3A90.21 90.21 0 00239 77.22a81.5 81.5 0 00-67.24-11.59 90 90 0 00-33.37 17l-1.19 1a2.44 2.44 0 00-.54 3.09l.81 1.29a90 90 0 0025.74 27.22 81.46 81.46 0 0067.24 11.6 90.29 90.29 0 0033.38-17l1.19-1a2.4 2.4 0 00.83-1.45 2.37 2.37 0 00-.31-1.64z"
                fill="#beeda2"
              />
              <path
                className="prefix__cls-8"
                d="M169.56 82.15l-1.97 14.13"
              />
              <motion.path
                animate={{
                  scale: [1, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1],
                }}
                style={{
                  originX: 1,
                  originY: 1,
                }}
                transition={{ duration: 4, repeat: Infinity, repeatDelay: 5.5 }}
                className="prefix__cls-8"
                d="M213.86 84.75l-1.97 14.13"
              />

              <path
                d="M203.56 116.16a56.91 56.91 0 00-9.61-1.48 31.19 31.19 0 00-9.09 1.35 1 1 0 01-.9-.86c-.51-2.75-1.42-11.25 6.95-11.13 7.72.1 11.91 7.3 13.42 10.56a1.05 1.05 0 01-.77 1.56z"
                fill="#006838"
              />
              <motion.g
                id="prefix__LOOKING_GLASS_GRP"
                data-name="LOOKING GLASS GRP"
                animate={{
                  x: [0, 20, 20, 20, 20, 20, 20, 0],
                }}
                transition={{
                  duration: 4, repeat: Infinity, repeatDelay: 5,
                }}
                style={{
                  originX: 0.5,
                  originY: 0.5,

                }}
              >
                <path
                  id="prefix__HAND_L"
                  data-name="HAND L"
                  className="prefix__cls-4"
                  d="M112.69 111.63A29.23 29.23 0 0090.27 76.9a1.43 1.43 0 00-1.7 1.1A29.25 29.25 0 00111 112.72a1.42 1.42 0 001.69-1.09z"
                />
                <g id="prefix__kikkert">
                  <path
                    className="prefix__cls-10"
                    d="M104.56 79L34.8 77.25v28.7l69.76-1.76V79z"
                  />
                  <path
                    className="prefix__cls-10"
                    d="M140.72 83.6l-62.91-1.08v18.15l62.91-1.08V83.6zM49.18 91.6A14.55 14.55 0 0034.8 77.28a14.34 14.34 0 100 28.63A14.55 14.55 0 0049.18 91.6z"
                  />
                  <path
                    className="prefix__cls-10"
                    d="M115.67 91.6c0-6.79-4.91-12.41-11.11-12.57-6.36-.16-11.63 5.47-11.63 12.57s5.27 12.72 11.63 12.56c6.2-.16 11.11-5.78 11.11-12.56zM147.34 91.6c0-4.36-2.94-7.94-6.62-8s-6.8 3.52-6.8 8 3.07 8.06 6.8 8 6.62-3.6 6.62-8z"
                  />
                </g>
                <path
                  d="M47.14 91.6a12.32 12.32 0 00-12.22-12.13 12.14 12.14 0 100 24.25A12.32 12.32 0 0047.14 91.6z"
                  fill="#a5dff1"
                />
                <path
                  d="M34.92 82.08c4.56.07 9.18 3.21 9.18 9.08a1.2 1.2 0 01-2.4 0c0-4.55-3.42-6.63-6.82-6.68a1.2 1.2 0 010-2.4z"
                  fill="#fafdfa"
                />
                <path
                  id="prefix__HAND_L-2"
                  data-name="HAND L"
                  className="prefix__cls-4"
                  d="M111.91 112.58a17.92 17.92 0 003.63-25.08.88.88 0 00-1.23-.18 18 18 0 00-3.63 25.09.89.89 0 001.23.17z"
                />
              </motion.g>
            </motion.g>
          </g>
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
