import * as React from 'react';
import { motion } from 'framer-motion';

function SvgComponent(props) {
  return (
    <svg
      className="eboti"
      id="prefix__Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 200 200"
      {...props}
    >
      <defs>
        <filter id="shadow-eboti-11" y="-40%" x="-40%" height="180%" width="180%" colorInterpolationFilters="sRGB">
          <feDropShadow dx="0" dy="0" stdDeviation="6" floodOpacity="0.4" />
        </filter>
        <clipPath id="prefix__clip-path">
          <path
            className="prefix__cls-1"
            d="M121.92 134.63l1.55 14.08c1.23 11.12-5.72 21-15.51 22.1-9.8 1.08-18.74-7.06-20-18.19l-1.56-14.07c-1.07-9.72 5-18.36 13.56-19.3l4.49-.5c8.55-.94 16.4 6.17 17.47 15.88z"
          />
        </clipPath>
        <clipPath id="prefix__clip-path-2">
          <rect
            className="prefix__cls-2"
            x={60.29}
            y={143.51}
            width={85.31}
            height={24.56}
            rx={12.28}
          />
        </clipPath>
        <clipPath id="prefix__clip-path-3">
          <path
            className="prefix__cls-2"
            d="M157.77 106.09L157 105a80.82 80.82 0 00-24.15-23.63 71.78 71.78 0 00-60.67-7.44A81 81 0 0043 91l-1 .92a2.25 2.25 0 00-.35 2.85l.77 1.14a80.82 80.82 0 0024.15 23.63 71.7 71.7 0 0060.66 7.43 80.9 80.9 0 0029.18-17.08l1-.92a2.19 2.19 0 00.68-1.37 2.16 2.16 0 00-.32-1.51z"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-1{fill:#2bb673}.prefix__cls-11,.prefix__cls-14,.prefix__cls-2{fill:none}.prefix__cls-4{fill:#199b69}.prefix__cls-7{fill:#f8a444}.prefix__cls-11{stroke:#006838;stroke-width:4.97px}.prefix__cls-11,.prefix__cls-14{stroke-linecap:round;stroke-linejoin:round}.prefix__cls-13{fill:#433845}.prefix__cls-14{stroke:#fff;stroke-width:3.32px;opacity:.5}'
          }
        </style>
      </defs>
      <g
        filter="url(#shadow-eboti-11)"
        id="prefix__BOTI"
      >
        <g id="prefix__boti-2" data-name="boti">
          <path
            className="prefix__cls-1"
            d="M121.92 134.63l1.55 14.08c1.23 11.12-5.72 21-15.51 22.1-9.8 1.08-18.74-7.06-20-18.19l-1.56-14.07c-1.07-9.72 5-18.36 13.56-19.3l4.49-.5c8.55-.94 16.4 6.17 17.47 15.88z"
          />
          <g clipPath="url(#prefix__clip-path)">
            <path
              className="prefix__cls-4"
              d="M121.07 129.65l2.49 10.8A75.94 75.94 0 0186 148.92l-2.45-10.61c-2.37-10.27 3-20.29 12.09-22.38l4.75-1.1c9.05-2.09 18.31 4.55 20.68 14.82z"
            />
          </g>
          <g>
            <path
              className="prefix__cls-1"
              d="M38.81 130.84A24.12 24.12 0 0162.93 155a24.12 24.12 0 01-24.12-24.12v-.04z"
              transform="rotate(156.17 50.865 142.902)"
            />
            <path
              className="prefix__cls-1"
              d="M52.16 130.32a16.93 16.93 0 0116.93 16.93 16.93 16.93 0 01-16.93-16.93z"
              transform="rotate(-165.86 60.635 138.781)"
            />
          </g>
        </g>
        <g clipPath="url(#prefix__clip-path-2)">
          <rect
            x={60.29}
            y={143.51}
            width={85.31}
            height={24.56}
            rx={12.28}
            fill="#e06371"
          />
          <ellipse
            className="prefix__cls-7"
            cx={135.5}
            cy={148.07}
            rx={8.43}
            ry={8.83}
          />
          <ellipse
            className="prefix__cls-7"
            cx={116.21}
            cy={166.5}
            rx={8.43}
            ry={8.83}
          />
          <ellipse
            className="prefix__cls-7"
            cx={100.66}
            cy={146.39}
            rx={8.43}
            ry={8.83}
          />
          <ellipse
            className="prefix__cls-7"
            cx={69.84}
            cy={143.28}
            rx={8.43}
            ry={8.83}
          />
          <ellipse
            className="prefix__cls-7"
            cx={78.61}
            cy={166.11}
            rx={8.43}
            ry={8.83}
          />
        </g>
        <motion.g
          animate={{
            rotate: [-10, 10, -10, 10, 0],
          }}
          transition={{
            delay: 7, duration: 1, repeat: Infinity, repeatDelay: 9,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'center',
            originY: 'bottom',
          }}
        >
          <path
            className="prefix__cls-4"
            d="M124 47.52a21.53 21.53 0 01-21.53 21.53A21.53 21.53 0 01124 47.52z"
            transform="rotate(9.39 113.214 58.307)"
          />
        </motion.g>
        <motion.g
          animate={{
            rotate: [10, -10, 10, -10, 0],
          }}
          transition={{
            delay: 7, duration: 1, repeat: Infinity, repeatDelay: 9,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'center',
            originY: 'bottom',
          }}
        >
          <path
            className="prefix__cls-1"
            d="M74.67 31.63a33.76 33.76 0 0133.76 33.76 33.76 33.76 0 01-33.76-33.76z"
            transform="matrix(-.99 -.16 .16 -.99 173.96 111.29)"
          />
        </motion.g>
        <path
          className="prefix__cls-1"
          d="M170.53 105.27l-6.3-7.94A89.6 89.6 0 0038.34 82.86l-7.94 6.3a5.81 5.81 0 00-.93 8.15l6.3 7.94a89 89 0 0059.94 33.31 90.49 90.49 0 0017.55.32 89 89 0 0048.39-19.15l7.84-6.23a5.94 5.94 0 002.21-3.6 5.79 5.79 0 00-1.17-4.63z"
        />
        <g clipPath="url(#prefix__clip-path-3)">
          <path
            className="prefix__cls-4"
            d="M157.77 106.09L157 105a80.82 80.82 0 00-24.15-23.63 71.78 71.78 0 00-60.67-7.44A81 81 0 0043 91l-1 .92a2.25 2.25 0 00-.35 2.85l.77 1.14a80.82 80.82 0 0024.15 23.63 71.7 71.7 0 0060.66 7.43 80.9 80.9 0 0029.18-17.08l1-.92a2.19 2.19 0 00.68-1.37 2.16 2.16 0 00-.32-1.51z"
          />
        </g>
        <path
          d="M156.37 107.41l-.76-1.1a79.26 79.26 0 00-23.69-22.83 71.27 71.27 0 00-59.39-7.31A78.9 78.9 0 0044 92.57l-1 .88a2.16 2.16 0 00-.34 2.75l.76 1.1a79.14 79.14 0 0023.69 22.83 71.27 71.27 0 0059.39 7.31A79 79 0 00155 111l1-.88a2.13 2.13 0 00.67-1.31 2.17 2.17 0 00-.3-1.4z"
          fill="#beeda2"
        />
        <path
          d="M84.05 110.6a80.79 80.79 0 0012-.12 45.8 45.8 0 0011-3 1.1 1.1 0 011.2.74c1 2.78 3.11 11.49-7.17 12.82-9.48 1.23-15.5-5.56-17.74-8.7a1.11 1.11 0 01.71-1.74z"
          fill="#006838"
        />
        <g id="prefix__eyes">
          <path
            className="prefix__cls-11"
            d="M76.66 86.43l.2 12.54"
          />
          <motion.path
            animate={{
              scaleY: [1, 0.2, 0.2, 1],
            }}
            transition={{
              delay: 3,
              duration: 0.4,
              times: [0, 0.2, 0.8, 1],
              repeat: Infinity,
              repeatDelay: 9.6,
            }}
            className="prefix__cls-11"
            d="M119.91 86.03l.19 12.55"
          />
        </g>
        <motion.g
          animate={{
            y: [0, -25, -25, 0],
          }}
          transition={{
            delay: 1, duration: 5, times: [0, 0.3, 0.7, 1], repeat: Infinity, repeatDelay: 5,
          }}
          id="prefix__glasses"
        >
          <path fill="#796598" d="M93.19 81.52h10.38v4.35H93.19z" />
          <path
            className="prefix__cls-13"
            d="M95 80.18H65.07a1.33 1.33 0 00-1.32 1.33v6.83a16.26 16.26 0 1032.51 0v-6.83A1.33 1.33 0 0095 80.18zM132.29 80.18h-29.88a1.33 1.33 0 00-1.31 1.33v6.83a16.26 16.26 0 1032.51 0v-6.83a1.33 1.33 0 00-1.32-1.33z"
          />
          <path
            className="prefix__cls-14"
            d="M69 85.11v4.56c0 10.22 9.27 10.22 9.27 10.22M106.12 85.11v4.56c0 10.22 9.27 10.22 9.27 10.22"
          />
        </motion.g>
        <motion.g
          animate={{
            y: [0, -70, -70, 0],
            rotate: [0, 80, 80, 0],
          }}
          transition={{
            duration: 7, times: [0, 0.3, 0.7, 1], repeat: Infinity, repeatDelay: 3,
          }}
          style={{
            transformBox: 'fill-box',
            originX: 'right',
            originY: 'center',
          }}
        >
          <path
            className="prefix__cls-1 hand"
            d="M117.93 140.34A24.12 24.12 0 01142 164.45a24.12 24.12 0 01-24.12-24.12l.05.01z"
            transform="rotate(-96.06 129.992 152.396)"
          />
        </motion.g>
      </g>
    </svg>
  );
}

export default SvgComponent;
