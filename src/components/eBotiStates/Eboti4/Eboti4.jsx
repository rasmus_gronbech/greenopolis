import * as React from 'react';
import { motion } from 'framer-motion';

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 200 200"
      {...props}
      className="eboti"
    >
      <defs>
        <clipPath id="prefix__clip-path">
          <path
            d="M146.15 49.64c-3-14.09-11.42-22.64-24.22-24.73-12.06-2-22.18-.05-30.09 5.68-12.34 9-14.7 23.85-14.79 24.48a1.68 1.68 0 000 .67c.27 1.22 2.63 7.48 22.12 10.6 5.56.89 11.91 1.73 18.06 2.46 14.2 1.7 27.39 2.86 27.58 2.87a1.94 1.94 0 002.08-1.62 62.47 62.47 0 00-.74-20.41z"
            fill="none"
          />
        </clipPath>
        <style>
          {
            '.prefix__cls-17{fill:none}.prefix__cls-4{fill:#2bb673}.prefix__cls-5{fill:#199b69}.prefix__cls-6{fill:#fafcff}.prefix__cls-7{fill:#cfdae8}.prefix__cls-10{fill:#006838}.prefix__cls-17,.prefix__cls-18{stroke-linecap:round;stroke-linejoin:round}.prefix__cls-18{fill:transparent}.prefix__cls-17,.prefix__cls-18{stroke:#c6edf5;stroke-width:1.06px}'
          }
        </style>
      </defs>
      <g
        style={{
          isolation: 'isolate',
        }}
      >
        <g id="prefix__Layer_2" data-name="Layer 2">
          <path
            className="prefix__cls-4"
            d="M134.66 129.4l.89 16.69c.52 9.75-7.23 18.07-17.31 18.57l-5.29.27c-10.08.5-18.67-7-19.19-16.76l-.89-16.68c-.53-9.76 7.22-18.08 17.3-18.58l5.29-.26c10.08-.51 18.68 6.99 19.2 16.75z"
          />
          <path
            className="prefix__cls-5"
            d="M134.77 131.52l.55 10.25a96.25 96.25 0 01-41.8 1.91L93 133.61c-.52-9.75 7.23-18.07 17.31-18.57l5.29-.27c10.06-.5 18.65 7 19.17 16.75z"
          />
          <path
            id="prefix__HAND_R"
            data-name="HAND R"
            className="prefix__cls-4"
            d="M155.82 143.61a19 19 0 002.26 14.74 18.79 18.79 0 0012.17 8.5 1 1 0 001.17-.74A18.93 18.93 0 00157 142.87a1 1 0 00-1.18.74z"
          />
          <g id="prefix__jacket">
            <path
              className="prefix__cls-6"
              d="M124.62 131.61l15.73.48 3.86 37.8-20.65 3.71a4.13 4.13 0 01-4.13-4.13v-6.88z"
            />
            <path
              className="prefix__cls-7"
              d="M127 132.04l4.72 10.59-5.22 1.9 3.68 2.6-10.74 15.46 4.81-32.65 2.75 2.1z"
            />
            <path
              className="prefix__cls-6"
              d="M109 131.32l-16.73 1.26L91 170.37l19 2.93a4.13 4.13 0 004.13-4.13v-6.88z"
            />
            <path
              className="prefix__cls-7"
              d="M106.56 131.74l-4.71 10.59 5.22 1.91-3.68 2.59 10.75 15.46-4.82-32.65-2.76 2.1z"
            />
          </g>
          <path
            id="prefix__hrad"
            className="prefix__cls-4"
            d="M188.38 106.83l-6.48-8.31A97 97 0 0049.15 80l-8.54 6.22a5.81 5.81 0 00-1.17 8.31l6.48 8.32a95.61 95.61 0 0062.62 35.85 98.06 98.06 0 0018.55.84 95.23 95.23 0 0051.58-18.18l8.54-6.21a5.81 5.81 0 001.17-8.32z"
          />
          <path
            d="M175 107.66l-.79-1.15a83.34 83.34 0 00-24.81-23.86 78.05 78.05 0 00-62.86-8.77A83.9 83.9 0 0056.08 90l-1.08.92a2.13 2.13 0 00-.4 2.8l.79 1.14a83.22 83.22 0 0024.81 23.86 78.05 78.05 0 0062.86 8.77 83.83 83.83 0 0030.45-16.15l1.07-.89a2.12 2.12 0 00.4-2.79z"
            fill="#217b38"
          />
          <path
            d="M174.81 108.93l-.79-1.14a83.37 83.37 0 00-24.81-23.87 78.16 78.16 0 00-62.86-8.77A83.77 83.77 0 0055.9 91.31l-1.07.88a2.13 2.13 0 00-.4 2.8l.79 1.14A83.37 83.37 0 0080 120a78.16 78.16 0 0062.86 8.77 84 84 0 0030.45-16.16l1.07-.88a2.14 2.14 0 00.4-2.8z"
            fill="#beeda2"
          />
          <motion.path
            className="prefix__cls-10 eye"
            animate={{
              scale: [1, 0, 1],
            }}
            style={{
              originX: 1,
              originY: 1,
            }}
            transition={{ duration: 0.5, repeat: Infinity, repeatDelay: 5.5 }}
            d="M138.9 90.09a2.77 2.77 0 00-3.29 2.16l-1.6 8.36a2.83 2.83 0 005.56 1.06l1.59-8.36a2.78 2.78 0 00-2.26-3.22z"
          />
          <motion.path
            className="prefix__cls-10 eye"
            animate={{
              scale: [1, 0, 1],
            }}
            style={{
              originX: 1,
              originY: 1,
            }}
            transition={{ duration: 0.5, repeat: Infinity, repeatDelay: 5.5 }}
            d="M88.76 82.86A2.79 2.79 0 0085.47 85l-1.59 8.37a2.83 2.83 0 005.56 1.06L91 86.08a2.79 2.79 0 00-2.24-3.22z"
          />
          <path
            d="M126.5 106l-28.29 4.6s3.43 12.94 15.14 10.57S126.5 106 126.5 106z"
            strokeLinecap="round"
            strokeLinejoin="round"
            stroke="#006838"
            strokeWidth={2.72}
            fill="#006838"
          />
          <g id="prefix__hat-2" data-name="hat" fill="#fcf568">
            <path
              d="M146 70.55s-21.21 2.93-41 2.14c-13.45-.53-25.45-3.64-32.84-7.18-18.29-8.78 6.93-11 6.93-11s5.34 4.57 20.71 7.63 46.2 8.41 46.2 8.41z"
              strokeWidth={3.84}
              stroke="#fcf568"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <g clipPath="url(#prefix__clip-path)">
              <path d="M146.15 49.64c-3-14.09-11.42-22.64-24.22-24.73-12.06-2-22.18-.05-30.09 5.68-12.34 9-14.7 23.85-14.79 24.48a1.68 1.68 0 000 .67c.27 1.22 2.63 7.48 22.12 10.6 5.56.89 11.91 1.73 18.06 2.46 14.2 1.7 27.39 2.86 27.58 2.87a1.94 1.94 0 002.08-1.62 62.47 62.47 0 00-.74-20.41z" />
              <path
                d="M125.71 24.11a79.36 79.36 0 00-19.27 19.71A53.31 53.31 0 0098 68.92l-17.18-4.68s1.7-12.77 7.72-22.56c6.39-10.38 22.07-19.37 22.07-19.37z"
                stroke="#f9cf5f"
                strokeWidth={3.84}
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </g>
          </g>
          <motion.g
            id="prefix__hand"
            animate={{
              y: [0, 10, 0, 10, 0, 10, 0],
            }}
            transition={{
              duration: 2, repeat: Infinity, repeatDelay: 5,
            }}
            style={{
              originX: 0.5,
              originY: 0.5,
            }}
          >
            <path
              id="prefix__HAND_R-2"
              data-name="HAND R"
              className="prefix__cls-5"
              d="M63.24 147.59a21.22 21.22 0 00-12.84-10.53 20.93 20.93 0 00-16.4 2 1.11 1.11 0 00-.42 1.48 21.08 21.08 0 0029.24 8.52 1.12 1.12 0 00.42-1.47z"
            />
            <motion.path
              animate={{
                fill: ['#5eeaea', '#fff', '#5eeaea'],
              }}
              transition={{
                duration: 2, repeat: Infinity, repeatDelay: 5,
              }}
              style={{
                originX: 0.5,
                originY: 0.5,
              }}
              className="screen"
              d="M29.32 165.31l24.78-2.62a4.48 4.48 0 003.9-5.95l-11.7-40.28a6.87 6.87 0 00-7-4.79l-24.78 2.61a4.48 4.48 0 00-3.92 6l11.7 40.28a6.88 6.88 0 007.02 4.75z"
              fill="#5eeaea"
            />
            <path
              id="prefix__HAND_R-3"
              data-name="HAND R"
              className="prefix__cls-4"
              d="M63.52 147.93a18.23 18.23 0 00-6.69-10.31 11.71 11.71 0 00-10.3-2.08.78.78 0 00-.43 1c2.28 8.9 9.9 14.46 17 12.39a.77.77 0 00.42-1z"
            />
            <path
              className="prefix__cls-17"
              d="M18 121.35l5.87 4-2.25 3.43-5.87-4 1 4.39 5.11 3.48 4.28-1.35 16.59 12.23a2.68 2.68 0 003.89-.35c.74-1.18.08-2.91-1.33-3.8L27.9 128.57l-.43-4.57-5.11-3.48-4.34.79s-.02.03-.02.04z"
            />
            <ellipse
              className="prefix__cls-17"
              cx={43.81}
              cy={141.39}
              rx={1.16}
              ry={1.39}
              transform="rotate(-53.62 43.817 141.383)"
            />
            <g>
              <path
                className="prefix__cls-18"
                d="M46.91 129l-1.27 1.94-13.07-8.89 3.15-4.8 9.71 6.6-.09.13c-.99 1.51-.28 3.76 1.57 5.02zM26 145.4l.43.3a1.57 1.57 0 002.21-.24l8.75-13.33-3.28-2.23-8.75 13.33a1.63 1.63 0 00.64 2.17z"
              />
              <path
                className="prefix__cls-18"
                d="M37.76 125.58l2.09 1.42-3.1 4.72-2.09-1.42 3.1-4.72z"
              />
            </g>
            <g>
              <path
                className="prefix__cls-17"
                d="M34.06 153.43a2.32 2.32 0 01-2.35-1.59 2.3 2.3 0 00-2.34-1.59h-.25a1.47 1.47 0 00-1.28 2 1.48 1.48 0 01-1.29 2 1.48 1.48 0 00-1.29 2l.07.24a2.28 2.28 0 002.34 1.51 2.31 2.31 0 012.33 1.61 2.3 2.3 0 002.34 1.59h.25a1.47 1.47 0 001.28-2 1.48 1.48 0 011.29-2 1.48 1.48 0 001.29-2l-.05-.2a2.29 2.29 0 00-2.34-1.57z"
              />
              <ellipse
                className="prefix__cls-17"
                cx={45.04}
                cy={157.7}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 45.048 157.705)"
              />
              <ellipse
                className="prefix__cls-17"
                cx={43.1}
                cy={151.16}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 43.117 151.166)"
              />
              <ellipse
                className="prefix__cls-17"
                cx={51.6}
                cy={157.01}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 51.601 157.022)"
              />
              <ellipse
                className="prefix__cls-17"
                cx={49.66}
                cy={150.47}
                rx={2.14}
                ry={2.57}
                transform="rotate(-53.66 49.66 150.478)"
              />
            </g>
          </motion.g>
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
