const React = require('react');
const GlobalContextProvider = require('./src/context/GlobalContextProvider')
  .default;
const ThemeContextProvider = require('./src/context/ThemeContextProvider')
  .default;

exports.wrapRootElement = ({ element }) => (
  <ThemeContextProvider>
    <GlobalContextProvider>
      {element}
    </GlobalContextProvider>
  </ThemeContextProvider>
);
