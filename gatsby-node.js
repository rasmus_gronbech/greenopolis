const path = require('path');
const chunk = require('lodash/chunk');

// This is a simple debugging tool
// dd() will prettily dump to the terminal and kill the process
// const { dd } = require(`dumper.js`)

/**
 * exports.createPages is a built-in Gatsby Node API.
 * It's purpose is to allow you to create pages for your site! 💡
 *
 * See https://www.gatsbyjs.com/docs/node-apis/#createPages for more info.
 */

exports.createPages = async (gatsbyUtilities) => {
  // Query our posts from the GraphQL server
  // const posts = await getPosts(gatsbyUtilities);
  const biodiversityPosts = await getBiodiversityPosts(gatsbyUtilities);
  const heatPosts = await getHeatPosts(gatsbyUtilities);
  const waterPosts = await getWaterPosts(gatsbyUtilities);
  const airPosts = await getAirPosts(gatsbyUtilities);
  const noisePosts = await getNoisePosts(gatsbyUtilities);
  const introductionPosts = await getIntroductionPosts(gatsbyUtilities);

  const biodiversityExercisesPosts = await getBiodiversityExercisesPosts(gatsbyUtilities);
  const heatExercisesPosts = await getHeatExercisesPosts(gatsbyUtilities);
  const airExercisesPosts = await getAirExercisesPosts(gatsbyUtilities);
  const waterExercisesPosts = await getWaterExercisesPosts(gatsbyUtilities);
  const noiseExercisesPosts = await getNoiseExercisesPosts(gatsbyUtilities);

  const allWpPages = await getAllWpPages(gatsbyUtilities);

  // If there are no posts in WordPress, don't do anything
  // if (!posts.length) {
  //   return;
  // }

  // If there are posts, create pages for them
  // await createIndividualBlogPostPages(posts, gatsbyUtilities);
  await createIndividualBlogPostPages(biodiversityPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(heatPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(waterPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(airPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(noisePosts, gatsbyUtilities);

  await createIndividualBlogPostPages(introductionPosts, gatsbyUtilities);

  await createIndividualBlogPostPages(biodiversityExercisesPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(heatExercisesPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(airExercisesPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(waterExercisesPosts, gatsbyUtilities);
  await createIndividualBlogPostPages(noiseExercisesPosts, gatsbyUtilities);

  await createFrontPage(introductionPosts, gatsbyUtilities);
  await createTextPage(allWpPages, gatsbyUtilities);

  // And a paginated archive
  // await createBlogPostArchive({ posts, gatsbyUtilities });
};

/**
 * This function creates all the individual blog pages in this site
 */
const createIndividualBlogPostPages = async (posts, gatsbyUtilities) => Promise.all(
  posts.map(({ previous, post, next }) =>
  // createPage is an action passed to createPages
  // See https://www.gatsbyjs.com/docs/actions#createPage for more info
    gatsbyUtilities.actions.createPage({
      // Use the WordPress uri as the Gatsby page path
      // This is a good idea so that internal links and menus work 👍
      path: post.uri,

      // use the blog post template as the page component
      component: path.resolve(`./src/templates/${post.__typename}/${post.__typename}.jsx`),

      // `context` is available in the template as a prop and
      // as a variable in GraphQL.
      context: {
        // we need to add the post id here
        // so our blog post template knows which blog post
        // the current page is (when you open it in a browser)
        id: post.id,

        // We also use the next and previous id's to query them and add links!
        previousPostId: previous ? previous.id : null,
        nextPostId: next ? next.id : null,
      },
    })),
);

const createTextPage = async (posts, gatsbyUtilities) => Promise.all(
  posts.map(({ previous, post, next }) =>
  // createPage is an action passed to createPages
  // See https://www.gatsbyjs.com/docs/actions#createPage for more info
    gatsbyUtilities.actions.createPage({
      // Use the WordPress uri as the Gatsby page path
      // This is a good idea so that internal links and menus work 👍
      path: post.uri,

      // use the blog post template as the page component
      component: path.resolve('./src/templates/TextPage/TextPage.jsx'),

      // `context` is available in the template as a prop and
      // as a variable in GraphQL.
      context: {
        // we need to add the post id here
        // so our blog post template knows which blog post
        // the current page is (when you open it in a browser)
        id: post.id,

        // We also use the next and previous id's to query them and add links!
        previousPostId: previous ? previous.id : null,
        nextPostId: next ? next.id : null,
      },
    })),
);

const createFrontPage = async (posts, gatsbyUtilities) => Promise.all(
  posts.map(({ previous, post, next }) => gatsbyUtilities.actions.createPage({

    path: '/',

    component: path.resolve(`./src/templates/${post.__typename}/index.js`),

    context: {
      id: 'cG9zdDozNDg=',
    },
  })),
);

/**
 * This function creates all the individual blog pages in this site
 */
async function createBlogPostArchive({ posts, gatsbyUtilities }) {
  const graphqlResult = await gatsbyUtilities.graphql(/* GraphQL */ `
    {
      wp {
        readingSettings {
          postsPerPage
        }
      }
    }
  `);

  const { postsPerPage } = graphqlResult.data.wp.readingSettings;

  const postsChunkedIntoArchivePages = chunk(posts, postsPerPage);
  const totalPages = postsChunkedIntoArchivePages.length;

  return Promise.all(
    postsChunkedIntoArchivePages.map(async (_posts, index) => {
      const pageNumber = index + 1;

      const getPagePath = (page) => {
        if (page > 0 && page <= totalPages) {
          // Since our homepage is our blog page
          // we want the first page to be "/" and any additional pages
          // to be numbered.
          // "/blog/2" for example
          return page === 1 ? '/blog/' : `/blog/${page}`;
        }

        return null;
      };

      // createPage is an action passed to createPages
      // See https://www.gatsbyjs.com/docs/actions#createPage for more info
      await gatsbyUtilities.actions.createPage({
        path: getPagePath(pageNumber),

        // use the blog post archive template as the page component
        component: path.resolve('./src/templates/blog-post-archive.js'),

        // `context` is available in the template as a prop and
        // as a variable in GraphQL.
        context: {
          // the index of our loop is the offset of which posts we want to display
          // so for page 1, 0 * 10 = 0 offset, for page 2, 1 * 10 = 10 posts offset,
          // etc
          offset: index * postsPerPage,

          // We need to tell the template how many posts to display too
          postsPerPage,

          nextPagePath: getPagePath(pageNumber + 1),
          previousPagePath: getPagePath(pageNumber - 1),
        },
      });
    }),
  );
}

/**
 * This function queries Gatsby's GraphQL server and asks for
 * All WordPress blog posts. If there are any GraphQL error it throws an error
 * Otherwise it will return the posts 🙌
 *
 * We're passing in the utilities we got from createPages.
 * So see https://www.gatsbyjs.com/docs/node-apis/#createPages for more info!
 */
async function getPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      # Query all WordPress blog posts sorted by date
      allWpPost(sort: { fields: [date], order: DESC }) {
        edges {
          previous {
            id
          }
          # note: this is a GraphQL alias. It renames "node" to "post" for this query
          # We're doing this because this "node" is a post! It makes our code more readable further down the line.
          post: node {
            id
            uri
            __typename
          }
          next {
            id
          }
        }
      }
    }
  `);

  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return ('');
  }

  return graphqlResult.data.allWpPost.edges;
}

/**
 * This function queries all the posts of a type 'WaterPosts'
 */

async function getWaterPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpWaterPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpWaterPost.edges;
}

/**
 * This function queries all the posts of a type 'Introduction post'
 */

async function getIntroductionPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpIntroductionPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpIntroductionPost.edges;
}
/**
 * This function queries all the posts of a type 'Heat post'
 */

async function getHeatPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpHeatPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpHeatPost.edges;
}

/**
 * This function queries all the posts of a type 'Biodiversity post'
 */

async function getBiodiversityPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpBiodiversityPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpBiodiversityPost.edges;
}

/**
 * This function queries all the posts of a type 'Air post'
 */

async function getAirPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpAirPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpAirPost.edges;
}

/**
 * This function queries all the posts of a type 'Noise post'
 */

async function getNoisePosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpNoisePost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpNoisePost.edges;
}

// --------------------------- exercises -------------------------------

/**
 * This function queries all the posts of a type 'Biodiversity exercises post'
 */

async function getBiodiversityExercisesPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpBiodiversityExercisesPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpBiodiversityExercisesPost.edges;
}

/**
 * This function queries all the posts of a type 'Heat exercises post'
 */

async function getHeatExercisesPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpHeatExercisesPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpHeatExercisesPost.edges;
}

/**
 * This function queries all the posts of a type 'Air exercises post'
 */

async function getAirExercisesPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpAirExercisesPost {
        edges {
          post: node {
            id
            uri 
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpAirExercisesPost.edges;
}

/**
 * This function queries all the posts of a type 'Water exercises post'
 */

async function getWaterExercisesPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
      allWpWaterExercisesPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpWaterExercisesPost.edges;
}

/**
 * This function queries all the posts of a type 'Noise exercises post'
 */

async function getNoiseExercisesPosts({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
       allWpNoiseExercisesPost {
        edges {
          post: node {
            id
            uri
            __typename
          }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }

  return graphqlResult.data.allWpNoiseExercisesPost.edges;
}

/**
 * This function queries all the posts of a type 'Noise exercises post'
 */

async function getAllWpPages({ graphql, reporter }) {
  const graphqlResult = await graphql(/* GraphQL */ `
    query WpPosts {
       allWpPage {
        edges {
          post: node {
            id
            uri
           }
        }
      }
    }
  `);
  if (graphqlResult.errors) {
    reporter.panicOnBuild(
      'There was an error loading your blog posts',
      graphqlResult.errors,
    );
    return '';
  }
  return graphqlResult.data.allWpPage.edges;
}
